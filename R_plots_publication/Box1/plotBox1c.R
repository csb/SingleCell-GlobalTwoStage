# 
library(dplyr)
library(ggplot2)

# Theme
theme_Publication <- function(base_size=14, base_family="helvetica") {
  (theme_classic(base_size = base_size)+ theme(text = element_text(),
                                               axis.title = element_text(size = rel(1)),
                                               axis.title.y = element_text(angle=90,vjust =2),
                                               axis.title.x = element_text(vjust = -0.2),
                                               axis.text = element_text(colour='black'), 
                                               axis.line = element_line(colour="black",size=base_size/30),
                                               axis.ticks = element_line(size=base_size/30),
                                               legend.key = element_rect(colour = NA),
                                               legend.position = "bottom",
                                               legend.direction = "horizontal",
                                               legend.key.size= unit(0.4, "cm"),
                                               legend.margin = unit(0, "cm"),
                                               legend.title = element_text(),
                                               plot.margin=unit(c(1.5,1.5,1.5,1.5),"mm"),
                                               strip.background=element_blank(),
                                               strip.text = element_text()
  ))
}
# Calculates the median sobol index over five runs, ignores time zero ...
man_dir_supplement='/Users/dlekshmi/polybox/Manuscript-SingleCell/R_plots_publication/Box1-noise-decomposition/'

load(paste0(man_dir_supplement,'/Sobol_box1c.Rdata'))
data_to_plot= Sobol_df_all%>%group_by(Parameter,Times,Dhats)%>%
  summarise(Med_Si=median(Sis))%>%
  subset(Times>0&Times<100)

# Rearrange the runs, treat NaNS as zero
data_to_plot$Dhats=factor(data_to_plot$Dhats, levels = data_to_plot$Dhats[c(3,1,2)])
data_to_plot=data_to_plot%>%replace(., is.na(.), 0)

# plotting all the estimates
plot_all=ggplot(data_to_plot,aes(x=Times))+geom_jitter(aes(y=Med_Si,col=Parameter),shape=20,size=3,width=2.5)+
  facet_grid(~Dhats)+theme_classic()+
  theme_Publication(12)+xlab('Time [min]')+ylab('Sobol index')+ 
  theme(axis.ticks.length=unit(-0.1, "cm"), axis.text.x = element_text(margin=unit(c(0.25,0.25,0.25,0.25), "cm")), axis.text.y = element_text(margin=unit(c(0.25,0.25,0.25,0.25), "cm")) )
plot_all=plot_all+ scale_x_continuous(expand = c(0, 0),breaks = c(0,40,80),limits = c(0,101))+scale_color_manual(values=c('#bc8c2d','#1a9641')) 


# Modify path to save: 
ggsave(plot=plot_all,height=3,width=5.5,dpi=200, filename=paste0(man_dir_supplement,'/Box1c.pdf'), useDingbats=FALSE)

