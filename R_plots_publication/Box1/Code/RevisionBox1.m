% Simulate a two-state model of gene expression
% Taken from https://github.com/nvictus/Gillespie 
% Latest commit d772e07  on 29 Jun 2013
% http://ch.mathworks.com/matlabcentral/fileexchange/34707-gillespie-stochastic-simulation-algorithm
% All licesnses as in license.txt in the Gillespie 

% Used by Lekshmi D
close all
clear all



% Simulate a two-state model of gene expression
% Taken from https://github.com/nvictus/Gillespie 
% Latest commit d772e07  on 29 Jun 2013
% http://ch.mathworks.com/matlabcentral/fileexchange/34707-gillespie-stochastic-simulation-algorithm
% All licesnses as in license.txt in the Gillespie 
addpath(genpath('../'))

close all
clear all
q1=0.025;
q2=0.975;

%% Get the determinsitic model for the simple model
% Used for panel a

mnoise=0.2;
seeds={[3,5,8,6,10,100],[3,5,8,6,10,100]}; %            
rate_mRNA=[0.5];   
rate_protein=[0.05];   
cols={'k','b'};
file_to_save='../box2_fig';
noise_param.theta0=30;
noise_param.theta1=0.1;
df=ssa_example(seeds, rate_mRNA,rate_protein,noise_param,cols,file_to_save);
fd=cat(1,df(1).run.mRNA);
fd=fd+normrnd(0,mnoise,size(fd));
% print -painters  -dpdf -bestfit fig3_cut.pdf

% Get the poisson rate
lambda=rate_mRNA/0.07;
var_poisson=lambda;


% Assume deterministic model and simulate things. 
var_mRNA=0.2;
deg_mRNA=0.07;
params=normrnd(rate_mRNA,var_mRNA,1,10000);
quantile1=quantile(params,q1);
quantile2=quantile(params,q2);

t=0:10000;
Xt=zeros(10000,length(t));
for i=1:10000
Xt(i,:)=params(i)./deg_mRNA*(1-exp(-deg_mRNA.*t))+normrnd(0,mnoise,1,length(t));
end
Xmean=rate_mRNA./deg_mRNA*(1-exp(-deg_mRNA.*t));
Xquan1=quantile1./deg_mRNA*(1-exp(-deg_mRNA.*t));
Xquan2=quantile2./deg_mRNA*(1-exp(-deg_mRNA.*t));

mean(Xt(:,end))
ksdensity(poissrnd(lambda,1,10000));
hold on 
ksdensity(Xt(:,i))

params1=[params',repmat(0.07,length(params),1)];
params2=[rate_mRNA,deg_mRNA];

% poisson process
figure()
subplot(2,4,3)
for i=1:100
plot1=plot(0:10000,df(1).run(i).mRNA,'-','Color','r');
plot1.Color(4)=0.02;
hold on 
end
plot1=plot(0:10000,df(1).run(1).mRNA,'-','Color','r');

plot(0:10000,mean(fd),'-k','LineWidth',2)
plot(0:10000,quantile(fd,q2),'--k','LineWidth',2)
plot(0:10000,quantile(fd,q1),'--k','LineWidth',2)

ylabel('Y molecules')
xlim([0,500])
ylim([0,20])
box off

% Deterministic model
subplot(2,4,2)
for i=1:100
plot2=plot(0:10000,Xt(i,:),'-','Color','b');
plot2.Color(4)=0.05;
hold on 
end

plot(0:10000,Xmean,'-b','LineWidth',2)
plot(0:10000,mean(Xt),'-k','LineWidth',2)
plot(0:10000,Xquan1,'--b','LineWidth',2)
plot(0:10000,Xquan2,'--b','LineWidth',2)
plot(0:10000,quantile(Xt,q2),'--k','LineWidth',2)
plot(0:10000,quantile(Xt,q1),'--k','LineWidth',2)

ylim([0,20])
xlim([0,500])
ylabel('Y molecules')
xlabel('Time s')
box off
%
% the density at ss
subplot(2,4,4)

[f,xi] =ksdensity(poissrnd(lambda,1,10000));
plot(xi,f,'-r','LineWidth',2)
hold on 
[f,xi]=ksdensity(Xt(:,end));
plot(xi,f,'-c','LineWidth',2)
set(gca,'XDir','reverse');
xlim([0,20])
camroll(-90)
xlabel('Y molecules')
ylabel('density')
box off

%% Non-linear model with the same settings
% integrate the ODE model now. 
% Simulate with correlation

t0 = 0;
tfinal = 10000;
y0 = [0 0]';   

% Simulate the differential equation.
pgY=0.003; pkX=0.05;
[t,y] = ode15s(@ToyModel,[0:1:tfinal],y0,[],[pgY,pkX]);
Y_mean=y(:,1);

var_mRNA=0.002;
betahat=log([pgY,pkX]);

rng(200)
D=corr2cov(sqrt([0.1,3]),[1,1;1,1]);% 
n_cells=100;
params=mvnrnd(betahat,D,n_cells);
params3=params;
pquan1=quantile(params,q1);
pquan2=quantile(params,q2);



Xt=zeros(n_cells,length(t));
opt=odeset('NonNegative',1:2);
[t,yquan1] = ode15s(@ToyModel,t,y0,opt,exp(pquan1)); 
[t,yquan2] = ode15s(@ToyModel,t,y0,opt,exp(pquan2)); 

for i=1:n_cells
[t,y] = ode15s(@ToyModel,t,y0,opt,exp(params(i,:)));   
Xt(i,:)=y(:,1)'+normrnd(0,mnoise,1,length(t));
end

subplot(2,4,5)
for i=1:n_cells
plot2=plot(0:10000,Xt(i,:),'-','Color','b','LineWidth',0.5);
plot2.Color(4)=0.05;
hold on 
end
%plot2=plot(0:10000,Xt(2,:),'-','Color','b');

plot(0:10000,Y_mean,'-b','LineWidth',2)
plot(0:10000,yquan1(:,1),'--b','LineWidth',2)
plot(0:10000,yquan2(:,1),'--b','LineWidth',2)


%plot(0:10000,median(Xt),'--k','LineWidth',2)
plot(0:10000,mean(Xt),'-k','LineWidth',2)
plot(0:10000,quantile(Xt,q2),'--k','LineWidth',2)
plot(0:10000,quantile(Xt,q1),'--k','LineWidth',2)
ylim([0,50])
xlim([0,5000])
ylabel('Y molecules')
xlabel('Time s')
box off

%% No Correlation between parameters
D=corr2cov(sqrt([0.1,3]),[1,0;0,1]);% 
params=mvnrnd(betahat,D,n_cells);
params4=params;
pquan1=quantile(params,q1);
pquan2=quantile(params,q2);

[t,yquan1] = ode15s(@ToyModel,t,y0,opt,exp(pquan1)); 
[t,yquan2] = ode15s(@ToyModel,t,y0,opt,exp(pquan2)); 

Xt=zeros(n_cells,length(t));
opt=odeset('NonNegative',1:2);
for i=1:n_cells
[t,y]=ode15s(@ToyModel,t,y0,opt,exp(params(i,:)));   
Xt(i,:)=y(:,1)'+normrnd(0,mnoise,1,length(t));
end


subplot(2,4,6)
for i=1:n_cells
plot2=plot(0:10000,Xt(i,:),'-','Color','b','LineWidth',0.5);
plot2.Color(4)=0.05;
hold on 
end

plot(0:10000,Y_mean,'-b','LineWidth',2)
plot(0:10000,yquan1(:,1),'--b','LineWidth',2)
plot(0:10000,yquan2(:,1),'--b','LineWidth',2)

%plot(0:10000,median(Xt),'--k','LineWidth',2)
plot(0:10000,mean(Xt),'-k','LineWidth',2)
plot(0:10000,quantile(Xt,q2),'--k','LineWidth',2)
plot(0:10000,quantile(Xt,q1),'--k','LineWidth',2)
ylabel('Y molecules')
xlabel('Time s')
ylim([0,50])
xlim([0,5000])
box off

%% Only one varying parameter
D=corr2cov(sqrt([0,2]),[1,0;0,1]);% 
params=mvnrnd(betahat,D,n_cells);
params5=params;
pquan1=quantile(params,q1);
pquan2=quantile(params,q2);

[t,yquan1] = ode15s(@ToyModel,t,y0,opt,exp(pquan1)); 
[t,yquan2] = ode15s(@ToyModel,t,y0,opt,exp(pquan2)); 

Xt=zeros(n_cells,length(t));
opt=odeset('NonNegative',1:2);
for i=1:n_cells
[t,y] = ode15s(@ToyModel,t,y0,opt,exp(params(i,:)));   
Xt(i,:)=y(:,1)'+normrnd(0,mnoise,1,length(t));
end

subplot(2,4,7)
for i=1:n_cells
plot2=plot(0:10000,Xt(i,:),'-','Color','b','LineWidth',0.5);
plot2.Color(4)=0.05;

hold on 
end


plot(0:10000,Y_mean,'-b','LineWidth',2)
plot(0:10000,yquan1(:,1),'--b','LineWidth',2)
plot(0:10000,yquan2(:,1),'--b','LineWidth',2)

%plot(0:10000,median(Xt),'--k','LineWidth',2)
plot(0:10000,mean(Xt),'-k','LineWidth',2)
plot(0:10000,quantile(Xt,q2),'--k','LineWidth',2)
plot(0:10000,quantile(Xt,q1),'--k','LineWidth',2)
ylim([0,50])
xlim([0,5000])
ylabel('Y molecules')
xlabel('Time s')
box off

%% Gillespie with the nonlinear model
rng(20)
df=ssa_example2(seeds, 0.2,0.05,noise_param,cols,file_to_save);
fd=cat(1,df(1).run.Y);
fd=fd+normrnd(0,mnoise,size(fd));
%
subplot(2,4,8)

params6=log([pgY,pkX]);

for i=2:100
plot1=plot(0:10000,df(1).run(i).Y,'-','Color','r');
plot1.Color(4)=0.02;
hold on 
end


plot(0:10000,mean(fd),'-k','LineWidth',2)
plot(0:10000,quantile(fd,q2),'--k','LineWidth',2)
plot(0:10000,quantile(fd,q1),'--k','LineWidth',2)

plot(0:10000,Y_mean,'-r','LineWidth',2)
ylim([0,50])
xlim([0,5000])
box off
ylabel('Y molecules')
xlabel('Time s')
print -painters -dpdf trial2_2_95.pdf
%% SCATTER plots of paramseters

figure()
subplot(2,4,1)
scatter(params1(:,1),params1(:,2),20,'filled','b','MarkerFaceAlpha',0.2)
hold on
quantiles=quantile(params1,[q1,0.5,q2]);
scatter(quantiles(:,1),quantiles(:,2),20,'filled','k')
xlabel('ln k_y')
ylabel('ln g_y')
xlim([-1,2])
ylim([-1,2])
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,4,2)
scatter(params2(:,1),params2(:,2),20,'filled','b')
hold on
%quantiles=quantile(params2,[q1,0.5,q2]);
%scatter(quantiles(:,1),quantiles(:,2),20,'filled','k')
xlabel('ln k_y')
ylabel('ln g_y')
xlim([-1,2])
ylim([-1,2])
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,4,5)
scatter(params3(:,2),params3(:,1),20,'filled','b','MarkerFaceAlpha',0.2)
hold on
quantiles=quantile(params3,[q1,0.5,q2]);
scatter(quantiles(:,2),quantiles(:,1),20,'filled','k')
xlabel('ln k_x')
ylabel('ln g_y')
xlim([-10,5])
ylim([-7,-4.5])
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,4,6)
scatter(params4(:,2),params4(:,1),20,'filled','b','MarkerFaceAlpha',0.2)
hold on
quantiles=quantile(params4,[q1,0.5,q2]);
scatter(quantiles(:,2),quantiles(:,1),20,'filled','k')
xlabel('ln k_x')
ylabel('ln g_y')
xlim([-10,5])
ylim([-7,-4.5])
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,4,7)
scatter(params5(:,2),params5(:,1),20,'filled','b','MarkerFaceAlpha',0.2)
hold on
quantiles=quantile(params5,[q1,0.5,q2]);
scatter(quantiles(:,2),quantiles(:,1),20,'filled','k')
xlabel('ln k_x')
ylabel('ln g_y')
xlim([-10,5])
ylim([-7,-4.5])
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,4,8)
scatter(params6(:,2),params6(:,1),20,'filled','b')
xlim([-10,5])
ylim([-7,-4.5])
xlabel('ln k_x')
ylabel('ln g_y')
set(gca,'xtick',[])
set(gca,'ytick',[])
print -painters -dpdf trial2_3_95.pdf