function yp =ToyModel(t,x,px)
%Toy model 

p.gX = 0.002;
p.kY=0.2;
basal_rate=2e-3;

Y    = x(1);
X = x(2);
p.gY=px(1);
p.kX=px(2);

a = [p.kY*(X)/100+basal_rate;                     
    p.kX*Y*(25-X)/(50+25-X);     
    p.gY*Y                                   
    p.gX*X/(10+X)];                

stoich_matrix = [ 1  0    
                  0  1   
                 -1  0    
                  0 -1 ]; 
         
yp= (a'*stoich_matrix)';

end