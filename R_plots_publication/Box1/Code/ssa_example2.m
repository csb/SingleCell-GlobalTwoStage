function data=ssa_example(seeds, rate_Y,rate_X,noise_param,cols,file_to_save)
% Simulate a two-state model of gene expression
% Taken from https://github.com/nvictus/Gillespie
% All licesnses as in license.txt



import Gillespie.*
data=struct;
for i=1:length(seeds)
    seeds_to_set=1:1000;
    for j=1:length(seeds_to_set);
        rng('shuffle');
   
  
        
        %% Rate constants
        p.kY = rate_Y;
        p.kX = rate_X;
        p.gY = 0.003;
        p.gX = 0.002;
        scale_p=0.1;
        
        %% Initial state
        tspan = [0, 10000]; %seconds
        x0    = [0,0];     %Y, X
        
        %% Specify reaction network
        pfun = @propensities_2state;
        stoich_matrix = [ 1  0    % Y production
             0  1    % X prodution
            -1  0    %Y decay
             0 -1 ]; %X decay
        
        %% Run simulation
        [t,x] = directMethod(stoich_matrix, pfun, tspan, x0, p);
        %[t,x] = firstReactionMethod(stoich_matrix, pfun, tspan, x0, p);
        
        %% Plot time course
        %figure();
        % stairs(t,x); set(gca,'XLim',tspan);
        % xlabel('time (s)');
        % ylabel('molecules');
        % legend({'Y','X'});
        
        % Y
%         subplot(1,3,[1])
         time_interp=0:10000;
         Y=interp1(t,x(:,1),time_interp);
%         stairs(time_interp/3600,Y,cols{i});
%         xlabel('time (h)');
%         xlim([0 1])
%         ylabel('Y molecules');
%         hold on
%         
%         subplot(1,3,2)
         X=interp1(t,x(:,2),time_interp);
%         stairs(time_interp/3600,X,cols{i});
%         xlabel('time (h)');
%         xlim([0,1])
%         ylabel('X molecules');
%         hold on
%         
        
        
        %subplot(2,3,3)
        f_t=interp1(t,scale_p*x(:,2),time_interp);
        % additive noise ONLY
        y_noise=f_t+(noise_param.theta0).*normrnd(0,1,1,length(f_t));
        
        
        %hold on
        
        texp=[0:120:10000];
        f_t=interp1(t,scale_p*x(:,2),texp);
        y=f_t+(noise_param.theta0+noise_param.theta1.*f_t).*normrnd(0,1,1,length(f_t));
        
        
        data(i).run(j).Y=Y;
        data(i).run(j).X=X;
        data(i).run(j).measured=y;
        data(i).run(j).noise=y_noise;
        data(i).run(j).time=time_interp;
        data(i).run(j).texp=texp;
        data(i).run(j).seed=seeds_to_set(j);
        data(i).run(j).param=p;
    end
    
    % add avergaes
    mean_Y=smooth(mean(cat(1,data(i).run.Y)),0.1);
%    mean_X=smooth(mean(cat(1,data(i).run.X)),0.1);
    mean_noise=mean(cat(1,data(i).run.noise));
    mean_meas=(cat(1,data(i).run(1).measured)); % an instance
    
    % measurement
%     subplot(1,3,3)
%     plot(texp/3600,mean_meas,sh{i}, 'MarkerFaceColor',cols{i},'MarkerEdgeColor',cols{i});
%     xlabel('time (h)');
%     xlim([0,1])
%     ylim([0, 1000])
%     ylabel('AU');
%     hold on
%     
    
    
    
   % subplot(1,3,1)
  %  plot(time_interp/3600,mean_Y,'-r');
    hold on
end


% save the data
save(sprintf('%s.mat',file_to_save),'data')



end


function a = propensities_2state(x, p)
% Return reaction propensities given current state x
Y    = x(1);
X = x(2);
basal_rate=2e-3;

a = [p.kY*(X)/100+basal_rate;                     %transcription, +0.05*X
    p.kX*Y*(25-X)/(50+25-X);     %translation
    p.gY*Y                                   %Y decay*X 
    p.gX*X/(10+X)];    
end



