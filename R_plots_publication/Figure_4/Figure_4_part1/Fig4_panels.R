rm(list=ls())
## Generates things needed for Figure 4 in the Manuscript
## Lekshmi Dharmarajan
## CSB, 2017

# libraries needed
library(R.matlab) # Matlab-> R
library(ggplot2)  # PLot
library(gridExtra) # gridlike
library(grid)  # gridlike
require(tikzDevice) # Tex figures
library(RColorBrewer) # Define my colors
library(reshape2)   # for melt
library(plyr)  # table manipulation
library(ggdendro)

source('~/corrplot.R') 
source('~/colorlegend.R')

grid_arrange_shared_legend <- function(..., nrow = 1, ncol = length(list(...)), position = c("bottom", "right")) {
  plots <- list(...)
  position <- match.arg(position)
  g <- ggplotGrob(plots[[1]] + theme(legend.position = position))$grobs
  legend <- g[[which(sapply(g, function(x) x$name) == "guide-box")]]
  lheight <- sum(legend$height)
  lwidth <- sum(legend$width)
  gl <- lapply(plots, function(x) x + theme(legend.position = "none"))
  gl <- c(gl, nrow = nrow, ncol = ncol)
  
  combined <- switch(position,
                     "bottom" = arrangeGrob(do.call(arrangeGrob, gl),
                                            legend,
                                            ncol = 1,
                                            heights = unit.c(unit(1, "npc") - lheight, lheight)),
                     "right" = arrangeGrob(do.call(arrangeGrob, gl),
                                           legend,
                                           ncol = 2,
                                           widths = unit.c(unit(1, "npc") - lwidth, lwidth)))
  #grid.newpage()
  grid.draw(combined)
  return(combined)
}

# My theme for the paper.
theme_Publication <- function(base_size=14, base_family="helvetica") {
  (theme_classic(base_size = base_size)+ theme(text = element_text(),
                                               axis.title = element_text(size = rel(1)),
                                               axis.title.y = element_text(angle=90,vjust =2),
                                               axis.title.x = element_text(vjust = -0.2),
                                               axis.text = element_text(colour='black'), 
                                               axis.line = element_line(colour="black",size=base_size/30),
                                               axis.ticks = element_line(size=base_size/30),
                                               legend.key = element_rect(colour = NA),
                                               legend.position = "bottom",
                                               legend.direction = "horizontal",
                                               legend.key.size= unit(0.4, "cm"),
                                               legend.margin = unit(0, "cm"),
                                               legend.title = element_text(),
                                               plot.margin=unit(c(1.5,1.5,1.5,1.5),"mm"),
                                               strip.background=element_blank(),
                                               strip.text = element_text()
  ))
}
# SET DIRECTORY TO ROOT OF THE FOLDER.
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
direc='../../Single_Cell_Comparison'
setwd(direc)


plot_path='../R_plots_publication/Figure_4/' # saving the plot
man_dir='../R_plots_publication/Figure_4/'
dir.create(man_dir)
man_dir5='../R_plots_publication/Figure_5/'
dir.create(man_dir5)

# Plot where I save the figures
man_dir_supplement='../R_plots_publication/Supplementary_figs/SI5'
dir.create(man_dir_supplement)


parameter_name=c('ln $k_2$','ln $k_3$','ln $k_4$','ln $k_5$','ln $K_M$','ln $\\alpha_R$','ln $T_P$','ln $T_E$','ln $\\alpha_E$','ln $\\mu_C$','ln $\\mu_M$','ln $V_{EM}$','ln $ V_{EC}$','ln $\\delta$','ln $V_{C0}$','ln $V_{M0}$','$I_{C0}$','$I_{M0}$');
parameter_des=c('GFP transfer cyt - mem','Degradation','Production GFP','Recycling constant','Membrane insertion','Recycling part. coef','Input time const.(protein)','Input time const. (Endocytosis)','Basal endocytosis','Cell growth','Membrane growth','Effective membrane vol.','Spot detection','Spot detection','Initial condition','Initial condition','Initial condition cell intensity','Initial condition memb. intensity')

names(parameter_des)=parameter_name
param_units=c('1/min','1/min','molec/(AU*min)','1/min','AU','-','min','min','-','1/min','1/min','AU','AU','-','AU','AU','AU','AU');

##########################################################################################################################
############################################################# Plotting Predictions 4d #######################################
######### ############################################################################################################

Preds=readMat('./Mup1_Endocytosis/NLMD_PREDICTIONS_505_new/data_image_95.mat') # 50 multistarts
Preds$sim.time=as.vector(Preds$sim.time)


# SAEM 
Preds_SAEM=readMat('./Mup1_Endocytosis/SAEM_FILES/Mup1_7/SAEM_predictions/data_image_95.mat')
Preds_SAEM$sim.time=as.vector(Preds_SAEM$sim.time)
JI=Preds_SAEM$JI.GTS[,2]


# data
raw_data=read.table(paste0(direc2,'/Mup1_Endocytosis/Data/single_cell2017_all_timeshift.tsv'),header=TRUE)
raw_data_sel=raw_data[raw_data$ID%in%c(sample(1:1907,50)),] # some randomly chosen traces

colors=c("#000000","#D55E00","#0072B2","grey")

predplot_list<-list()
titles=c("Cell volume \n $V_C$ [X $10^3$ AU]","Membrane volume \n $V_M$ [X $10^3$ AU]","Cell Intensity \n $I_C$ [AU]","Membrane Intensity \n $I_M$ [AU]","Spot Intensity \n $I_S$ [AU]",'Cytoplasmic Intensity \n $I_{CP}$ [AU]')

for(i in 1:6)
  local({
    i<-i
    print(i)
    rS_dat=raw_data_sel[raw_data_sel$YTYPE==i,]
    # Add the data
    p1=ggplot()+geom_line(data=rS_dat,aes(x=TIME,y=Y,col=colors[4],group=ID),lty=1,lwd=0.5)
    ## Add the prediction quantiles: 
    p1=p1+geom_line(aes(x=Preds$Time.dat,y=Preds$dat.025[,i],col=colors[1]),lty=2,lwd=2.5)+geom_line(aes(x=Preds$Time.dat,y=Preds$dat.5[,i],col=colors[1]),lty=1,lwd=2.5)+geom_line(aes(x=Preds$Time.dat,y=Preds$dat.975[,i],col=colors[1]),lty=2,lwd=2.5)
    
    
    p1=p1+geom_line(aes(x=Preds$sim.time,y=Preds$quan.025[,,i],col=colors[3]),lty=2,lwd=2.5)+geom_line(aes(x=Preds$sim.time,y=Preds$quan.50[,,i],col=colors[3]),lty=1,lwd=2.5)+geom_line(aes(x=Preds$sim.time,y=Preds$quan.975[,,i],col=colors[3]),lty=2,lwd=2.5)
    
      p1=p1+geom_line(aes(x=Preds_SAEM$sim.time,y=Preds_SAEM$quan.025[,,i],col=colors[2]),lty=2,lwd=2.5)+geom_line(aes(x=Preds_SAEM$sim.time,y=Preds_SAEM$quan.50[,,i],col=colors[2]),lty=1,lwd=2.5)+geom_line(aes(x=Preds_SAEM$sim.time,y=Preds_SAEM$quan.975[,,i],col=colors[2]),lty=2,lwd=2.5)
    
    
    p1=p1+xlab('')+ylab(titles[i])
    p1=p1+annotate("text", x=Inf,y=Inf, label = paste0('JI=',round(Preds$JI.GTS[i,2],2),',',round(Preds_SAEM$JI.GTS[i,2],2)),vjust=1,hjust=1,size = 4.5*11/1.25*5/14)
  
    p1=p1+theme_classic()+theme_Publication(40)+theme(axis.line=element_line(colour = 'black'),axis.ticks = element_line(colour = "black"))
    p1=p1+theme(axis.text=element_text(colour = 'black'),axis.text.x = element_text(angle = 60),axis.title=element_text(colour = 'black'))+scale_x_continuous(breaks=c(-10,0,20,40,60,80),labels=c(-10,0,20,40,60,80),)
    p1=p1+scale_colour_manual(values=c(colors[1],colors[2],colors[3],colors[4]),labels=c("Data","Predictions: GTS","Predictions: SAEM",'raw'))
    
    ###print(p1)
    #predplot_list[[i]]<<-p1+theme(legend.position = 'bottom',legend.text = element_text(size=9),legend.key.size = unit(3,"mm"),legend.title=element_text(size=0))
    predplot_list[[i]]<<-p1+theme(legend.position = 'none',,legend.title = element_text(' '))
  })
predplot_list[[1]]=predplot_list[[1]]+scale_y_continuous(breaks=c(5000,10000,15000,20000,25000),labels=c(5,10,15,20,25))
predplot_list[[2]]=predplot_list[[2]]+scale_y_continuous(label=function(x) x/1000)
p11=grid.arrange(predplot_list[[1]],predplot_list[[2]],predplot_list[[3]],predplot_list[[4]],predplot_list[[5]]+xlab('Time [min]'),predplot_list[[6]],ncol=3,nrow=2)

tikz(paste0(man_dir,'/Fig4d_option2.tex'),width=13,height =5*4,pointsize = 40)
p11=grid.arrange(predplot_list[[1]],predplot_list[[2]],predplot_list[[3]],predplot_list[[4]],predplot_list[[5]],predplot_list[[6]],ncol=2,nrow=3)
dev.off()
# Save the data used: NOT saved As I did not have it as a data frame. 
# The relevant .mat files should be used. 

#########################################################################################################
########################### Plot the covariance matrix Fig4c ###########################################
########################################################################################################
All_data=readMat('./Mup1_Endocytosis/Pub_Results_new/NLMD50_5.mat') # Using the estimates form 50 iterations
colnames(All_data$betahat)=parameter_name

Dhat=(All_data$Dhat)
dimnames(Dhat)=list(parameter_name,parameter_name)

# Fig 4c
tikz(paste0(man_dir,'Fig4c.tex'), width =13,height =13, pointsize=22) #define plot name size and font size
par(xpd=TRUE,ps=30)
corrplot(Dhat,method="color", is.corr = FALSE,type='upper',tl.cex = 1,number.cex = 3.5,mar = c(0,0,2,0),col=brewer.pal(n=10, name="RdBu"), tl.col = "black",cl.lim=c(-2,2))#addCoef.col = "black"
dev.off()
#####################################################################################################################################################
################################################################ Plot the Fig4e #############################################################
######################################################################################################################################################

# Get the correlations first!
Correl=cov2cor(Dhat)                                   # Correlation
dimnames(Correl)=list(parameter_name,parameter_name)
var_ests=diag(All_data$Dhat)# Coef. 

CoefVar_Dhat=sqrt(exp(var_ests)-1)                                    # for lognormal variables: sqrt(exp(sigma^2)-1)
CoefVar_Dhat[17:18]=(sqrt(var_ests[17:18])/All_data$betahat[17:18])# Std. dev / mean 

##################################################################################################################
################################################ Do clustering ###################################################
##################################################################################################################
library(maptree) # for choosing optimal k value using kgs function 
library(zoo) # for tree manipulation
library(ggdendro)

dissimilarity=dist(1-abs(Correl));
hc <- hclust(dissimilarity,'complete')
# op_k <- kgs(hc,as.dist(dissimilarity), maxclus = 18)


# use the optimal k value and cut the tree
cut=10;
clust    <- cutree(hc,k=cut)                              # find k clusters
plot(silhouette(cutree(hc,cut),dist(abs(Correl))))
dendr    <- dendro_data(hc, type="rectangle")             # convert for ggplot

clust.df <- data.frame(label=names(clust), cluster=factor(clust))
cbbPalette <- c( "#E69F00","#B2182B" ,"#56B4E9","#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7","#999933", "#332288")#"#6699CC"
#cbbPalette=c("#332288", "#6699CC", "#88CCEE", "#44AA99", "#117733", "#999933", "#DDCC77", "#661100", "#CC6677", "#AA4466", "#882255",)

# Manipulating tree manipulation taken from Stack overflow and modified
height <- unique(dendr$segments$y)[order(unique(dendr$segments$y), decreasing = TRUE)]
cut.height <- mean(c(height[cut], height[cut-1]))
dendr$segments$line <- ifelse(dendr$segments$y == dendr$segments$yend &
                                dendr$segments$y > cut.height, 1, 2)
dendr$segments$line <- ifelse(dendr$segments$yend  > cut.height, 1, dendr$segments$line)

dendr$segments$cluster <- c(-1, diff(dendr$segments$line))
change <- which(dendr$segments$cluster == 1)
for (i in 1:cut) dendr$segments$cluster[change[i]] = i + 1
dendr$segments$cluster <-  ifelse(dendr$segments$line == 1, 1, 
                                  ifelse(dendr$segments$cluster == 0, NA, dendr$segments$cluster))
dendr$segments$cluster <- na.locf(dendr$segments$cluster)
clust.df$label <- factor(clust.df$label, levels = levels(dendr$labels$label))
clust.df <- arrange(clust.df, label)
clust.df$cluster <- factor((clust.df$cluster), levels = unique(clust.df$cluster), labels = (1:cut) + 1)

# Positions for cluster labels
n.rle <- rle(dendr$segments$cluster)
N <- cumsum(n.rle$lengths)
N <- N[seq(1, length(N), 2)] + 1
N.df <- dendr$segments[N, ]
N.df$cluster <- N.df$cluster - 1

rownames(dendr$labels)=dendr$labels$label
dendr$labels=dendr$labels[parameter_name,]
cluster_x=dendr$labels$x
clust.df=clust.df[sort(cluster_x,decreasing = T),]
clust.df$CoefVar=CoefVar_Dhat[order(cluster_x,decreasing = T)]
cluster_x=dendr$labels$x

label_data=round(CoefVar_Dhat[order(cluster_x,decreasing = T)],2)
dendr[["labels"]] <- merge(dendr[["labels"]], clust.df, by = "label")

# Finally our plot!
p_dendro=ggplot() + 
  geom_segment(data=segment(dendr), aes(x=x, y=y, xend=xend, yend=yend,color=factor(cluster)),lwd=1)+geom_point(data=label(dendr),aes(x=x, y,col=factor(cluster),size=as.numeric(CoefVar)),pch=15)+geom_text(aes(x=18:1,y=-0.3,label=label_data),size=4.5)+scale_x_reverse(expand=c(0.01,1.1))+
  theme_Publication(16)+theme(axis.line.y=element_blank(),
        axis.ticks.y=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        axis.line.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.x=element_blank(),
        axis.title.x=element_blank(),
        panel.background=element_rect(fill="white"),
        panel.grid=element_blank())+scale_colour_manual(values = c("grey60", cbbPalette),guide=FALSE)

cluster_labels=clust.df

rownames(cluster_labels)=cluster_labels$label
df=cluster_labels[parameter_name,]
df$index=1:18


## order with respect to parameter_name
cluster_member=cluster_labels[parameter_name,]$cluster

###################################################################################################################
################################################# Sobol indices ##########################################
##################################################################################################################
library(dplyr)
library(cowplot)

# Make changes related to Sobol indices here. 
file_basename='SInd_'
dirname='./Importance_Measures/Mup1_model/' # link to sobols
folder_basename='New_run'
parameter_name=c('ln $k_2$','ln $k_3$','ln $k_4$','ln $k_5$','ln $K_M$','ln $\\alpha_R$','ln $T_P$','ln $T_E$','ln $\\alpha_E$','ln $\\mu_C$','ln $\\mu_M$','ln $V_{EM}$','ln $ V_{EC}$','ln $\\delta$','ln $V_{C0}$','ln $V_{M0}$','$I_{C0}$','$I_{M0}$');

# 
observables=c('$V_C$','$V_M$','$I_C$','$I_M$','$I_S$','$I_{CP}$')
readout_ids=c(1:6);
interesting_times=c(-4,4,14,24,44); # interesting times (rounded, like last time )
time_ids=1:5;

# Saving everythin together to plot. 
All_measures=list()
Sis=c();
V_Xis=c()
V_X_is=c();
V_Y=c()
STis=c();
Times=c();
CoV=c()
Observation=c();
Parameter=c()
Cluster=c();
Runs=c()
Var_ests=c()
for(run_id in 1:5){
    for(readout_id in readout_ids){
      Sobol_measures=list();
      for(time_id in time_ids){
        tryCatch({
        Sobol_measures[[time_id]]=readMat(paste0(dirname,folder_basename,run_id,'/',file_basename,readout_id,'_',time_id,'.mat'))
        
        Runs=c(Runs,rep(run_id,18))
        Sis=c(Sis,Sobol_measures[[time_id]]$S.i[order(cluster_x,decreasing = T)])
        CoV=c(CoV,CoefVar_Dhat[order(cluster_x,decreasing = T)])
        Parameter=c(Parameter,parameter_name[order(cluster_x,decreasing = T)])
        STis=c(STis,Sobol_measures[[time_id]]$S.Ti[order(cluster_x,decreasing = T)])
        V_Xis=c(V_Xis,Sobol_measures[[time_id]]$V.X.is[order(cluster_x,decreasing = T)])
        V_X_is=c(V_X_is,Sobol_measures[[time_id]]$V.Xis[order(cluster_x,decreasing = T)])
        Times=c(Times,rep(Sobol_measures[[time_id]]$time,18))
        Observation=c(Observation,rep(observables[Sobol_measures[[time_id]]$model.opt[,,1]$observation.id],18))
        V_Y=c(V_Y,rep(Sobol_measures[[time_id]]$V.Y,18))
        Cluster=c(Cluster,cluster_member[order(cluster_x,decreasing = T)])
}
      , error=function(e){cat("ERROR :",conditionMessage(e), "\n")}) 
      }}
  All_measures[[readout_id]]=Sobol_measures
} 

# Aggregate everything to one. 
Sobol_df_all=data.frame(Observation,CoV=CoV,Times,Parameter,Cluster,Sis,STis,V_Xis,V_X_is,V_Y,Runs)
Sobol_df_all$Parameter=factor(Sobol_df_all$Parameter, levels = parameter_name[order(cluster_x,decreasing = T)])
Sobol_df_all$Observation=factor(Sobol_df_all$Observation, levels = unique(Observation))
data_to_plot=Sobol_df_all

# plotting all the estimates
plot_all=ggplot(Sobol_df_all,aes(x=factor(Times)))+geom_boxplot(aes(y=Sis,col=Parameter))+theme_classic()+theme_Publication(16)+facet_grid(Observation~Parameter,scales='free')+theme(axis.text.x = element_text(angle = 60, hjust = 1,color='black'))+theme(legend.position = 'bottom')
plot_all=plot_all+xlab('Time [min]')+ylab('First order Sobol index')+scale_colour_manual(values = cbbPalette[clust.df$cluster],guide=FALSE)+theme(axis.text.y=element_text(color='black'),strip.background = element_rect(fill = NA, color = "black"),panel.background = element_rect(fill = NA, color = "black",linetype = 3,size=0.2),panel.spacing = unit(0, "mm") )

# Supplementary plot!
tikz(paste0(man_dir_supplement,'/SI5-figb.tex'),width=13,height =8)
print(plot_grid(p_dendro+theme(legend.position = 'none')+theme(plot.margin = unit(c(0, 0, 0, 0), "cm")), plot_all+theme(plot.margin = unit(c(0, 0, 0, 0), "cm")), ncol=1,rel_heights = c(0.3, 0.7)))
dev.off()


# only using medians
Sobol_df=Sobol_df_all%>%group_by(Times,Parameter,Observation)%>%summarise(median_Sis=median(Sis),Cluster=unique(Cluster),CoV=unique(CoV))

# Make plot
blank<-rectGrob(gp=gpar(col="white"))  # a blank region
p_plot=ggplot(Sobol_df,aes(x=factor(Times)))+geom_point(aes(y=median_Sis))+theme_classic()+facet_grid(Observation~Parameter,scales='free')+theme(axis.text.x = element_text(angle = 45, hjust = 1))+theme(legend.position = 'bottom')
p_plot=p_plot+xlab('Time [min]')+ylab('Si (median)')+scale_colour_manual(values = cbbPalette[clust.df$cluster],guide=FALSE)

# ONLY chosing the relevant readouts. 
Sobol_df_reduced=Sobol_df[Sobol_df$Observation%in%c('$V_C$','$I_M$','$I_S$'),]
p_plot_reduced2=ggplot(Sobol_df_reduced,aes(x=factor(Times)))+geom_point(aes(y=median_Sis))+theme_classic()+facet_grid(Observation~Parameter,scales='free')+theme(axis.text.x = element_text(angle = 45, hjust = 1))+theme(legend.position = 'bottom')
p_plot_reduced2=p_plot_reduced2+aes(col = as.factor(Parameter))+scale_colour_manual(values = cbbPalette[clust.df$cluster],guide=FALSE)
blank<-rectGrob(gp=gpar(col="white")) 
p_plot_reduced2=p_plot_reduced2+xlab('Time [min]')+ylab('First order Sobol index')

## Only colouring the largest Sobol index per time point for each readout
Top_two=Sobol_df_reduced%>%group_by(Observation,Times)%>%summarize(max_p=Parameter[which.max(median_Sis)],max_clu=Cluster[order(median_Sis,decreasing=T)[c(1)]],s_max_clu=Cluster[order(median_Sis,decreasing=T)[c(2)]],s_max_p=Parameter[order(median_Sis,decreasing=T)[c(2)]])

Sobol_df_relevant2=Sobol_df_reduced
Sobol_df_relevant2$alpha=NaN;
Sobol_df_relevant3=Sobol_df_relevant2

colours_for_plot=c('grey60')
all_clus=c('NaN')
for(i in unique(Top_two$Observation)){
  rel_clus=c(Top_two[Top_two$Observation==i,]$max_clu)
  
  Sobol_df_relevant2$median_Sis[Sobol_df_relevant2$Observation==i&!Sobol_df_relevant2$Cluster%in%rel_clus]=NaN
  
  all_clus=c(all_clus,unique(rel_clus));
  for (k in unique(rel_clus)){
    colours_for_plot=c(colours_for_plot,unique(cbbPalette[clust.df$cluster][clust.df$cluster%in%(k+1)]))
    
    Sobol_df_relevant3$alpha[Sobol_df_relevant3$Observation==i&Sobol_df_relevant3$Cluster%in%k]=k
    
  }
  
}

# Final plot used in main text
Sobol_df_relevant3$alpha=factor(Sobol_df_relevant3$alpha,levels=all_clus)
p_plot_reduced3=ggplot(Sobol_df_relevant3,aes(x=factor(round(Times))))+geom_point(aes(y=median_Sis,col=factor(alpha)))+theme_classic()+theme_Publication(18)+facet_grid(Observation~Parameter,scales='free')+theme(axis.text.x = element_text(angle = 60, hjust = 1,color='black'))+theme(legend.position = 'bottom')
p_plot_reduced3=p_plot_reduced3+scale_colour_manual(values=colours_for_plot,guide=FALSE)
p_plot_reduced3=p_plot_reduced3+theme(axis.text.y=element_text(color='black'),panel.background = element_rect(fill = NA, color = "black",linetype = 3,size=0.2),panel.spacing = unit(0, "mm") )
blank<-rectGrob(gp=gpar(col="white")) 
p_plot_reduced3=p_plot_reduced3+xlab('Time [min]')+ylab('First-order Sobol index (median)')

# Visualise the plot. 
plot_grid(p_dendro+theme(legend.position = 'none')+theme(plot.margin = unit(c(0, 0, 0, 0), "cm")), p_plot_reduced3+theme(plot.margin = unit(c(0, 0, 0, 0), "cm")), ncol=1,rel_heights = c(0.3, 0.7))


# Save the .tex for the plot
tikz(paste0(man_dir5,'/Fig5.tex'),width=15,height =7,pointsize = 20)
plot_grid(p_dendro+theme(legend.position = 'none')+theme(plot.margin = unit(c(0, 0, 0, 0), "cm")), p_plot_reduced3+theme(plot.margin = unit(c(0, 0, 0.1, 0.1), "cm"),strip.background=element_rect()), ncol=1,rel_heights = c(0.3, 0.7))
dev.off()

# Save data used for plotting
dir.create(paste0(man_dir5,'/data/'))
data_for_figs=list(data_all=Sobol_df_all,data_summary=Sobol_df,dendr_data=dendr)
save(data_for_figs,file=paste0(man_dir5,'/data/Sobol.RData'))
