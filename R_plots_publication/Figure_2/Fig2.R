## Generates things needed for Figure 2 in the Manuscript
## Lekshmi Dharmarajan
## CSB, 2017

# libraries needed

library(R.matlab) # Matlab-> R
library(ggplot2)  # PLot
library(gridExtra) # gridlike
library(grid)  # gridlike
require(tikzDevice) # Tex figures
library(xtable) # Making tables
library(RColorBrewer) # Define my colors
library(reshape2)   # for melt
library(plyr)  # table manipulation
library(data.table) # data table

# Colours
my_palette2 =c("#009E73","#0072B2","#D55E00")
my_palette1 = c("#009E73","#0072B2","#D55E00","#999999")
my_palette1_1 = c("#999999")

# My theme for the paper.
theme_Publication <- function(base_size=14, base_family="arial") {
  (theme_classic(base_size = base_size)+ theme(text = element_text(),
                                               axis.title = element_text(size = rel(1)),
                                               axis.title.y = element_text(angle=90,vjust =2),
                                               axis.title.x = element_text(vjust = -0.2),
                                               axis.text = element_text(colour='black'), 
                                               axis.line = element_line(colour="black",size=base_size/30),
                                               axis.ticks = element_line(size=base_size/30),
                                               legend.key = element_rect(colour = NA),
                                               legend.position = "bottom",
                                               legend.direction = "horizontal",
                                               legend.key.size= unit(0.4, "cm"),
                                               legend.margin = unit(0, "cm"),
                                               legend.title = element_text(),
                                               plot.margin=unit(c(1.5,1.5,1.5,1.5),"mm"),
                                               strip.background=element_blank(),
                                               strip.text = element_text()
  ))
}


# !! Set current directory automatically  if using RStudio else set manually to the
# directory Single_Cell_Comparison/
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
direc='../../Single_Cell_Comparison'
setwd(direc)

# !! Where to save the plot. Needs modifications by user
plot_path='../R_plots_publication/Figure_2/' # saving the plot
man_dir=plot_path;
dir.create(man_dir)

# Load Results+ Predictions
# Naive predictions

# Load Results+ Predictions
# Naive predictions
Res_naive=readMat('./Llamosi_2016/Naive_STS/Pubs_Results/STS_estimates.mat')
Predictions_naive=readMat('./Llamosi_2016/Naive_STS/Pubs_Results/plots_data/STS_estimates.mat_02-Feb-2018.mat')
# GTS predictions
Res_gts=readMat('./Llamosi_2016/Pubs_Results/GTS-Result.mat')
Predictions_gts=readMat('./Llamosi_2016/Pubs_Results/plots_data/GTS-Result.mat.mat')
# SAEM results+ predictions
res_dir='./Llamosi_2016/SAEM_MONOLIX/'
folder_name='Llamosi2016_full'
Res_saem=readMat(paste0(res_dir,'/',folder_name,'/results.mat'))
Predictions_saem=readMat(paste0(res_dir,'/',folder_name,'_SAEMPreds.mat'))

############################################################# Plotting Predictions fig2b ##################################################################
######### GET 95% QUANTILES OF THE PREDICTIONS, and PLOT
df_simu=c() # used for plotting

# naive
Preds_it=Predictions_naive$Model.prediction
colnames(Preds_it)=c(1:ncol(Preds_it))
rownames(Preds_it)=c(1:nrow(Preds_it))
Preds_it=Preds_it[!rowSums(Preds_it)==Inf,]
melted_pred1=melt(Preds_it)
colnames(melted_pred1)=c('Time','Trajectory','Prediction')
med_95=apply(Preds_it,2,median)
ql=apply(Preds_it,2,function(s) quantile(s,0.025,na.rm=T))
qu=apply(Preds_it,2,function(s) quantile(s,0.975,na.rm=T))
df_sts=data.frame(median=med_95,ql=ql,qu=qu,time=1:ncol(Preds_it),data=rep('NTS',length(med_95)))

# gts
Preds_it=Predictions_gts$Model.prediction
colnames(Preds_it)=c(1:ncol(Preds_it))
rownames(Preds_it)=c(1:nrow(Preds_it))
Preds_it=Preds_it[!rowSums(Preds_it)==Inf,]
melted_pred1=melt(Preds_it)
colnames(melted_pred1)=c('Time','Trajectory','Prediction')
med_95=apply(Preds_it,2,median)
ql=apply(Preds_it,2,function(s) quantile(s,0.025,na.rm=T))
qu=apply(Preds_it,2,function(s) quantile(s,0.975,na.rm=T))
df_gts=data.frame(median=med_95,ql=ql,qu=qu,time=1:ncol(Preds_it),data=rep('GTS',length(med_95)))

# saem
colnames(Predictions_saem$Model.prediction)=c(1:ncol(Predictions_saem$Model.prediction))
rownames(Predictions_saem$Model.prediction)=c(1:nrow(Predictions_saem$Model.prediction))
Predictions_saem$Model.prediction=Predictions_saem$Model.prediction
melted_pred1=melt(Predictions_saem$Model.prediction)
colnames(melted_pred1)=c('Time','Trajectory','Prediction')
med_95=apply(Predictions_saem$Model.prediction,2,median)
ql=apply(Predictions_saem$Model.prediction,2,function(s) quantile(s,0.025,na.rm=T))
qu=apply(Predictions_saem$Model.prediction,2,function(s) quantile(s,0.975,na.rm=T))
df_SAEM=data.frame(median=med_95,ql=ql,qu=qu,time=1:ncol(Predictions_saem$Model.prediction),data=rep(paste0('SAEM'),length(med_95)))

# data
raw_Data=read.table(paste0('./Llamosi_2016/Data/Di_monolix_full.csv'),sep = ',',na.strings = 'NA',header = TRUE)
med_95=sapply(unique(raw_Data$TIME),function(s) median(raw_Data$Y[raw_Data$TIME==s],na.rm=T))
ql=sapply(unique(raw_Data$TIME),function(s) quantile(raw_Data$Y[raw_Data$TIME==s],0.025,na.rm=T))
qu=sapply(unique(raw_Data$TIME),function(s) quantile(raw_Data$Y[raw_Data$TIME==s],0.975,na.rm=T))
df3=data.frame(median=med_95,ql=ql,qu=qu,time=unique(raw_Data$TIME),data=rep('Data',length(med_95)))


# aggregate everything
df_simu=rbind(df_simu,df_sts,df_SAEM,df_gts,df3)

# Input used for the experiment 
inp=readMat('../R_plots_publication/Figure_2/INPUT_TO_PLOT.mat')

inp_df=data.frame(TIME=t(inp$T),U_C=(colSums(inp$U.C)),U_V=(colSums(inp$U.V)))


# Setting for the plot
lnsize=0.7

# Make the plot and view it
# linesize
lnsize=0.7 

# Setting for the plot
lnsize=0.7

# Make the plot and view it
# linesize
lnsize=0.7 

pSimu=ggplot(df_simu[df_simu$data!='Data',], aes(x=time/60, y=median),lty=2) +geom_line(aes(y=ql,col=data),lty=3,lwd=lnsize)+geom_line(aes(y=qu,col=data,lty=factor(3)),lwd=lnsize)+geom_line(aes(col=data,lty=factor(2)),lwd=lnsize)+theme_classic()+xlab('Time [h]')+ylab('Protein, y(t) [AU]')

# Add shaded region for the data
pSimu=pSimu+geom_ribbon(data=df3,aes(ymax=qu,ymin=ql,fill=factor(data)),alpha=0.3)
# Add color manual
pSimu=pSimu+scale_colour_manual(values=my_palette1,guide=FALSE)+scale_fill_manual(values=my_palette1_1,labels='95\\% data quantiles')+scale_linetype_manual(values=c(1,3),labels=c('Median','95\\% prediction quantiles'))
# Add input  and data lines
pSimu=pSimu+geom_polygon(data=inp_df,aes(x=TIME/60,y=U_C*500-1000),col='black',fill='black')+annotate("text",x=9.9,y=-1000,label='$u_c$',fontface='plain')
pSimu+geom_line(data=df_simu[df_simu$data=='Data',], aes(x=time/60, y=median),col='black')
pSimu=pSimu+geom_line(data=df_simu[df_simu$data=='Data',], aes(x=time/60, y=median),col='black')
# make clean, format legend
pSimu=pSimu+theme_Publication(10)+theme(legend.position=c(0.5,0.97),legend.direction='horizontal', legend.box = "horizontal",legend.margin = unit(0,"cm"),legend.title = element_blank())

# view it.
print(pSimu)

################################################################## PLotting Fig 2c#########################################################################################################
gts_noise=cbind(rep(sqrt(Res_gts$sigma2)*(Res_gts$theta.hat[1]),325),rep(log(sqrt(Res_gts$sigma2)*exp(Res_gts$theta.hat[2])),325))
Params=rbind(cbind(Res_naive$betai.sts,'1',1:325),cbind((Res_saem$indexp.param[,c(1,4,3)]),t(matrix(c(Res_saem$Params[,,1]$res[1],log(Res_saem$Params[,,1]$res[2])),2,325)),'2',1:325),cbind(Res_gts$betahati,gts_noise,'3',1:325))
X=data.frame(Params)
colnames(X)=c('ln $\\tilde{k}_{mi}$','ln $\\tilde{g}_{mi}$','ln $\\tilde{g}_{pi}$','$\\tilde{\\sigma \\cdot \\theta}^0$','ln $\\tilde{\\sigma \\cdot \\theta}^1$','Data','ID')
X[,1:5]=apply(X[,1:5],2,as.numeric)
paras=X
paras[,1:5]=apply(paras[,1:5],2,as.numeric)

#### plotting thetas
paras2=paras
paras2[,c(5)]=(paras2[,c(5)])

# Only the kinetic parameters
colnames(paras2)=colnames(X)=c('ln $\\hat{k}_{mi}$','ln $\\hat{g}_{mi}$','ln $\\hat{g}_{pi}$','$\\hat{\\sigma} \\cdot \\hat{\\theta}^0$','ln $\\hat{\\sigma} \\cdot \\hat{\\theta}^1$','Data','ID')
xx <- with(paras2, data.table(id=1:nrow(paras2), group=Data, paras2[,c(1,2,3)]))
yy <- melt(xx,id=1:2, variable.name="H", value.name="xval")
setkey(yy,id,group)
ww <- yy[,list(V=H,yval=xval),key="id,group"]
zz <- yy[ww,allow.cartesian=T]
setkey(zz,H,V,group)
zz <- zz[,list(id, group, xval, yval, min.x=min(xval),min.y=min(yval),
               range.x=diff(range(xval)),range.y=diff(range(yval))),by="H,V"]
d  <- zz[H==V,list(x=density(xval,bw = 0.0001)$x,
                   y=density(xval)$y/max(density(xval)$y)),
         by="H,V,group"]
i=1


# Defining the breaks for the x axis. 
equal_breaks <- function(n = 3, s = 0.05, ...){
  function(x){
    # rescaling
    d <- s * diff(range(x)) / (1+2*s)
    round(seq(min(x)+d, max(x)-d, length=n))
  }
}

# plot marginal distributions
p2=ggplot()+
  geom_line( data=d, aes(x=x, y=y, color=factor(group)),size=0.4)+
  facet_wrap(~H, scales="free",ncol=1,shrink = T,strip.position = "bottom")+theme_classic()+
  labs(x="", y="")+scale_fill_manual(values=my_palette1,name='Data')+scale_colour_manual(values=my_palette2,name="Method",labels=c('NTS','SAEM','GTS'))
p2=p2+theme_Publication(8)+theme(strip.placement = 'outside')
# Adding the breaks and legend formating
p2=p2+scale_x_continuous(breaks=equal_breaks(n=4, s=0.01),expand = c(0.01, 0))
p2_a=p2+theme(plot.margin=margin(0.6,0.2,0,0,'cm'),legend.box.margin = margin(0,0,0,0),legend.justification ='right',legend.position=c(0.97,1.2))

# NOISE
xx <- with(paras2, data.table(id=1:nrow(paras2), group=Data, paras2[,c(4,5)]))
yy <- melt(xx,id=1:2, variable.name="H", value.name="xval")
setkey(yy,id,group)
ww <- yy[,list(V=H,yval=xval),key="id,group"]
zz <- yy[ww,allow.cartesian=T]
setkey(zz,H,V,group)
zz <- zz[,list(id, group, xval, yval, min.x=min(xval),min.y=min(yval),
               range.x=diff(range(xval)),range.y=diff(range(yval))),by="H,V"]
d  <- zz[H==V,list(x=density(xval,bw = 0.0001)$x,
                   y=density(xval)$y/max(density(xval)$y)),
         by="H,V,group"]
i=1

p2=ggplot()+
  geom_line( data=d, aes(x=x, y=y, color=factor(group)),size=0.4)+
  facet_wrap(~H, scales="free",ncol=1,shrink = T,strip.position = "bottom")+theme_classic()+
  labs(x="", y="")+scale_fill_manual(values=my_palette1,name='Data')+scale_colour_manual(values=my_palette2,name="Method",labels=c('NTS','SAEM','GTS'))
p2=p2+theme_Publication(8)+theme(strip.placement = 'outside')
# Adding the breaks and legend formating
p2=p2+scale_x_continuous(breaks=equal_breaks(n=4, s=0.01),expand = c(0.01, 0))
p2_b=p2+theme(plot.margin=margin(0.6,0.2,0,0,'cm'),legend.box.margin = margin(0,0,0,0),legend.justification ='right',legend.position=c(0.97,0.0),legend.direction = 'vertical')

# Put the panel together and view it
lay <- rbind(c(NA,NA,1,2),
             c(NA,NA,1,2),
             c(3,3,1,2),
             c(3,3,1,NA))
grid.arrange(p2_a+ylab('Density'),p2_b,pSimu,layout_matrix=lay)
grid.text("b",  x=0.41, y=0.96, gp=gpar(family='Helvetica',fontface='bold',fontsize=10)) 
grid.text("c",  x=0.02,y=0.35, gp=gpar(family='Helvetica',fontface='bold',fontsize=10)) 


# Save the data used

Fig2b_data=list(datasets=df_simu,input_profile=inp_df)
Fig2c_data=list(Parameters=paras)
Fig2_data=list(Fig2b_data=Fig2b_data,Fig2c_data=Fig2c_data)
save(Fig2_data,file='./R_plots_publication/Figure_2/FigData/Fig2.RData')
save(Fig2_data,file=paste0(man_dir,'/Fig2.RData')) # in manuscript folder

###################################### Save the two subfigures in the Manuscript folder
ttikz(paste0(man_dir,'Fig2b.tex'), width = 2.75,height = 2,pointsize = 10) #define plot name size and font size
print(pSimu)
dev.off()

tikz(paste0(man_dir,'Fig2c.tex'), width = 2.75,height = 4,pointsize = 10) #define plot name size and font size
lay <- rbind(c(1,2),
             c(1,2),
             c(1,2),
             c(1,NA))
grid.arrange(p2_a+ylab('Probability density'),p2_b,layout_matrix=lay)
dev.off()

