echo " Compiling figure 2"
## Get the figures in suplementary text 2 (a)
cd Figure_2
xelatex -pdf ---enable-write18  -shell-escape  Compilepanel_Fig2.tex

echo " Compiling figure 3"
cd ../Figure_3
## Get the figures in suplementary text 4 (a)
xelatex -pdf -shell-escape  Compilepanel_Fig3.tex


echo " Compiling figure 4"
cd ../Figure_4
## Get the figures in suplementary text 4 (b)
xelatex -pdf -shell-escape  Compilepanel_Fig4.tex 

echo " Compiling figure 5"
cd ../Figure_4
## Get the figures in suplementary text 5 (b)
xelatex -pdf -shell-escape Compilepanel_Fig5.tex 


echo " deleting all logs and aux files"
cd ..
find . -name \*.aux -type f -delete
find . -name \*.auxlock -type f -delete
find . -name \*.fdb_latexmk -type f -delete
find . -name \*.fls -type f -delete
find . -name \*.md5 -type f -delete
find . -name \*.dpth -type f -delete
find . -name \*.log -type f -delete

echo 'Moving files to Main_figs folder'
mv Figure_5/pdfs/* ./Main_figs/Fig5.pdf
mv Figure_4/pdfs/* ./Main_figs/Fig4.pdf
mv Figure_3/pdfs/* ./Main_figs/Fig3.pdf
mv Figure_2/pdfs/* ./Main_figs/Fig2.pdf
scp -r Figure_1/pdfs/* ./Main_figs/Fig1.pdf