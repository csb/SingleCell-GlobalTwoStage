This contains all the things related to the figures used in the manuscript

a) scripts: R scripts  are found in the respective folder
b) The supplementary figures can be generated using the accompanying .Rmd files in R and the figures are stored in the Supplementary_figs folder.
c) The generated figures are either stored as pdfs/ or .Tex files. The .Tex files are compiled to pdfs using the TeX2pdfs scripts. The results are stored in the pdfs folder within each figure folder. 

