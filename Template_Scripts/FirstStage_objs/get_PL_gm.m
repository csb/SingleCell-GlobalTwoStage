function PL=get_PL_gm(sim_struct,noise_params,data,indexes)
%%%------------------------------------------------------------------------
%
%   function get_PL_gm.m
%	Gets the pooled PL (pseudolikelihood) likelihood. Implementation is based on 
%	"Some Simple Methods for Estimating Intraindividual Variability in Nonlinear Mixed
%	Effects Models"
%	Author(s): Marie Davidian and David M. Giltinan
%	Source: Biometrics, Vol. 49, No. 1 (Mar., 1993), pp. 59-73
%   https://www.jstor.org/stable/2532602?seq=1#page_scan_tab_contents 
%  	Please see the  appendix of the paper
%   or Section 2.4.2 of Nonlinear Models for Repeated Measurement Data, Chapman & Hall, London
%   1996 Reprint edition .
%


%  INPUT==> sim_struct:  structure containing the simulated data for all
%                       cells at their estimated kinetic parameters from
%                       last iteration
%           noise_params: Noise parameters, theta in the model
%			data: data.Y
%           indexes: cell IDs 


%  OUTPUT==> PL: objective function (pooled PL, in log)
%   Lekshmi Dharmarajan, July 2017
%   Checked January 2018

%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

PL_i=zeros(1,length(indexes)); % initialising the individual PLs

% Getting the ingredients needed, residuals, noise model, and geometric mean of log noise model
% for each cell
for_pl=struct();
parfor i = 1: length(indexes)
[for_pl(i).res,for_pl(i).h,for_pl(i).gm]=WLS_gm(sim_struct(i).f_t,noise_params,data.Y(data.ID==indexes(i),:));
end


% If the log geometric mean does not exist for any cell, then set the objective function to be infinite
if any(cell2mat({for_pl.gm})==Inf)
    PL=Inf;
else
% Calculate the objective function for each cell
g_dot=log(geomean(cell2mat({for_pl.h}')));
parfor i = 1: length(indexes)
	PL_i(i)=sum(sum((for_pl(i).res.*exp(g_dot)./for_pl(i).h).^2));
end

% Pooled over all the cells
PL=log(sum(PL_i)); 

end
end
