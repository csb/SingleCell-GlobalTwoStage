-----------------------------------------------------------------------------------
# How to modify the scripts.
-----------------------------------------------------------------------------------

These contain some script skeletons that can enable the user to modify the scripts. The scripts 
used in the manuscript are based on these... though we had modified some additional lines 
based on the data/ model used. 

The folder is composed as follows: 
1. FirstStage and SecondStage.m main scripts which have to be modified. 
Look for lines with comments related to 'USER must modify'. 

FirstStage_TEMPLATE.m: 
Functions that the user must provide to run the script: 
	a) ``model_fn_user``: Model to integrate
	b) ``noise_model_user``: measurement error model
	c) ``getFIM_user``: obtain the FIM/Hessian at the optimum
	d) Modify the Data/ Model folders. 
	e) Modify the format of the results to be stored and where it should be stored. 
	f) Provide links to ODE  integrator and optimiser. The current format is suitable for NLOPT. 
	
All objective functions used in first stage are in `FirstStage_objs``. All the functions in this
folder take the parameter values in the ln scale and he transformation is made in the 
respective scripts. 

SecondStage_TEMPLATE.m:
Load the result from FirstStage. Minimal information that is required is: 
	a)  FIM: variable name ``C_inv``, a multidimensional array with each being a square matrix of the
	dimension of the number of parameters. the number of such matrices should be the number of cells)
	b) individual estiamtes: Variable name ``betaihat``, an array with number of cells as number of rows and number of parameters as
	columns. 
	b) User should modify where to save the file. 