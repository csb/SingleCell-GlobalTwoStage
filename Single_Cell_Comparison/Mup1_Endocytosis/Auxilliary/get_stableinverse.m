function [InvOut,err]=get_stableinverse(E)
% See discussions on https://stats.stackexchange.com/questions/50947/numerical-instability-of-calculating-inverse-covariance-matrix

% Invert using Cholesky Factorization
[U,err]=chol(E);
if(err==0)
InvOut=inv(U)*inv(U)';
else
    
disp('WARNING:Cholesky inverse failed, issues in matrix inversion likely');
% If matrix is working precision
if rcond(E)==0 
        E=E+eye(size(E,1))*10^-10;
end
% Invert using LU decomposition
InvOut=inv(E);

    
end
