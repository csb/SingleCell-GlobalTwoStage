%% This script checks if all the cells have been fitted
% And if not rerun the ones that were not fit.
% D Lekshmi 
dirname='./MultiStart_Results/Multistart_ResultsNLMD_100'; 
files=dir(dirname);

tmp_params=struct;
% concatenate structures 
for i=1:(length(files)-2)
load(sprintf('%s/%s',dirname,files(i+2).name))
tmp_params(i).cellID= MS_params.cellID;
tmp_params(i).retcode=MS_params.retcode;
end

total_ids=1:1907;
cell_ids=setdiff(total_ids,cat(1,tmp_params.cellID));

%%
for i=1:length(cell_ids)
system(sprintf('qsub -t %d submit_multistart100.sh',cell_ids(i)))

end
