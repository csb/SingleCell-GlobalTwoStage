function [model] = model_endocytosis()

    % Model describing the Mup1 Endocytosis.See supplementary for more
    % details
   
    %%
    % STATES
    
    syms V_C V_M N_E I_C I_M n_E
    
    model.sym.x = [V_C V_M N_E I_C I_M n_E];

    %%
    % PARAMETERS
    
    syms N_C  BBETA GGAMMA k1 k2 k3 k4 k5 K_M  ...
			alpha_R T_P T_E alpha_E mu_C mu_M V_EM V_EC DDELTA ...
            V_C00 V_M00 N_E00 I_C00 I_M00 n_E00 tm u_in
    
    model.sym.p = [ k2 k3 k4 k5 K_M alpha_R T_P T_E alpha_E mu_C mu_M V_EM V_EC DDELTA V_C00 V_M00  I_C00 I_M00];
    
    
    % Sensitivites will NOT be computed for these.
    model.sym.k = [tm u_in N_C  BBETA GGAMMA k1 N_E00 n_E00];
    
    
    %%
    % INPUT
    syms t

    u_P = u_in+(1-u_in)*exp(-log(2)/T_P*(t-tm));
    u_E = alpha_E+(1-alpha_E)*(1-u_in)*(1-exp(-log(2)/T_E*(t-tm)));
    
    % Algebraic relations
    v_P = k4*V_C*u_P;
    alpha_M2 = (k2)*(1-((I_M)^BBETA)/(K_M^BBETA+(I_M)^BBETA));
    mu_E = k1*N_C*u_E;
    v_E = V_EM*I_M*mu_E;

    %%
    % SYSTEM EQUATIONS
    model.sym.xdot = sym(zeros(size(model.sym.x)));
  
     %  X4=(1-alpha_M)*v_P-k3*n_CP+(1-alpha_R)*k5*(n_E); %used to be the
     %  dfe, nCP
        
    model.sym.xdot(1) = mu_C*V_C; % dVCdt
    model.sym.xdot(2) = mu_M*V_M; % dVMdt
    model.sym.xdot(3) = mu_E-(k5+k3)*N_E; %dNEdt
    model.sym.xdot(4) = v_P/V_C-k3*(I_C-I_M*V_M/V_C)-mu_C*I_C; %dICdt
    model.sym.xdot(5) = -v_E/V_M+alpha_R*k5*(n_E)/V_M+alpha_M2*(I_C*V_C-I_M*V_M-n_E)/V_M-mu_M*I_M; % dIMdt
    model.sym.xdot(6) = v_E-(k5+k3)*n_E; %dnEdt

    
    
    %%
    % INITIAL CONDITIONS
    
    model.sym.x0 = sym(zeros(size(model.sym.x)));
    
    model.sym.x0(1) =V_C00;    
    model.sym.x0(2) =V_M00;
    model.sym.x0(3) =N_E00;
    model.sym.x0(4) =I_C00; 
    model.sym.x0(5) =I_M00;
    model.sym.x0(6) =n_E00;
    %%
    % OBSERVABLES
    model.sym.y = sym(zeros(6,1));
    model.sym.y(1) = V_C;
    model.sym.y(2) = V_M;

    % Algebraic relations relating spots to number of endosomes
    V_E0=V_EC*N_C;                     % An initial estimate of the volume of endosomes
    I_E0=(n_E)/V_E0 ;                  % Initial estimate of the intensity in endosomes
    V_CP0=V_C-V_M-V_E0	;              % Initial cytoplasmic volume			
    I_CP0=(I_C*V_C-I_M*V_M-n_E)/V_CP0; % Initial estmate of cytoplasmic intensity        				
    F_S = (I_E0)^GGAMMA/((DDELTA*I_CP0)^GGAMMA+(I_E0)^GGAMMA);	% detection
    
    V_S = F_S*V_E0;		% Spot volume	
    %N_S=F_S*N_E;       %	Number of spots related to number of endosomes, not observed.		
    I_S = F_S*n_E/V_S;  % Detected intensity			
    I_CP = ((I_C*V_C-I_M*V_M-n_E)+(1-F_S)*(n_E))/(V_C-V_M-V_S); %detected cytoplasmic intensity
    model.sym.y(3) = I_C;
    model.sym.y(4) = I_M;
    model.sym.y(5) = I_S;
    model.sym.y(6) = I_CP;
     
end
