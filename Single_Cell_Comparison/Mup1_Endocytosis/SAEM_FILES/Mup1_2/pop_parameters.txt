******************************************************************
*      Mup1_2.mlxtran
*      December 31, 2017 at 03:01:35
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

                         parameter 
k2l_pop                :     -1.27 
k3l_pop                :     -1.72 
k4l_pop                :     -2.29 
k5l_pop                :     -8.74 
K_Ml_pop               :     -6.16 
alpha_Rl_pop           :    -0.445 
T_Pl_pop               :      3.87 
T_El_pop               :      1.14 
alpha_El_pop           :     -19.6 
mu_Cl_pop              :     -7.52 
mu_Ml_pop              :     -6.64 
V_EMl_pop              :      1.52 
V_ECl_pop              :      4.38 
deltal_pop             :    -0.233 
VC0l_pop               :      9.09 
VM0l_pop               :      8.34 
NE0l_pop               :       -20 
IC0n_pop               :      2.04 
IM0n_pop               :      1.08 
nE0l_pop               :       -20 

omega_k2l              :     0.851 
omega_k3l              :     0.631 
omega_k4l              :     0.643 
omega_k5l              :     0.395 
omega_K_Ml             :     0.656 
omega_alpha_Rl         :     0.354 
omega_T_Pl             :     0.288 
omega_T_El             :      0.68 
omega_alpha_El         :      1.98 
omega_mu_Cl            :      2.47 
omega_mu_Ml            :      1.12 
omega_V_EMl            :     0.367 
omega_V_ECl            :     0.607 
omega_deltal           :     0.737 
omega_VC0l             :     0.401 
omega_VM0l             :     0.288 
omega_NE0l             :         0 
omega_IC0n             :     0.611 
omega_IM0n             :     0.246 
omega_nE0l             :         0 
corr_k2l_k3l           :    -0.335 
corr_k2l_k4l           :     -0.41 
corr_k3l_k4l           :     0.927 
corr_k2l_k5l           :    0.0415 
corr_k3l_k5l           :    -0.661 
corr_k4l_k5l           :     -0.69 
corr_k2l_K_Ml          :     0.255 
corr_k3l_K_Ml          :    -0.527 
corr_k4l_K_Ml          :    -0.333 
corr_k5l_K_Ml          :     0.351 
corr_k2l_alpha_Rl      :     0.644 
corr_k3l_alpha_Rl      :    -0.159 
corr_k4l_alpha_Rl      :    -0.246 
corr_k5l_alpha_Rl      :     0.136 
corr_K_Ml_alpha_Rl     :    0.0942 
corr_k2l_T_Pl          :     0.144 
corr_k3l_T_Pl          :     0.385 
corr_k4l_T_Pl          :     0.308 
corr_k5l_T_Pl          :   -0.0161 
corr_K_Ml_T_Pl         :    -0.264 
corr_alpha_Rl_T_Pl     :      0.38 
corr_k2l_T_El          :      0.23 
corr_k3l_T_El          :    -0.529 
corr_k4l_T_El          :    -0.468 
corr_k5l_T_El          :     0.715 
corr_K_Ml_T_El         :     0.342 
corr_alpha_Rl_T_El     :     0.131 
corr_T_Pl_T_El         :    0.0017 
corr_k2l_alpha_El      :    0.0973 
corr_k3l_alpha_El      :     0.331 
corr_k4l_alpha_El      :     0.321 
corr_k5l_alpha_El      :     -0.35 
corr_K_Ml_alpha_El     :  -0.00645 
corr_alpha_Rl_alpha_El :    0.0766 
corr_T_Pl_alpha_El     :     0.615 
corr_T_El_alpha_El     :    -0.259 
corr_k2l_mu_Cl         :     0.176 
corr_k3l_mu_Cl         :    -0.106 
corr_k4l_mu_Cl         :    -0.213 
corr_k5l_mu_Cl         :     0.105 
corr_K_Ml_mu_Cl        :     0.208 
corr_alpha_Rl_mu_Cl    :     0.368 
corr_T_Pl_mu_Cl        :    0.0236 
corr_T_El_mu_Cl        :    0.0386 
corr_alpha_El_mu_Cl    :    -0.156 
corr_k2l_mu_Ml         :     0.197 
corr_k3l_mu_Ml         :    -0.132 
corr_k4l_mu_Ml         :    -0.273 
corr_k5l_mu_Ml         :     0.137 
corr_K_Ml_mu_Ml        :     0.197 
corr_alpha_Rl_mu_Ml    :     0.445 
corr_T_Pl_mu_Ml        :    0.0565 
corr_T_El_mu_Ml        :    0.0567 
corr_alpha_El_mu_Ml    :    -0.103 
corr_mu_Cl_mu_Ml       :     0.971 
corr_k2l_V_EMl         :    -0.339 
corr_k3l_V_EMl         :    -0.101 
corr_k4l_V_EMl         :     0.125 
corr_k5l_V_EMl         :    -0.153 
corr_K_Ml_V_EMl        :   -0.0896 
corr_alpha_Rl_V_EMl    :    -0.596 
corr_T_Pl_V_EMl        :    -0.343 
corr_T_El_V_EMl        :     0.196 
corr_alpha_El_V_EMl    :     -0.26 
corr_mu_Cl_V_EMl       :    -0.279 
corr_mu_Ml_V_EMl       :    -0.392 
corr_k2l_V_ECl         :    -0.135 
corr_k3l_V_ECl         :    -0.581 
corr_k4l_V_ECl         :     -0.39 
corr_k5l_V_ECl         :     0.373 
corr_K_Ml_V_ECl        :     0.385 
corr_alpha_Rl_V_ECl    :    -0.334 
corr_T_Pl_V_ECl        :    -0.527 
corr_T_El_V_ECl        :     0.413 
corr_alpha_El_V_ECl    :    -0.382 
corr_mu_Cl_V_ECl       :    -0.201 
corr_mu_Ml_V_ECl       :    -0.257 
corr_V_EMl_V_ECl       :     0.619 
corr_k2l_deltal        :     0.451 
corr_k3l_deltal        :    -0.713 
corr_k4l_deltal        :    -0.726 
corr_k5l_deltal        :      0.35 
corr_K_Ml_deltal       :     0.364 
corr_alpha_Rl_deltal   :     0.243 
corr_T_Pl_deltal       :    -0.422 
corr_T_El_deltal       :     0.242 
corr_alpha_El_deltal   :    -0.442 
corr_mu_Cl_deltal      :     0.381 
corr_mu_Ml_deltal      :     0.387 
corr_V_EMl_deltal      :   -0.0705 
corr_V_ECl_deltal      :    0.0532 
corr_k2l_VC0l          :    -0.218 
corr_k3l_VC0l          :   -0.0227 
corr_k4l_VC0l          :      0.22 
corr_k5l_VC0l          :    -0.209 
corr_K_Ml_VC0l         :  -0.00486 
corr_alpha_Rl_VC0l     :    -0.534 
corr_T_Pl_VC0l         :    -0.104 
corr_T_El_VC0l         :     0.117 
corr_alpha_El_VC0l     :    0.0618 
corr_mu_Cl_VC0l        :    -0.438 
corr_mu_Ml_VC0l        :     -0.56 
corr_V_EMl_VC0l        :     0.881 
corr_V_ECl_VC0l        :     0.527 
corr_deltal_VC0l       :    -0.218 
corr_k2l_VM0l          :    -0.204 
corr_k3l_VM0l          :   -0.0119 
corr_k4l_VM0l          :     0.234 
corr_k5l_VM0l          :    -0.215 
corr_K_Ml_VM0l         :   -0.0102 
corr_alpha_Rl_VM0l     :    -0.519 
corr_T_Pl_VM0l         :     -0.12 
corr_T_El_VM0l         :     0.107 
corr_alpha_El_VM0l     :     0.023 
corr_mu_Cl_VM0l        :     -0.46 
corr_mu_Ml_VM0l        :    -0.587 
corr_V_EMl_VM0l        :     0.879 
corr_V_ECl_VM0l        :     0.533 
corr_deltal_VM0l       :    -0.219 
corr_VC0l_VM0l         :     0.997 
corr_k2l_IC0n          :    -0.106 
corr_k3l_IC0n          :     0.355 
corr_k4l_IC0n          :     0.361 
corr_k5l_IC0n          :    -0.258 
corr_K_Ml_IC0n         :    -0.398 
corr_alpha_Rl_IC0n     :    -0.154 
corr_T_Pl_IC0n         :      0.05 
corr_T_El_IC0n         :     0.016 
corr_alpha_El_IC0n     :    0.0399 
corr_mu_Cl_IC0n        :    -0.648 
corr_mu_Ml_IC0n        :    -0.577 
corr_V_EMl_IC0n        :     0.196 
corr_V_ECl_IC0n        :    0.0133 
corr_deltal_IC0n       :    -0.474 
corr_VC0l_IC0n         :      0.23 
corr_VM0l_IC0n         :     0.244 
corr_k2l_IM0n          :    0.0631 
corr_k3l_IM0n          :    -0.128 
corr_k4l_IM0n          :     0.148 
corr_k5l_IM0n          :   0.00523 
corr_K_Ml_IM0n         :     0.578 
corr_alpha_Rl_IM0n     :     0.163 
corr_T_Pl_IM0n         :    -0.159 
corr_T_El_IM0n         :    0.0997 
corr_alpha_El_IM0n     :    -0.167 
corr_mu_Cl_IM0n        :   -0.0777 
corr_mu_Ml_IM0n        :    -0.111 
corr_V_EMl_IM0n        :    0.0956 
corr_V_ECl_IM0n        :     0.304 
corr_deltal_IM0n       :    0.0194 
corr_VC0l_IM0n         :    0.0492 
corr_VM0l_IM0n         :    0.0796 
corr_IC0n_IM0n         :    0.0586 

a1                     :       731 
a2                     :       294 
a3                     :    0.0511 
b3                     :  1.04e-10 
a4                     :    0.0632 
b4                     :  2.85e-10 
a5                     :     0.147 
b5                     :     0.607 
a6                     :    0.0452 
b6                     :    0.0154 

correlation matrix (IIV)
k2l           1                                                    
k3l       -0.34       1                                                 
k4l       -0.41    0.93       1                                              
k5l        0.04   -0.66   -0.69       1                                           
K_Ml       0.26   -0.53   -0.33    0.35       1                                        
alpha_Rl   0.64   -0.16   -0.25    0.14    0.09       1                                     
T_Pl       0.14    0.38    0.31   -0.02   -0.26    0.38       1                                  
T_El       0.23   -0.53   -0.47    0.72    0.34    0.13       0       1                               
alpha_El    0.1    0.33    0.32   -0.35   -0.01    0.08    0.61   -0.26       1                            
mu_Cl      0.18   -0.11   -0.21     0.1    0.21    0.37    0.02    0.04   -0.16       1                         
mu_Ml       0.2   -0.13   -0.27    0.14     0.2    0.44    0.06    0.06    -0.1    0.97       1                      
V_EMl     -0.34    -0.1    0.12   -0.15   -0.09    -0.6   -0.34     0.2   -0.26   -0.28   -0.39       1                   
V_ECl     -0.13   -0.58   -0.39    0.37    0.38   -0.33   -0.53    0.41   -0.38    -0.2   -0.26    0.62       1                
deltal     0.45   -0.71   -0.73    0.35    0.36    0.24   -0.42    0.24   -0.44    0.38    0.39   -0.07    0.05       1             
VC0l      -0.22   -0.02    0.22   -0.21      -0   -0.53    -0.1    0.12    0.06   -0.44   -0.56    0.88    0.53   -0.22       1          
VM0l       -0.2   -0.01    0.23   -0.21   -0.01   -0.52   -0.12    0.11    0.02   -0.46   -0.59    0.88    0.53   -0.22       1       1       
IC0n      -0.11    0.35    0.36   -0.26    -0.4   -0.15    0.05    0.02    0.04   -0.65   -0.58     0.2    0.01   -0.47    0.23    0.24       1    
IM0n       0.06   -0.13    0.15    0.01    0.58    0.16   -0.16     0.1   -0.17   -0.08   -0.11     0.1     0.3    0.02    0.05    0.08    0.06       1 


Population parameters estimation...

Elapsed time is 3.71e+05 seconds. 
CPU time is 1.78e+06 seconds. 
