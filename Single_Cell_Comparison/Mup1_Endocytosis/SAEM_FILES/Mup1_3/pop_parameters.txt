******************************************************************
*      Mup1_3.mlxtran
*      December 30, 2017 at 06:49:58
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

                         parameter 
k2l_pop                :     -0.85 
k3l_pop                :     -3.79 
k4l_pop                :     -4.21 
k5l_pop                :      -3.5 
K_Ml_pop               :     -4.34 
alpha_Rl_pop           :     -18.7 
T_Pl_pop               :      1.05 
T_El_pop               :     0.985 
alpha_El_pop           :      -5.4 
mu_Cl_pop              :     -6.64 
mu_Ml_pop              :     -6.63 
V_EMl_pop              :      1.54 
V_ECl_pop              :       5.2 
deltal_pop             :     0.545 
VC0l_pop               :      9.08 
VM0l_pop               :      8.35 
NE0l_pop               :       -20 
IC0n_pop               :    -0.499 
IM0n_pop               :      1.11 
nE0l_pop               :       -20 

omega_k2l              :     0.339 
omega_k3l              :     0.232 
omega_k4l              :     0.279 
omega_k5l              :     0.283 
omega_K_Ml             :     0.235 
omega_alpha_Rl         :     0.278 
omega_T_Pl             :      0.93 
omega_T_El             :     0.801 
omega_alpha_El         :      1.18 
omega_mu_Cl            :      1.23 
omega_mu_Ml            :      1.09 
omega_V_EMl            :      0.36 
omega_V_ECl            :     0.516 
omega_deltal           :     0.486 
omega_VC0l             :     0.404 
omega_VM0l             :      0.29 
omega_NE0l             :         0 
omega_IC0n             :      1.15 
omega_IM0n             :     0.262 
omega_nE0l             :         0 
corr_k2l_k3l           :     0.385 
corr_k2l_k4l           :   -0.0836 
corr_k3l_k4l           :     0.637 
corr_k2l_k5l           :     0.165 
corr_k3l_k5l           :     0.186 
corr_k4l_k5l           :     0.158 
corr_k2l_K_Ml          :      0.22 
corr_k3l_K_Ml          :   0.00158 
corr_k4l_K_Ml          :     0.228 
corr_k5l_K_Ml          :     0.147 
corr_k2l_alpha_Rl      :   -0.0171 
corr_k3l_alpha_Rl      :    0.0189 
corr_k4l_alpha_Rl      :     0.153 
corr_k5l_alpha_Rl      :     0.235 
corr_K_Ml_alpha_Rl     :       0.1 
corr_k2l_T_Pl          :    -0.386 
corr_k3l_T_Pl          :    0.0888 
corr_k4l_T_Pl          :     0.199 
corr_k5l_T_Pl          :    -0.171 
corr_K_Ml_T_Pl         :    -0.152 
corr_alpha_Rl_T_Pl     :      0.31 
corr_k2l_T_El          :    -0.378 
corr_k3l_T_El          :   -0.0164 
corr_k4l_T_El          :     0.285 
corr_k5l_T_El          :    -0.126 
corr_K_Ml_T_El         :     0.115 
corr_alpha_Rl_T_El     :    0.0238 
corr_T_Pl_T_El         :     0.718 
corr_k2l_alpha_El      :      0.63 
corr_k3l_alpha_El      :     0.467 
corr_k4l_alpha_El      :     0.163 
corr_k5l_alpha_El      :     0.223 
corr_K_Ml_alpha_El     :   -0.0945 
corr_alpha_Rl_alpha_El :   -0.0624 
corr_T_Pl_alpha_El     :    -0.194 
corr_T_El_alpha_El     :    -0.489 
corr_k2l_mu_Cl         :    -0.115 
corr_k3l_mu_Cl         :    -0.361 
corr_k4l_mu_Cl         :    -0.289 
corr_k5l_mu_Cl         :    -0.128 
corr_K_Ml_mu_Cl        :    0.0759 
corr_alpha_Rl_mu_Cl    :     0.168 
corr_T_Pl_mu_Cl        :     0.107 
corr_T_El_mu_Cl        :    0.0636 
corr_alpha_El_mu_Cl    :    -0.442 
corr_k2l_mu_Ml         :   -0.0912 
corr_k3l_mu_Ml         :     -0.36 
corr_k4l_mu_Ml         :    -0.296 
corr_k5l_mu_Ml         :    -0.126 
corr_K_Ml_mu_Ml        :    0.0896 
corr_alpha_Rl_mu_Ml    :      0.17 
corr_T_Pl_mu_Ml        :    0.0849 
corr_T_El_mu_Ml        :    0.0376 
corr_alpha_El_mu_Ml    :    -0.433 
corr_mu_Cl_mu_Ml       :     0.999 
corr_k2l_V_EMl         :    -0.234 
corr_k3l_V_EMl         :     0.228 
corr_k4l_V_EMl         :     0.428 
corr_k5l_V_EMl         :   -0.0619 
corr_K_Ml_V_EMl        :    -0.488 
corr_alpha_Rl_V_EMl    :    -0.423 
corr_T_Pl_V_EMl        :     0.087 
corr_T_El_V_EMl        :     0.195 
corr_alpha_El_V_EMl    :     0.218 
corr_mu_Cl_V_EMl       :      -0.4 
corr_mu_Ml_V_EMl       :    -0.424 
corr_k2l_V_ECl         :    -0.216 
corr_k3l_V_ECl         :     0.257 
corr_k4l_V_ECl         :      0.39 
corr_k5l_V_ECl         :   -0.0632 
corr_K_Ml_V_ECl        :    -0.255 
corr_alpha_Rl_V_ECl    :    -0.485 
corr_T_Pl_V_ECl        :   -0.0352 
corr_T_El_V_ECl        :     0.113 
corr_alpha_El_V_ECl    :     0.321 
corr_mu_Cl_V_ECl       :    -0.488 
corr_mu_Ml_V_ECl       :    -0.502 
corr_V_EMl_V_ECl       :     0.739 
corr_k2l_deltal        :     0.266 
corr_k3l_deltal        :    0.0236 
corr_k4l_deltal        :   -0.0104 
corr_k5l_deltal        :    -0.507 
corr_K_Ml_deltal       :   -0.0697 
corr_alpha_Rl_deltal   :     0.202 
corr_T_Pl_deltal       :    0.0559 
corr_T_El_deltal       :    0.0185 
corr_alpha_El_deltal   :    -0.125 
corr_mu_Cl_deltal      :     0.187 
corr_mu_Ml_deltal      :     0.196 
corr_V_EMl_deltal      :   -0.0224 
corr_V_ECl_deltal      :    -0.474 
corr_k2l_VC0l          :    -0.196 
corr_k3l_VC0l          :    0.0898 
corr_k4l_VC0l          :     0.335 
corr_k5l_VC0l          :    0.0489 
corr_K_Ml_VC0l         :    -0.411 
corr_alpha_Rl_VC0l     :    -0.255 
corr_T_Pl_VC0l         :    0.0753 
corr_T_El_VC0l         :      0.11 
corr_alpha_El_VC0l     :     0.337 
corr_mu_Cl_VC0l        :    -0.561 
corr_mu_Ml_VC0l        :    -0.578 
corr_V_EMl_VC0l        :     0.892 
corr_V_ECl_VC0l        :     0.756 
corr_deltal_VC0l       :    -0.171 
corr_k2l_VM0l          :    -0.178 
corr_k3l_VM0l          :     0.126 
corr_k4l_VM0l          :     0.358 
corr_k5l_VM0l          :    0.0599 
corr_K_Ml_VM0l         :    -0.398 
corr_alpha_Rl_VM0l     :    -0.224 
corr_T_Pl_VM0l         :    0.0715 
corr_T_El_VM0l         :     0.111 
corr_alpha_El_VM0l     :     0.355 
corr_mu_Cl_VM0l        :    -0.596 
corr_mu_Ml_VM0l        :    -0.613 
corr_V_EMl_VM0l        :     0.883 
corr_V_ECl_VM0l        :     0.762 
corr_deltal_VM0l       :    -0.167 
corr_VC0l_VM0l         :     0.998 
corr_k2l_IC0n          :     0.253 
corr_k3l_IC0n          :    -0.111 
corr_k4l_IC0n          :    -0.246 
corr_k5l_IC0n          :     0.194 
corr_K_Ml_IC0n         :  -0.00882 
corr_alpha_Rl_IC0n     :    -0.283 
corr_T_Pl_IC0n         :     -0.21 
corr_T_El_IC0n         :    -0.327 
corr_alpha_El_IC0n     :     0.544 
corr_mu_Cl_IC0n        :    -0.319 
corr_mu_Ml_IC0n        :    -0.305 
corr_V_EMl_IC0n        :     0.211 
corr_V_ECl_IC0n        :     0.286 
corr_deltal_IC0n       :    -0.181 
corr_VC0l_IC0n         :     0.378 
corr_VM0l_IC0n         :     0.376 
corr_k2l_IM0n          :     0.178 
corr_k3l_IM0n          :      0.38 
corr_k4l_IM0n          :     0.698 
corr_k5l_IM0n          :     0.265 
corr_K_Ml_IM0n         :     0.424 
corr_alpha_Rl_IM0n     :     0.184 
corr_T_Pl_IM0n         :    0.0169 
corr_T_El_IM0n         :    0.0682 
corr_alpha_El_IM0n     :     0.378 
corr_mu_Cl_IM0n        :    -0.143 
corr_mu_Ml_IM0n        :    -0.134 
corr_V_EMl_IM0n        :     0.101 
corr_V_ECl_IM0n        :     0.233 
corr_deltal_IM0n       :   -0.0403 
corr_VC0l_IM0n         :    0.0892 
corr_VM0l_IM0n         :     0.115 
corr_IC0n_IM0n         :     0.204 

a1                     :       728 
a2                     :       294 
a3                     :    0.0531 
b3                     :  2.37e-09 
a4                     :    0.0628 
b4                     :  1.08e-09 
a5                     :   0.00409 
b5                     :       0.7 
a6                     :    0.0494 
b6                     :   0.00258 

correlation matrix (IIV)
k2l           1                                                    
k3l        0.38       1                                                 
k4l       -0.08    0.64       1                                              
k5l        0.16    0.19    0.16       1                                           
K_Ml       0.22       0    0.23    0.15       1                                        
alpha_Rl  -0.02    0.02    0.15    0.23     0.1       1                                     
T_Pl      -0.39    0.09     0.2   -0.17   -0.15    0.31       1                                  
T_El      -0.38   -0.02    0.29   -0.13    0.12    0.02    0.72       1                               
alpha_El   0.63    0.47    0.16    0.22   -0.09   -0.06   -0.19   -0.49       1                            
mu_Cl     -0.11   -0.36   -0.29   -0.13    0.08    0.17    0.11    0.06   -0.44       1                         
mu_Ml     -0.09   -0.36    -0.3   -0.13    0.09    0.17    0.08    0.04   -0.43       1       1                      
V_EMl     -0.23    0.23    0.43   -0.06   -0.49   -0.42    0.09     0.2    0.22    -0.4   -0.42       1                   
V_ECl     -0.22    0.26    0.39   -0.06   -0.25   -0.48   -0.04    0.11    0.32   -0.49    -0.5    0.74       1                
deltal     0.27    0.02   -0.01   -0.51   -0.07     0.2    0.06    0.02   -0.12    0.19     0.2   -0.02   -0.47       1             
VC0l       -0.2    0.09    0.33    0.05   -0.41   -0.25    0.08    0.11    0.34   -0.56   -0.58    0.89    0.76   -0.17       1          
VM0l      -0.18    0.13    0.36    0.06    -0.4   -0.22    0.07    0.11    0.35    -0.6   -0.61    0.88    0.76   -0.17       1       1       
IC0n       0.25   -0.11   -0.25    0.19   -0.01   -0.28   -0.21   -0.33    0.54   -0.32   -0.31    0.21    0.29   -0.18    0.38    0.38       1    
IM0n       0.18    0.38     0.7    0.26    0.42    0.18    0.02    0.07    0.38   -0.14   -0.13     0.1    0.23   -0.04    0.09    0.11     0.2       1 


Population parameters estimation...

Elapsed time is 2.11e+05 seconds. 
CPU time is 2.2e+06 seconds. 
