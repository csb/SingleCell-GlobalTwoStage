                      ; parameter
               k2l_pop;  -0.84987
               k3l_pop;  -3.79160
               k4l_pop;  -4.20797
               k5l_pop;  -3.50201
              K_Ml_pop;  -4.33698
          alpha_Rl_pop; -18.68253
              T_Pl_pop;   1.04573
              T_El_pop;   0.98512
          alpha_El_pop;  -5.40066
             mu_Cl_pop;  -6.63993
             mu_Ml_pop;  -6.63325
             V_EMl_pop;   1.54181
             V_ECl_pop;   5.19674
            deltal_pop;   0.54535
              VC0l_pop;   9.07913
              VM0l_pop;   8.34654
              NE0l_pop; -20.00000
              IC0n_pop;  -0.49873
              IM0n_pop;   1.11275
              nE0l_pop; -20.00000
             omega_k2l;   0.33945
             omega_k3l;   0.23153
             omega_k4l;   0.27870
             omega_k5l;   0.28264
            omega_K_Ml;   0.23471
        omega_alpha_Rl;   0.27846
            omega_T_Pl;   0.92989
            omega_T_El;   0.80121
        omega_alpha_El;   1.18078
           omega_mu_Cl;   1.23264
           omega_mu_Ml;   1.08576
           omega_V_EMl;   0.36039
           omega_V_ECl;   0.51576
          omega_deltal;   0.48636
            omega_VC0l;   0.40423
            omega_VM0l;   0.28987
            omega_NE0l;   0.00000
            omega_IC0n;   1.14811
            omega_IM0n;   0.26245
            omega_nE0l;   0.00000
          corr_k2l_k3l;   0.38469
          corr_k2l_k4l;  -0.08364
          corr_k3l_k4l;   0.63742
          corr_k2l_k5l;   0.16471
          corr_k3l_k5l;   0.18566
          corr_k4l_k5l;   0.15837
         corr_k2l_K_Ml;   0.22050
         corr_k3l_K_Ml;   0.00158
         corr_k4l_K_Ml;   0.22830
         corr_k5l_K_Ml;   0.14741
     corr_k2l_alpha_Rl;  -0.01709
     corr_k3l_alpha_Rl;   0.01894
     corr_k4l_alpha_Rl;   0.15309
     corr_k5l_alpha_Rl;   0.23497
    corr_K_Ml_alpha_Rl;   0.10019
         corr_k2l_T_Pl;  -0.38636
         corr_k3l_T_Pl;   0.08885
         corr_k4l_T_Pl;   0.19915
         corr_k5l_T_Pl;  -0.17149
        corr_K_Ml_T_Pl;  -0.15228
    corr_alpha_Rl_T_Pl;   0.31048
         corr_k2l_T_El;  -0.37837
         corr_k3l_T_El;  -0.01635
         corr_k4l_T_El;   0.28518
         corr_k5l_T_El;  -0.12590
        corr_K_Ml_T_El;   0.11541
    corr_alpha_Rl_T_El;   0.02383
        corr_T_Pl_T_El;   0.71809
     corr_k2l_alpha_El;   0.63017
     corr_k3l_alpha_El;   0.46744
     corr_k4l_alpha_El;   0.16264
     corr_k5l_alpha_El;   0.22320
    corr_K_Ml_alpha_El;  -0.09454
corr_alpha_Rl_alpha_El;  -0.06240
    corr_T_Pl_alpha_El;  -0.19373
    corr_T_El_alpha_El;  -0.48872
        corr_k2l_mu_Cl;  -0.11496
        corr_k3l_mu_Cl;  -0.36145
        corr_k4l_mu_Cl;  -0.28866
        corr_k5l_mu_Cl;  -0.12802
       corr_K_Ml_mu_Cl;   0.07591
   corr_alpha_Rl_mu_Cl;   0.16804
       corr_T_Pl_mu_Cl;   0.10676
       corr_T_El_mu_Cl;   0.06359
   corr_alpha_El_mu_Cl;  -0.44245
        corr_k2l_mu_Ml;  -0.09120
        corr_k3l_mu_Ml;  -0.36036
        corr_k4l_mu_Ml;  -0.29590
        corr_k5l_mu_Ml;  -0.12602
       corr_K_Ml_mu_Ml;   0.08957
   corr_alpha_Rl_mu_Ml;   0.16961
       corr_T_Pl_mu_Ml;   0.08493
       corr_T_El_mu_Ml;   0.03762
   corr_alpha_El_mu_Ml;  -0.43260
      corr_mu_Cl_mu_Ml;   0.99862
        corr_k2l_V_EMl;  -0.23407
        corr_k3l_V_EMl;   0.22844
        corr_k4l_V_EMl;   0.42801
        corr_k5l_V_EMl;  -0.06189
       corr_K_Ml_V_EMl;  -0.48815
   corr_alpha_Rl_V_EMl;  -0.42263
       corr_T_Pl_V_EMl;   0.08702
       corr_T_El_V_EMl;   0.19512
   corr_alpha_El_V_EMl;   0.21780
      corr_mu_Cl_V_EMl;  -0.40041
      corr_mu_Ml_V_EMl;  -0.42400
        corr_k2l_V_ECl;  -0.21576
        corr_k3l_V_ECl;   0.25723
        corr_k4l_V_ECl;   0.38954
        corr_k5l_V_ECl;  -0.06322
       corr_K_Ml_V_ECl;  -0.25499
   corr_alpha_Rl_V_ECl;  -0.48458
       corr_T_Pl_V_ECl;  -0.03517
       corr_T_El_V_ECl;   0.11305
   corr_alpha_El_V_ECl;   0.32101
      corr_mu_Cl_V_ECl;  -0.48799
      corr_mu_Ml_V_ECl;  -0.50155
      corr_V_EMl_V_ECl;   0.73923
       corr_k2l_deltal;   0.26598
       corr_k3l_deltal;   0.02359
       corr_k4l_deltal;  -0.01035
       corr_k5l_deltal;  -0.50737
      corr_K_Ml_deltal;  -0.06965
  corr_alpha_Rl_deltal;   0.20165
      corr_T_Pl_deltal;   0.05586
      corr_T_El_deltal;   0.01846
  corr_alpha_El_deltal;  -0.12471
     corr_mu_Cl_deltal;   0.18687
     corr_mu_Ml_deltal;   0.19556
     corr_V_EMl_deltal;  -0.02239
     corr_V_ECl_deltal;  -0.47387
         corr_k2l_VC0l;  -0.19593
         corr_k3l_VC0l;   0.08978
         corr_k4l_VC0l;   0.33487
         corr_k5l_VC0l;   0.04894
        corr_K_Ml_VC0l;  -0.41074
    corr_alpha_Rl_VC0l;  -0.25460
        corr_T_Pl_VC0l;   0.07526
        corr_T_El_VC0l;   0.11045
    corr_alpha_El_VC0l;   0.33667
       corr_mu_Cl_VC0l;  -0.56095
       corr_mu_Ml_VC0l;  -0.57838
       corr_V_EMl_VC0l;   0.89168
       corr_V_ECl_VC0l;   0.75617
      corr_deltal_VC0l;  -0.17086
         corr_k2l_VM0l;  -0.17757
         corr_k3l_VM0l;   0.12590
         corr_k4l_VM0l;   0.35826
         corr_k5l_VM0l;   0.05988
        corr_K_Ml_VM0l;  -0.39801
    corr_alpha_Rl_VM0l;  -0.22415
        corr_T_Pl_VM0l;   0.07148
        corr_T_El_VM0l;   0.11062
    corr_alpha_El_VM0l;   0.35454
       corr_mu_Cl_VM0l;  -0.59576
       corr_mu_Ml_VM0l;  -0.61308
       corr_V_EMl_VM0l;   0.88322
       corr_V_ECl_VM0l;   0.76237
      corr_deltal_VM0l;  -0.16676
        corr_VC0l_VM0l;   0.99756
         corr_k2l_IC0n;   0.25287
         corr_k3l_IC0n;  -0.11081
         corr_k4l_IC0n;  -0.24589
         corr_k5l_IC0n;   0.19417
        corr_K_Ml_IC0n;  -0.00882
    corr_alpha_Rl_IC0n;  -0.28255
        corr_T_Pl_IC0n;  -0.20981
        corr_T_El_IC0n;  -0.32694
    corr_alpha_El_IC0n;   0.54442
       corr_mu_Cl_IC0n;  -0.31867
       corr_mu_Ml_IC0n;  -0.30536
       corr_V_EMl_IC0n;   0.21095
       corr_V_ECl_IC0n;   0.28601
      corr_deltal_IC0n;  -0.18056
        corr_VC0l_IC0n;   0.37834
        corr_VM0l_IC0n;   0.37596
         corr_k2l_IM0n;   0.17750
         corr_k3l_IM0n;   0.37998
         corr_k4l_IM0n;   0.69810
         corr_k5l_IM0n;   0.26494
        corr_K_Ml_IM0n;   0.42410
    corr_alpha_Rl_IM0n;   0.18404
        corr_T_Pl_IM0n;   0.01686
        corr_T_El_IM0n;   0.06821
    corr_alpha_El_IM0n;   0.37818
       corr_mu_Cl_IM0n;  -0.14301
       corr_mu_Ml_IM0n;  -0.13355
       corr_V_EMl_IM0n;   0.10148
       corr_V_ECl_IM0n;   0.23297
      corr_deltal_IM0n;  -0.04031
        corr_VC0l_IM0n;   0.08924
        corr_VM0l_IM0n;   0.11490
        corr_IC0n_IM0n;   0.20394
                    a1; 728.00005
                    a2; 294.02968
                    a3;   0.05314
                    b3;   0.00000
                    a4;   0.06281
                    b4;   0.00000
                    a5;   0.00409
                    b5;   0.69956
                    a6;   0.04940
                    b6;   0.00258
