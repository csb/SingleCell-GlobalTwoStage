******************************************************************
*      Mup1_4.mlxtran
*      January 05, 2018 at 07:10:51
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

                         parameter 
k2l_pop                :     -2.65 
k3l_pop                :     -1.61 
k4l_pop                :      -2.2 
k5l_pop                :      2.73 
K_Ml_pop               :     -6.11 
alpha_Rl_pop           :     -17.1 
T_Pl_pop               :      3.91 
T_El_pop               :      1.04 
alpha_El_pop           :     -3.96 
mu_Cl_pop              :     -6.61 
mu_Ml_pop              :     -6.78 
V_EMl_pop              :      1.51 
V_ECl_pop              :     0.156 
deltal_pop             :      6.57 
VC0l_pop               :      9.08 
VM0l_pop               :      8.35 
NE0l_pop               :       -20 
IC0n_pop               :      4.47 
IM0n_pop               :      1.19 
nE0l_pop               :       -20 

omega_k2l              :       1.9 
omega_k3l              :     0.591 
omega_k4l              :     0.607 
omega_k5l              :     0.464 
omega_K_Ml             :     0.965 
omega_alpha_Rl         :      1.25 
omega_T_Pl             :     0.372 
omega_T_El             :     0.754 
omega_alpha_El         :     0.894 
omega_mu_Cl            :      1.29 
omega_mu_Ml            :      1.21 
omega_V_EMl            :     0.367 
omega_V_ECl            :     0.543 
omega_deltal           :      1.04 
omega_VC0l             :     0.406 
omega_VM0l             :     0.288 
omega_NE0l             :         0 
omega_IC0n             :      4.09 
omega_IM0n             :     0.263 
omega_nE0l             :         0 
corr_k2l_k3l           :    0.0468 
corr_k2l_k4l           :   -0.0954 
corr_k3l_k4l           :     0.912 
corr_k2l_k5l           :    0.0838 
corr_k3l_k5l           :    0.0147 
corr_k4l_k5l           :    0.0403 
corr_k2l_K_Ml          :    0.0315 
corr_k3l_K_Ml          :    -0.138 
corr_k4l_K_Ml          :   -0.0959 
corr_k5l_K_Ml          :     0.197 
corr_k2l_alpha_Rl      :  -0.00516 
corr_k3l_alpha_Rl      :     0.301 
corr_k4l_alpha_Rl      :     0.198 
corr_k5l_alpha_Rl      :     0.251 
corr_K_Ml_alpha_Rl     :     0.219 
corr_k2l_T_Pl          :    -0.225 
corr_k3l_T_Pl          :     0.469 
corr_k4l_T_Pl          :     0.354 
corr_k5l_T_Pl          :    -0.332 
corr_K_Ml_T_Pl         :   -0.0525 
corr_alpha_Rl_T_Pl     :     0.597 
corr_k2l_T_El          :    -0.333 
corr_k3l_T_El          :    -0.362 
corr_k4l_T_El          :    -0.266 
corr_k5l_T_El          :   -0.0688 
corr_K_Ml_T_El         :    -0.379 
corr_alpha_Rl_T_El     :     0.137 
corr_T_Pl_T_El         :      0.02 
corr_k2l_alpha_El      :     0.303 
corr_k3l_alpha_El      :     0.248 
corr_k4l_alpha_El      :     0.192 
corr_k5l_alpha_El      :    0.0596 
corr_K_Ml_alpha_El     :     0.403 
corr_alpha_Rl_alpha_El :     0.127 
corr_T_Pl_alpha_El     :     0.154 
corr_T_El_alpha_El     :    -0.438 
corr_k2l_mu_Cl         :     0.231 
corr_k3l_mu_Cl         :  -0.00299 
corr_k4l_mu_Cl         :    -0.162 
corr_k5l_mu_Cl         :    -0.171 
corr_K_Ml_mu_Cl        :    0.0193 
corr_alpha_Rl_mu_Cl    :     0.192 
corr_T_Pl_mu_Cl        :      0.12 
corr_T_El_mu_Cl        :    0.0389 
corr_alpha_El_mu_Cl    :    -0.219 
corr_k2l_mu_Ml         :     0.256 
corr_k3l_mu_Ml         :   -0.0276 
corr_k4l_mu_Ml         :    -0.185 
corr_k5l_mu_Ml         :    -0.177 
corr_K_Ml_mu_Ml        :    0.0254 
corr_alpha_Rl_mu_Ml    :     0.187 
corr_T_Pl_mu_Ml        :     0.113 
corr_T_El_mu_Ml        :    0.0584 
corr_alpha_El_mu_Ml    :    -0.222 
corr_mu_Cl_mu_Ml       :     0.998 
corr_k2l_V_EMl         :    -0.263 
corr_k3l_V_EMl         :   -0.0849 
corr_k4l_V_EMl         :     0.164 
corr_k5l_V_EMl         :     0.375 
corr_K_Ml_V_EMl        :    -0.247 
corr_alpha_Rl_V_EMl    :     -0.19 
corr_T_Pl_V_EMl        :    -0.349 
corr_T_El_V_EMl        :     0.228 
corr_alpha_El_V_EMl    :    0.0444 
corr_mu_Cl_V_EMl       :    -0.413 
corr_mu_Ml_V_EMl       :    -0.412 
corr_k2l_V_ECl         :    0.0998 
corr_k3l_V_ECl         :     0.232 
corr_k4l_V_ECl         :     0.375 
corr_k5l_V_ECl         :    -0.189 
corr_K_Ml_V_ECl        :    -0.258 
corr_alpha_Rl_V_ECl    :     0.204 
corr_T_Pl_V_ECl        :    0.0517 
corr_T_El_V_ECl        :    0.0627 
corr_alpha_El_V_ECl    :    0.0847 
corr_mu_Cl_V_ECl       :    -0.337 
corr_mu_Ml_V_ECl       :    -0.351 
corr_V_EMl_V_ECl       :     0.248 
corr_k2l_deltal        :   0.00486 
corr_k3l_deltal        :    -0.289 
corr_k4l_deltal        :    -0.341 
corr_k5l_deltal        :    -0.198 
corr_K_Ml_deltal       :     -0.18 
corr_alpha_Rl_deltal   :    -0.354 
corr_T_Pl_deltal       :    -0.284 
corr_T_El_deltal       :    -0.107 
corr_alpha_El_deltal   :    -0.532 
corr_mu_Cl_deltal      :     0.357 
corr_mu_Ml_deltal      :     0.348 
corr_V_EMl_deltal      :    -0.392 
corr_V_ECl_deltal      :    -0.295 
corr_k2l_VC0l          :    -0.258 
corr_k3l_VC0l          :    -0.119 
corr_k4l_VC0l          :     0.153 
corr_k5l_VC0l          :     0.261 
corr_K_Ml_VC0l         :   -0.0505 
corr_alpha_Rl_VC0l     :   -0.0847 
corr_T_Pl_VC0l         :    -0.203 
corr_T_El_VC0l         :     0.128 
corr_alpha_El_VC0l     :     0.189 
corr_mu_Cl_VC0l        :    -0.587 
corr_mu_Ml_VC0l        :    -0.577 
corr_V_EMl_VC0l        :     0.882 
corr_V_ECl_VC0l        :     0.311 
corr_deltal_VC0l       :    -0.409 
corr_k2l_VM0l          :    -0.276 
corr_k3l_VM0l          :    -0.105 
corr_k4l_VM0l          :      0.17 
corr_k5l_VM0l          :     0.269 
corr_K_Ml_VM0l         :   -0.0352 
corr_alpha_Rl_VM0l     :   -0.0804 
corr_T_Pl_VM0l         :    -0.211 
corr_T_El_VM0l         :     0.121 
corr_alpha_El_VM0l     :     0.188 
corr_mu_Cl_VM0l        :    -0.609 
corr_mu_Ml_VM0l        :      -0.6 
corr_V_EMl_VM0l        :     0.878 
corr_V_ECl_VM0l        :     0.331 
corr_deltal_VM0l       :    -0.417 
corr_VC0l_VM0l         :     0.998 
corr_k2l_IC0n          :     0.537 
corr_k3l_IC0n          :     0.193 
corr_k4l_IC0n          :     0.076 
corr_k5l_IC0n          :     0.123 
corr_K_Ml_IC0n         :     0.188 
corr_alpha_Rl_IC0n     :     0.293 
corr_T_Pl_IC0n         :     0.242 
corr_T_El_IC0n         :    -0.504 
corr_alpha_El_IC0n     :     0.517 
corr_mu_Cl_IC0n        :   -0.0619 
corr_mu_Ml_IC0n        :   -0.0528 
corr_V_EMl_IC0n        :   -0.0532 
corr_V_ECl_IC0n        :     0.149 
corr_deltal_IC0n       :     -0.34 
corr_VC0l_IC0n         :   -0.0221 
corr_VM0l_IC0n         :   -0.0402 
corr_k2l_IM0n          :   -0.0707 
corr_k3l_IM0n          :    0.0112 
corr_k4l_IM0n          :       0.3 
corr_k5l_IM0n          :    0.0515 
corr_K_Ml_IM0n         :     0.297 
corr_alpha_Rl_IM0n     :   -0.0972 
corr_T_Pl_IM0n         :    -0.138 
corr_T_El_IM0n         :     0.027 
corr_alpha_El_IM0n     :     0.133 
corr_mu_Cl_IM0n        :    -0.148 
corr_mu_Ml_IM0n        :     -0.16 
corr_V_EMl_IM0n        :     0.151 
corr_V_ECl_IM0n        :     0.292 
corr_deltal_IM0n       :    -0.121 
corr_VC0l_IM0n         :     0.125 
corr_VM0l_IM0n         :     0.153 
corr_IC0n_IM0n         :   -0.0375 

a1                     :       729 
a2                     :       294 
a3                     :     0.051 
b3                     :   0.00279 
a4                     :    0.0641 
b4                     :  8.33e-08 
a5                     :    0.0219 
b5                     :     0.792 
a6                     :    0.0443 
b6                     :    0.0188 

correlation matrix (IIV)
k2l           1                                                    
k3l        0.05       1                                                 
k4l        -0.1    0.91       1                                              
k5l        0.08    0.01    0.04       1                                           
K_Ml       0.03   -0.14    -0.1     0.2       1                                        
alpha_Rl  -0.01     0.3     0.2    0.25    0.22       1                                     
T_Pl      -0.22    0.47    0.35   -0.33   -0.05     0.6       1                                  
T_El      -0.33   -0.36   -0.27   -0.07   -0.38    0.14    0.02       1                               
alpha_El    0.3    0.25    0.19    0.06     0.4    0.13    0.15   -0.44       1                            
mu_Cl      0.23      -0   -0.16   -0.17    0.02    0.19    0.12    0.04   -0.22       1                         
mu_Ml      0.26   -0.03   -0.18   -0.18    0.03    0.19    0.11    0.06   -0.22       1       1                      
V_EMl     -0.26   -0.08    0.16    0.38   -0.25   -0.19   -0.35    0.23    0.04   -0.41   -0.41       1                   
V_ECl       0.1    0.23    0.38   -0.19   -0.26     0.2    0.05    0.06    0.08   -0.34   -0.35    0.25       1                
deltal        0   -0.29   -0.34    -0.2   -0.18   -0.35   -0.28   -0.11   -0.53    0.36    0.35   -0.39   -0.29       1             
VC0l      -0.26   -0.12    0.15    0.26   -0.05   -0.08    -0.2    0.13    0.19   -0.59   -0.58    0.88    0.31   -0.41       1          
VM0l      -0.28    -0.1    0.17    0.27   -0.04   -0.08   -0.21    0.12    0.19   -0.61    -0.6    0.88    0.33   -0.42       1       1       
IC0n       0.54    0.19    0.08    0.12    0.19    0.29    0.24    -0.5    0.52   -0.06   -0.05   -0.05    0.15   -0.34   -0.02   -0.04       1    
IM0n      -0.07    0.01     0.3    0.05     0.3    -0.1   -0.14    0.03    0.13   -0.15   -0.16    0.15    0.29   -0.12    0.12    0.15   -0.04       1 


Population parameters estimation...

Elapsed time is 3.06e+05 seconds. 
CPU time is 2.72e+06 seconds. 
