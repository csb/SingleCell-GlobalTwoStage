% Initial fits based on old starting points..
% My initials I want them to be a bit rough... So 
% I stop the program fast ... 


% DL, June 2017
function [] =MultiStart_WLS100(cell)
%%%------------------------------------------------------------------------
%
%   function MultiStart_WLS.m
%   Implements the multi start for each cell for the analysis of the data from Met addition in yeast cells, and imaging Mup1. Reads in 
%   the data used in Data/ and saves the results in output folder.
%   If using an SGE (Sun Grid Engine) cluster, this can be run using the accompanying .sh script
%   Run this with the command qsub -t 1-1907:5 submit_multistart.sh in SGE 

%   The resulting mat files store a lot of information, the estimates, function values, return code,
%   data and model simulation for each sell in a structure 'MS_params'. Followed by information on the convergence of parameters, CPU time 'CPU_end' and 
%   noise parameters ('theta_hat', 'sigma2') and the individual FIMs 'C_inv'.  Here we donot estimate the noise parameters. We fix the noise parameters and 
% 	estimate the kinetic parameters. 
%   Tools needed: 
        % a) ODE SOLVER
        % b) Optimizer
%   Funtions, objective functions used: 
        % a) WOLS
        % b) Model integration, and evaluating the error model
        % c) Auxilliary functions to calculate the fisher information matrix.      
%   
%   Lines that have to modified:
%       % a) the paths to ODE integrator, and Optimizer. 
        % b) Output directory
        % c) parallellization options

%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------
w = warning ('off','all');
t1=cputime;
%addpath('/usr/local/stelling/grid/matlab')
addpath(genpath('~/TOOLBOXES/nlopt-2.4.2/')) % path to NLOPT optimizer                                               
addpath(genpath('./Model')) 
addpath(genpath('~/Repos/AMICI'))
addpath(genpath('./FirstStage_objs')) 
addpath(genpath('./Auxilliary')) 

% load data
load('./Data/Indcell_data.mat');

%  load initial values for the residuals
load('Spline_fits/noise_bounds.mat')
errs=cat(2,theta0s_bounds(:,2),theta1s_bounds(:,2));

% Storing results in MS_params
MS_params = struct;

% Create problem structure
fprintf('### Doing %d \n',cell)
rng(cell) %set seed generator different for each cell.

problem=struct();
ntrials=100; % number of multistarts to be done. 
problem.x_L=[log([10^-9,10^-9,10^-9,10^-9,10^-9,10^-9,1,1,10^-9,10^-6,10^-6,1,1,1,800,800]),1e-10,10^-10];
problem.x_U=[log([100,100,100,100,10,1,200,200,0.01,10^-1,10^-1,1000,500,1000,1e8,1e8]),3,3];
xn = lhsdesign(ntrials,length(problem.x_L)); % generate normalized design
xn_scaled = bsxfun(@plus,problem.x_L,bsxfun(@times,xn,(problem.x_U-problem.x_L)));

% Create  optimisation structure
optim=struct();
max_tol=1e-5;
%optim.algorithm = NLOPT_LN_SBPLX; % Using non gradient based. 
optim.algorithm = NLOPT_LN_NELDERMEAD; % Using non gradient based. 

MULTI=[];                         % Storing results of the multi-start
MULTI_x=struct();                 % Storing parameters of the multi-start
optim.ftol_rel=max_tol;
%optim.maxtime=60*5;              % Just to avoid being stuck in an optima for a LONG time. 
optim.maxeval=1e5;                                 

% integration options
int_opts=struct;
int_opts.atol=1e-6;
int_opts.rtol=1e-3;

% Get data
ydata_tot = data(:,[1,2,7:8,10,13:15,16]);      % Excluded SPOTS VOLUME/ number SO FAR
index = ydata_tot.Track_index==cell;
ydata = ydata_tot(index,:);                     
ydata_response = table2array(ydata);
time = ydata_response(:,1);
ydata_response = ydata_response(:,[3:4,6:9]);   % Response data only (no volume cytoplasm included)


% Objective function. 
fmin_obj = @(pp) WOLS(pp',time,errs,ydata_response,int_opts);
optim.min_objective=fmin_obj;



trial=1;                		  % trial (# of multi-start)
while trial<=ntrials
problem.x_0=xn_scaled(trial,:);   % change the initial starting point of estimation
optim.lower_bounds=problem.x_L;	  % update the bounds (if the bounds are different in each run)	
optim.upper_bounds=problem.x_U;	
% optim.verbose = 1;
[x, fval, retcode] = nlopt_optimize(optim, problem.x_0); % calling optimizer. If a gradient based optimizer is used, return the gradient also.  

% Store results
MULTI_x(trial).x=x;
MULTI_x(trial).problem=problem;
MULTI(trial,:)=[fval,retcode];
trial=trial+1;
end

% Choosing the parameters that lead to the minimum
[~,ind]=min(MULTI(:,1));
params=MULTI_x(ind).x;
fval=MULTI(ind,1);


% Simulate trajectory and same
fprintf('### Simulate at optima for %i \n',cell);
[f,~,simulated,sens_observations,FIM]=Endo_eq(params',time,ydata_response,errs,int_opts);

%Save EVRYTHING 
MS_params.params = params; 		  % parameter
MS_params.fval = fval;	 		  % function value
MS_params.Dsim =simulated;  	  % All simulations (if you want to save the state trajectories svae it, here)
MS_params.observables =simulated; % All simulations
MS_params.sens_observations =sens_observations; % Sensitivities.
MS_params.FIM =FIM;				  % FIM	
MS_params.ydata = ydata_response; % data	
MS_params.time=time;		      % time (data)
MS_params.CVODE_flag=f;			  % integration failed?
MS_params.err = errs;		      % noise parameters
MS_params.retcode=MULTI(ind,2);	  % return code of the optimization
MS_params.opt_settings = MULTI_x(ind).problem;	% settings
MS_params.cellID = cell;		  % cell ID
MS_params.dof=(sum(~isnan(ydata_response)));	% Number of data points

% WOLS of each observation.
MS_params.actfval=zeros(1,6);
if(f~=1)
    for kk=1:6
g_model=errs(kk,1)+MS_params.observables(:,kk).*errs(kk,2);
MS_params.actfval(kk)=nansum(((MS_params.ydata(:,kk)-MS_params.observables(:,kk))./g_model).^2);
    end
else
 MS_params.actfval=NaN;
end
MS_params.sigmasqcell= MS_params.actfval;% residuals per cell. (not corrected for degrees of freedom)
                                                                            
comp_time=cputime-t1;

fprintf('### Done  %i \n',cell);
mkdir('MultiStart_Results/Multistart_ResultsNLMD_100')
save(sprintf('MultiStart_Results/Multistart_ResultsNLMD_100/Estimates_%d.mat',cell), 'MS_params','comp_time','trial','MULTI','MULTI_x','ntrials')
