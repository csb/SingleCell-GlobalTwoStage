clear all
%%
% Add paths
addpath(genpath('/usr/local/stelling/grid/matlab'))
addpath(genpath('../Model'))
addpath(genpath('~/Repos/AMICI'))
addpath(genpath('../Auxilliary'))

% For the ith cell
%load('../Data/Indcell_data.mat');
load('../SAEM_FILES/Mup1_3/results.mat')
SAEM_IDS=IDs;
load('../Pub_Results_new/FirstStageNLMD50_iter5.mat')

% Add a check on the IDS from SAEM 
ids=1:1907;
cell_ids=zeros(1,1907);

NGLS=struct()
load('../SAEM_FILES/Mup1_3/Mup1_3.mat')
betahat=struct2array(betahat);
all_theta0s=err(:,1);
all_theta1s=err(:,2);

int_opts.atol=1e-6;
int_opts.rtol=1e-4;
Res=zeros(1907,6);
% 
for id=1:1907
    
num_samples=1;


% Then calulate the likelihood for the cell 
time=GLS_params(id).time;
ydata_response=GLS_params(id).ydata;

cell_id=GLS_params(id).cellID;
if cell_id==str2num(cell2mat(IDs(cell_id)))
x=betahati(cell_id,:);
NGLS(id).params=x;
NGLS(id).time=time;
NGLS(id).ydata=ydata_response;

[f,~,simulated,sens_observations,FIM]=Endo_eq(x',NGLS(id).time,NGLS(id).ydata,[all_theta0s,all_theta1s],int_opts);
NGLS(id).cellID=cell_id;
NGLS(id).FIM=FIM;
NGLS(id).err = [all_theta0s,all_theta1s];
end
end

GLS_params=NGLS;
save('data_SAEM_3.mat','GLS_params','all_theta0s','all_theta1s')
