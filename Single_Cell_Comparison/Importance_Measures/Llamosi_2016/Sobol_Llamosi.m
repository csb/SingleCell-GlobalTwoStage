function [] =Sobol_Llamosi(readout_id,time_id,run_num)
%%%------------------------------------------------------------------------
%
%   function Sobol_Llamosi.m
%   Sobol first order indices calculation for osmo-schok model using the GTS results.
%   Tools needed:
%         a) ODE SOLVER: odeSD

%   Funtions, objective functions used:
%         a) condMVN: Get the condition multivariate normal
%         b) get_outputs: simulates model
%         c) get_var_condExp: calculate conditional expectations of the model
%
%
%         d) Model integration

%        INPUT: readout_id: id of the readout whose variance we are interested in
%				time_id: experimental time, we are interested in 
%				run_num: trial. 
%		 OUTPUT: Output is svaed in folder, and contains the first order indices, variances
%				model options, number of samples used and any error flags.
%
%   Lines that have to modified:
%       % a) the paths to ODE integrator
%         b) Output directory
%         c) parallellization options

%   Lekshmi Dharmarajan, July 2017
%   Checked, January 2018
%   CSB, ETH Zurich.
%
%%%-----------------------------------------------------------------------

clc
% (USER MUST MODIFY )
addpath(genpath('/usr/local/stelling/grid/matlab'))
addpath(genpath('~/Repos/odeSD'))
myCluster = parcluster('local');
myCluster.NumWorkers = 50;
parpool(myCluster,myCluster.NumWorkers);



%% Path to the model files		% (USER MUST MODIFY )
addpath(genpath('../../Llamosi_2016/Model/'))

disp(readout_id)
disp(time_id)
disp(run_num)


% Load the result		% (USER MUST MODIFY )
load('../../Llamosi_2016/Pubs_Results/GTS-Result.mat')
%betahat=struct2array(betahat);

% Output characters: t=20, Intensity of Spots.
observation_id=readout_id;
interesting_times=[0.1 0.6 3.5 5.5 8.75 9.5]*60; %in minutes
time=interesting_times(time_id); % in minutes

% Get the parameter distribution
betahat=betahat;
Dhat=Dhat;
[~,Correl]=cov2corr(Dhat);

% Samples from joint distribution
num_samples=100;

% the seed number I use.
myseeds=combvec(1:length(interesting_times),1:5)'; % GENERATING COMBINATION OF THE TIME IDS AND THE NUMBER OF TRIALS
[~,myseed]=intersect(myseeds, [time_id run_num],'rows');

rng(myseed)
p_joint=mvnrnd(betahat,Dhat,num_samples);
int_tol.abstol=1e-6;
int_tol.rtol=1e-4;

%% Calculate the unconditional variance of the output.
uncon_Y=get_outputs(p_joint,time,int_tol,observation_id);
V_Y=var(uncon_Y);

%% Calculate the variation of the conditional expectation E_Xi'(Y|Xi=xi)
model_opt.time=time;
model_opt.int_tol=int_tol;
model_opt.observation_id=observation_id;
V_Xis=zeros(1,length(betahat));

for given_id=1:length(betahat)
    V_Xis(given_id)=get_var_condExp(given_id,num_samples,p_joint,betahat',Dhat,model_opt,myseed);
    
end
S_i=V_Xis./V_Y;


%% Calculte the expectation of the conditional variation
% Not computed. Only the intial values are returned. 

%model_opt.time=time;
%model_opt.int_tol=int_tol;
%model_opt.observation_id=observation_id;
V_X_is=zeros(1,length(betahat));
flags=V_X_is;

%for dep_id=1:length(betahat)
%given_ids=setdiff(1:18,dep_id);
%[V_X_is(dep_id),flags(dep_id)]=get_var_condExp(given_ids,num_samples,p_joint,betahat',Dhat,model_opt,myseed);

%end

S_Ti=[]; %not computed. 

%S_Ti=(V_Y-V_X_is)./V_Y;

%
delete(gcp('nocreate'))


mkdir(sprintf('Results_run%d',run_num))
save(sprintf('Results_run%d/SInd_%d_%d.mat',run_num,readout_id,time_id),'flags','time','model_opt','num_samples','S_i','S_Ti','V_Y','V_Xis','V_X_is')
