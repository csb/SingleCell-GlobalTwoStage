% Sobol indices calculation 
% for Box1 example b
function [] =Sobol_box1b(readout_id,run_num)

addpath(genpath('/usr/local/stelling/grid/matlab'))

myCluster = parcluster('local');
myCluster.NumWorkers = 50;
parpool(myCluster,myCluster.NumWorkers);

%% addpath
addpath(genpath('Model'))
addpath(genpath('./source_functions/'))

disp(readout_id)
disp(run_num)

% Load the result
res_dir='./Results/';
% data characteristics
observation_id=readout_id;
interesting_times=[0:20:100].*60; %in unit
time=interesting_times; % in unit




% Samples from joint distribution
num_samples=100;
rng('shuffle')

% parameters
g_y=0.003; k_x=0.05;
betahat=log([g_y, k_x]);
Dhat=corr2cov(sqrt([0.1,3]),[1,0;0,1]); 

% Change this according to different scenarios
% We changed only correlations/ variance of one of the parameter g_y 

p_joint=mvnrnd(betahat,Dhat,num_samples);


 
model_opt.time=time;
model_opt.observation_id=observation_id;
model_opt.model=@ToyModel;
model_opt.rtol=1e-6;
model_opt.x0=[0,0]';
model_opt.abstol=1e-6;
model_opt.k=[];

%% Calculate the unconditional variance of the output.


uncon_Y=get_outputs(p_joint,time,observation_id,model_opt);
V_Y=var(uncon_Y);



%% Calculate the variation of the conditional expectation E_Xi'(Y|Xi=xi)
V_Xis=zeros(length(betahat),length(time));
to_integrate=0;

for given_id=1:length(betahat)
V_Xis(given_id,:)=get_var_condExp(given_id,num_samples,p_joint,betahat',Dhat,model_opt,to_integrate);

end

S_i=V_Xis'./V_Y'; 



mkdir(sprintf('%s/Run%d',res_dir,run_num))
save(sprintf('%s/Run%d/SInd_%d_int.mat',res_dir,run_num,readout_id),'uncon_Y','S_i','model_opt')
