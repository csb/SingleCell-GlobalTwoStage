function outputs = get_outputs(params,time,observation_id,int_opts)
outputs=zeros(size(params,1),length(time));
for i=1:size(params,1) % parallelize this loop... The method depends on how well we accurately had
                       % sampled. 

opt=odeset('NonNegative',1:2);
[t, obs_y]=ode15s(int_opts.model,time,int_opts.x0,opt,exp(params(i,:)));
outputs(i,:)=obs_y(:,observation_id)';

end