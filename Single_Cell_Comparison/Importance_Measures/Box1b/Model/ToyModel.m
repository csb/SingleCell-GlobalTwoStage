function yp =ToyModel(t,x,px)
%Toy model:
p.gP = 0.002;
p.kR=0.2;
basal_rate=2e-3;

Y    = x(1);
Xp = x(2);
p.gR=px(1);
p.kP=px(2);

a = [p.kR*(Xp)/100+basal_rate;               
    p.kP*Y*(25-Xp)/(50+25-Xp);     
    p.gR*Y                                   
    p.gP*Xp/(10+Xp)];                 

stoich_matrix = [ 1  0    %transcription
                  0  1    %translation
                 -1  0    %mRNA decay
                  0 -1 ]; %protein decay
         
yp= (a'*stoich_matrix)';

end
