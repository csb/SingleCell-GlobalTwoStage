% Calculating Sobol indices for a toy example. 
% Toy example: 
%       y(x1,x2)=x1^2
% Domain: multivariate standard normal with NO correlations
% Lekshmi D
% Checked in March 2018, Sept 2018 for Alina

%%
clear all
close all
clc

% Model
fun_y=@(x1,x2) x1.*2;

% 
pidx=1:2; % parameter index

% Sampling regimes
rng('shuffle')
mu=[0,0];
sigma=[1 0;0 1]; % No correlations, std. gaussian
n_samples=1000; % number of samples, varying this will vary the approximation quality
x_samples=mvnrnd(mu,sigma,n_samples);


%% Get the unconditional variance of Y 
y_samples=fun_y(x_samples(:,1),x_samples(:,2));
V_y=var(y_samples);

%% Calculating S_p for the xi' th parameter
% calculate V_j'E_~j(Y|X_j') % Variance over j
% calculte E_~j(Y|X_j')       % Expectation over ~j


% Variance obtained by fixing x1 to different values
% treat the jth sample fixed
V_p=zeros(1,2); 

for j=1:2
 all_exps=[];
 
for i=1:n_samples
x_nsample=x_samples(i,j);   

% GIVEN X1 sample, sample the other parameters
% Done here like this, because the two parameters are
% independent. Should only use conditional distribution 
% otherwise. 

mu_samples=mu;
sigma_samples=sigma;
sigma_samples(j)=0;
mu_samples(j)=x_nsample;

% Sample the other parameter
x_samples1=mvnrnd(mu_samples,sigma_samples,n_samples);

% Evaluate the model
y_samples1=fun_y(x_samples1(:,1),x_samples1(:,2));

% Collect the expectation term into a vector
all_exps=cat(1,all_exps,mean(y_samples1));

end

% Take the variance of expectations
V_p(j)=var(all_exps);
end

% First Order
S_p=V_p./V_y;



% 
disp('############################################################################')
fprintf('First Order Sobol index for x1: %d and x2: %d \n', S_p(1),S_p(2))

disp('############################################################################')
disp('For this boring example, only x1 should matter and that is what we see.') 

