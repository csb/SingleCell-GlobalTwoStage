%% MY ATTEMPT AT CALCULATING THE SOBOL INDICES. 

function [] =Sobol_endo(readout_id,time_id,run_num)
clc
%clear all
addpath(genpath('/usr/local/stelling/grid/matlab'))

myCluster = parcluster('local');
myCluster.NumWorkers = 10;
parpool(myCluster,10);

%% addpath
addpath(genpath('../../Mup1_Endocytosis/Model/'))
addpath(genpath('~/Repos/AMICI'))

disp(readout_id)
disp(time_id)
disp(run_num)

% Load the result
load('../../Mup1_Endocytosis/Pub_Results/NLMD_505.mat')
%load('../../Mup1_Endocytosis/SAEM_FILES/Mup1_GTS/Mup1_GTS.mat')   
%betahat=struct2array(betahat);


% Output characters: t=20, Intensity of Spots.
observation_id=readout_id;
interesting_times=[-4,4,14,24,44];
time=interesting_times(time_id); % in minutes

% Get the parameter distribution
betahat=betahat;
Dhat=Dhat;
[~,Correl]=cov2corr(Dhat);

% Samples from joint distribution
num_samples=1000;


% the seed number I use.
myseeds=combvec(1:length(interesting_times),1:5,1:6)'; % GENERATING COMBINATION OF THE TIME IDS AND THE NUMBER OF TRIALS
[~,myseed]=intersect(myseeds, [time_id run_num readout_id],'rows');
rng(myseed)

p_joint=mvnrnd(betahat,Dhat,num_samples);
int_tol.atol=1e-4;
int_tol.rtol=1e-4;

%% Calculate the unconditional variance of the output.
uncon_Y=get_outputs(p_joint,time,int_tol,observation_id);
V_Y=var(uncon_Y);

%% Calculate the variation of the conditional expectation E_Xi'(Y|Xi=xi)
model_opt.time=time;
model_opt.int_tol=int_tol;
model_opt.observation_id=observation_id;
V_Xis=zeros(1,length(betahat));

for given_id=1:length(betahat)
V_Xis(given_id)=get_var_condExp(given_id,num_samples,p_joint,betahat',Dhat,model_opt,myseed);
end
S_i=V_Xis./V_Y; 


%% Calculte the expectation of the conditional variation
%model_opt.time=time;
%model_opt.int_tol=int_tol;
%model_opt.observation_id=observation_id;
V_X_is=zeros(1,length(betahat));
flags=V_X_is;

%for dep_id=1:length(betahat)
%given_ids=setdiff(1:18,dep_id); 

%[V_X_is(dep_id),flags(dep_id)]=get_var_condExp(given_ids,num_samples,p_joint,betahat',Dhat,model_opt);

%end

S_Ti=[];

%S_Ti=(V_Y-V_X_is)./V_Y;

%
delete(gcp('nocreate'))


mkdir(sprintf('New_run%d',run_num))
save(sprintf('New_run%d/SInd_%d_%d.mat',run_num,readout_id,time_id),'flags','time','model_opt','num_samples','S_i','S_Ti','V_Y','V_Xis','V_X_is')
