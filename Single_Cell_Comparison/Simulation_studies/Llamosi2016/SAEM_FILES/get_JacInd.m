% Calculate the Jaccard index 
% from all the figures. 

index=4;

if index==1

% data
data_folder='../Simulations/Simulated_Data/Data_Fig3a/';
folder_names={'500','350','250','150','50'};
% Results to GTS
dirname_AR='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3a/';
dirname_PL='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison//Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3a/';
% SAEM results
dirname_SAEM='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/SAEM_FILES/Data_Fig3a';
label_names=[500,350,250,150,50];
elseif index==2
data_folder='../Simulations/Simulated_Data/Data_Fig3b/'; 
folder_names={'2','4','8','16','32'};  
dirname_AR='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3b/';
dirname_PL='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3b/';
dirname_SAEM='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/SAEM_FILES/Data_Fig3b/'; 
label_names=[2,4,8,16,32];
elseif index==3
 data_folder='../Simulations/Simulated_Data/Data_Fig3c/';   
dirname_AR='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3c/';
dirname_PL='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3c/';
dirname_SAEM='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/SAEM_FILES/Data_Fig3c/';
folder_names={'20','40','60','80','100'};
label_names=[20,40,60,80,100];
else
data_folder='../Simulations/Simulated_Data/Data_Fig3d/';   
dirname_AR='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison//Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3d/';
dirname_PL='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/Simulations/SecondStage_Results/Data_Fig3d/';
dirname_SAEM='/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Simulation_studies/Llamosi2016/SAEM_FILES/Data_Fig3d/'; 
folder_names={'25','50','75','100'};%
label_names=[25,50,75,100];
end



tosave=[];
for j=1:length(folder_names)
    folder_name=folder_names{j};
for i=1:5
dat=imread(sprintf('%s/%s/images_scaled/data_%d.png',dirname_SAEM,folder_name,i));
pred=imread(sprintf('%s/%s/images_scaled/SAEM_Predictions_%d.png',dirname_SAEM,folder_name,i));
dat=im2bw(imresize(dat,[400,400]));
pred=im2bw(imresize(pred,[400,400]));

intersectImg = dat & pred;
unionImg = pred | dat;
Jac_index=sum(intersectImg(:))/sum(unionImg(:));
tosave=cat(1,tosave,[label_names(j), i, Jac_index]);
end
end
tosave_SAEM=tosave;

tosave=[];
for j=1:length(folder_names)
folder_name=folder_names{j};
for i=1:5
dat=imread(sprintf('%s/%s/images_scaled/data_%d.png',dirname_SAEM,folder_name,i));
pred=imread(sprintf('%s/%s/images_scaled/predictions_%d.png',dirname_AR,folder_name,i));
dat=im2bw(imresize(dat,[400,400]));
pred=im2bw(imresize(pred,[400,400]));
intersectImg = pred & dat;
unionImg = pred | dat;
Jac_index=sum(intersectImg(:))/sum(unionImg(:));
tosave=cat(1,tosave,[label_names(j), i, Jac_index]);
end
end
tosave_GTS=tosave;


tosave=[];
for j=1:length(folder_names)
folder_name=folder_names{j};
for i=1:5
dat=imread(sprintf('%s/%s/images_scaled/data_%d.png',dirname_SAEM,folder_name,i));
pred=imread(sprintf('%s/%s/images_scaled/PLpredictions_%d.png',dirname_PL,folder_name,i));
dat=im2bw(imresize(dat,[400,400]));
pred=im2bw(imresize(pred,[400,400]));
intersectImg = pred & dat;
unionImg = pred | dat;
Jac_index=sum(intersectImg(:))/sum(unionImg(:));
tosave=cat(1,tosave,[label_names(j), i, Jac_index]);
end
end
tosave_GTS_PL=tosave;

save('./Data_Fig3d/comp_pred.mat','tosave_GTS','tosave_SAEM','tosave_GTS_PL');
