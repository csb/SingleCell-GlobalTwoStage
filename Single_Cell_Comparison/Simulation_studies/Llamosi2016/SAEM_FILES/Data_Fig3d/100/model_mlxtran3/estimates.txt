          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.33895;   0.03700;      1.58;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.30021;   0.03278;      0.62;        NaN
    gm_pop;  -3.58445;   0.04809;      1.34;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.66955;   0.02693;      4.02;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.48111;   0.02599;      5.40;        NaN
  omega_gm;   0.80649;   0.03748;      4.65;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.02758;   0.07080;    256.74;        NaN
corr_km_gm;   0.78686;   0.02455;      3.12;        NaN
corr_gp_gm;  -0.59801;   0.04639;      7.76;        NaN
         a;  60.70396;   0.86587;      1.43;        NaN
         b;   0.08656;   0.00066;      0.76;        NaN
