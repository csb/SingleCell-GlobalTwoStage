          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.31277;   0.03563;      1.54;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.29518;   0.03241;      0.61;        NaN
    gm_pop;  -3.63926;   0.04771;      1.31;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.62408;   0.02656;      4.26;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.49183;   0.02598;      5.28;        NaN
  omega_gm;   0.79587;   0.03768;      4.73;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.01577;   0.07180;    455.25;        NaN
corr_km_gm;   0.74524;   0.02883;      3.87;        NaN
corr_gp_gm;  -0.63468;   0.04296;      6.77;        NaN
         a;  62.35335;   0.88158;      1.41;        NaN
         b;   0.08721;   0.00067;      0.77;        NaN
