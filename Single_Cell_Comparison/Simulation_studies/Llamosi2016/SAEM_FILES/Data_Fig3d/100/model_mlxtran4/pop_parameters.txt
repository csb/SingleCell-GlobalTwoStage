******************************************************************
*      model_mlxtran4.mlxtran
*      March 03, 2018 at 23:20:01
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.41         0.039          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.23         0.033          1    
gm_pop     :    -3.58         0.051          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.699         0.028          4    
omega_kp   :        0           -          -      
omega_gp   :    0.504         0.026          5    
omega_gm   :    0.861         0.039          5    
omega_ltau :        0           -          -      
corr_km_gp :  -0.0898         0.068         76    
corr_km_gm :    0.795         0.023          3    
corr_gp_gm :   -0.647         0.041          6    

a          :     62.4           0.9          1    
b          :   0.0859       0.00065          1    

correlation matrix (IIV)
km      1       
gp  -0.09       1    
gm   0.79   -0.65       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.07       1    
gm_pop   0.75   -0.67       1 

Eigenvalues (min, max, max/min): 0.026  2  79

omega_km      1             
omega_gp      0       1          
omega_gm   0.57    0.45       1       
a            -0   -0.01   -0.01       1    
b            -0   -0.01   -0.01    -0.4       1 

Eigenvalues (min, max, max/min): 0.28  1.7  6.3

corr_km_gp      1       
corr_km_gm  -0.63       1    
corr_gp_gm   0.82   -0.08       1 

Eigenvalues (min, max, max/min): 0.0027  2.1  7.7e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 2.62e+03 seconds. 
CPU time is 1.27e+04 seconds. 
