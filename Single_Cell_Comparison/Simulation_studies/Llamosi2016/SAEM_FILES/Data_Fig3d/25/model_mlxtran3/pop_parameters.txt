******************************************************************
*      model_mlxtran3.mlxtran
*      March 04, 2018 at 03:44:47
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.33         0.035          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.31         0.029          1    
gm_pop     :    -3.57         0.043          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.644         0.026          4    
omega_kp   :        0           -          -      
omega_gp   :    0.493         0.021          4    
omega_gm   :    0.754         0.032          4    
omega_ltau :        0           -          -      
corr_km_gp :   0.0872         0.059         68    
corr_km_gm :    0.751         0.026          3    
corr_gp_gm :   -0.551         0.042          8    

a          :     61.3          0.65          1    
b          :   0.0865       0.00051          1    

correlation matrix (IIV)
km      1       
gp   0.09       1    
gm   0.75   -0.55       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.07       1    
gm_pop   0.74   -0.58       1 

Eigenvalues (min, max, max/min): 0.025  1.9  76

omega_km      1             
omega_gp      0       1          
omega_gm   0.54    0.33       1       
a            -0      -0      -0       1    
b            -0   -0.01   -0.01   -0.39       1 

Eigenvalues (min, max, max/min): 0.37  1.6  4.4

corr_km_gp      1       
corr_km_gm  -0.56       1    
corr_gp_gm   0.76    0.11       1 

Eigenvalues (min, max, max/min): 0.0033  1.9  5.7e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 4.26e+03 seconds. 
CPU time is 1.82e+04 seconds. 
