******************************************************************
*      model_mlxtran1.mlxtran
*      March 04, 2018 at 01:22:54
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.34         0.037          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.29          0.03          1    
gm_pop     :    -3.61         0.048          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.658         0.027          4    
omega_kp   :        0           -          -      
omega_gp   :     0.51         0.023          4    
omega_gm   :    0.854         0.036          4    
omega_ltau :        0           -          -      
corr_km_gp :   -0.133          0.06         45    
corr_km_gm :    0.784         0.023          3    
corr_gp_gm :   -0.682         0.033          5    

a          :     62.8          0.67          1    
b          :    0.087       0.00051          1    

correlation matrix (IIV)
km      1       
gp  -0.13       1    
gm   0.78   -0.68       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   -0.1       1    
gm_pop   0.76   -0.69       1 

Eigenvalues (min, max, max/min): 0.023  2.1  90

omega_km      1             
omega_gp      0       1          
omega_gm   0.58    0.47       1       
a            -0      -0      -0       1    
b            -0   -0.01   -0.01   -0.39       1 

Eigenvalues (min, max, max/min): 0.26  1.7  6.8

corr_km_gp      1       
corr_km_gm  -0.68       1    
corr_gp_gm   0.78   -0.08       1 

Eigenvalues (min, max, max/min): 0.0038  2.1  5.5e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 4.28e+03 seconds. 
CPU time is 1.83e+04 seconds. 
