******************************************************************
*      model_mlxtran5.mlxtran
*      March 04, 2018 at 07:22:54
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.33         0.038          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.26          0.03          1    
gm_pop     :    -3.66         0.048          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.689         0.028          4    
omega_kp   :        0           -          -      
omega_gp   :    0.513         0.023          4    
omega_gm   :    0.839         0.035          4    
omega_ltau :        0           -          -      
corr_km_gp :  -0.0497         0.061        122    
corr_km_gm :    0.793         0.023          3    
corr_gp_gm :   -0.618         0.037          6    

a          :     61.8          0.65          1    
b          :   0.0874       0.00051          1    

correlation matrix (IIV)
km      1       
gp  -0.05       1    
gm   0.79   -0.62       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.03       1    
gm_pop   0.77   -0.63       1 

Eigenvalues (min, max, max/min): 0.018  2  1.1e+02

omega_km      1             
omega_gp     -0       1          
omega_gm   0.59     0.4       1       
a            -0      -0      -0       1    
b            -0   -0.01   -0.01   -0.38       1 

Eigenvalues (min, max, max/min): 0.29  1.7  6

corr_km_gp      1       
corr_km_gm  -0.62       1    
corr_gp_gm   0.79   -0.01       1 

Eigenvalues (min, max, max/min): 0.0022  2  9e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 6.92e+03 seconds. 
CPU time is 2.1e+04 seconds. 
