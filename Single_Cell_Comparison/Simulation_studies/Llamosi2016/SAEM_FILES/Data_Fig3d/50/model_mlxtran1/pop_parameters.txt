******************************************************************
*      model_mlxtran1.mlxtran
*      March 04, 2018 at 16:23:29
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.33         0.039          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.32          0.03          1    
gm_pop     :     -3.6         0.047          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.665         0.029          4    
omega_kp   :        0           -          -      
omega_gp   :    0.499         0.023          5    
omega_gm   :    0.811         0.036          4    
omega_ltau :        0           -          -      
corr_km_gp :  -0.0257         0.064        250    
corr_km_gm :    0.769         0.026          3    
corr_gp_gm :   -0.626         0.039          6    

a          :     62.7          0.72          1    
b          :   0.0868       0.00055          1    

correlation matrix (IIV)
km      1       
gp  -0.03       1    
gm   0.77   -0.63       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop      0       1    
gm_pop   0.75   -0.63       1 

Eigenvalues (min, max, max/min): 0.02  2  1e+02

omega_km      1             
omega_gp  -0.01       1          
omega_gm   0.56    0.39       1       
a            -0      -0      -0       1    
b         -0.01   -0.01   -0.01   -0.39       1 

Eigenvalues (min, max, max/min): 0.31  1.7  5.3

corr_km_gp      1       
corr_km_gm  -0.63       1    
corr_gp_gm   0.77       0       1 

Eigenvalues (min, max, max/min): 0.0025  2  8.1e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 6.06e+03 seconds. 
CPU time is 1.62e+04 seconds. 
