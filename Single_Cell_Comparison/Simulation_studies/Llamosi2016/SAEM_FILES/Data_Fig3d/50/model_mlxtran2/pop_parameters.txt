******************************************************************
*      model_mlxtran2.mlxtran
*      March 04, 2018 at 18:06:32
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.38         0.035          1    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.31         0.033          1    
gm_pop     :    -3.54         0.048          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.636         0.025          4    
omega_kp   :        0           -          -      
omega_gp   :    0.556         0.025          5    
omega_gm   :    0.841         0.036          4    
omega_ltau :        0           -          -      
corr_km_gp :   -0.107         0.061         57    
corr_km_gm :    0.742         0.027          4    
corr_gp_gm :   -0.714          0.03          4    

a          :     61.7          0.71          1    
b          :   0.0868       0.00054          1    

correlation matrix (IIV)
km      1       
gp  -0.11       1    
gm   0.74   -0.71       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   -0.1       1    
gm_pop   0.72   -0.73       1 

Eigenvalues (min, max, max/min): 0.026  2.1  79

omega_km      1             
omega_gp   0.01       1          
omega_gm   0.51    0.53       1       
a            -0      -0   -0.01       1    
b            -0   -0.01      -0   -0.39       1 

Eigenvalues (min, max, max/min): 0.27  1.7  6.5

corr_km_gp      1       
corr_km_gm  -0.71       1    
corr_gp_gm   0.75   -0.07       1 

Eigenvalues (min, max, max/min): 0.0036  2.1  5.7e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 6.02e+03 seconds. 
CPU time is 1.71e+04 seconds. 
