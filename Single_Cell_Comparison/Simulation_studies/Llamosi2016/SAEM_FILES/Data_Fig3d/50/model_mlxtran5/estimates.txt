          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.32498;   0.03708;      1.59;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.28191;   0.03132;      0.59;        NaN
    gm_pop;  -3.65886;   0.04858;      1.33;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.67883;   0.02671;      3.93;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.50937;   0.02374;      4.66;        NaN
  omega_gm;   0.85027;   0.03642;      4.28;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.10571;   0.06219;     58.83;        NaN
corr_km_gm;   0.80538;   0.02144;      2.66;        NaN
corr_gp_gm;  -0.64578;   0.03691;      5.72;        NaN
         a;  61.76536;   0.70523;      1.14;        NaN
         b;   0.08731;   0.00055;      0.63;        NaN
