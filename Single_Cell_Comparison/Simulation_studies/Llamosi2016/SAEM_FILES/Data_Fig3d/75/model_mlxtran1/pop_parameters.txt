******************************************************************
*      model_mlxtran1.mlxtran
*      March 04, 2018 at 08:56:02
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.33         0.039          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.28         0.031          1    
gm_pop     :    -3.63          0.05          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.651          0.03          5    
omega_kp   :        0           -          -      
omega_gp   :    0.491         0.025          5    
omega_gm   :    0.841         0.039          5    
omega_ltau :        0           -          -      
corr_km_gp :   -0.103         0.069         66    
corr_km_gm :    0.767         0.028          4    
corr_gp_gm :   -0.686         0.037          5    

a          :     63.2          0.79          1    
b          :   0.0869       0.00061          1    

correlation matrix (IIV)
km      1       
gp   -0.1       1    
gm   0.77   -0.69       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.03       1    
gm_pop   0.74   -0.67       1 

Eigenvalues (min, max, max/min): 0.019  2  1.1e+02

omega_km      1             
omega_gp  -0.01       1          
omega_gm   0.54    0.45       1       
a            -0      -0      -0       1    
b         -0.01   -0.02   -0.01   -0.39       1 

Eigenvalues (min, max, max/min): 0.29  1.7  5.9

corr_km_gp      1       
corr_km_gm  -0.69       1    
corr_gp_gm   0.77   -0.08       1 

Eigenvalues (min, max, max/min): 0.0026  2.1  7.9e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.42e+03 seconds. 
CPU time is 1.57e+04 seconds. 
