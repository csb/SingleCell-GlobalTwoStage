******************************************************************
*      model_mlxtran5.mlxtran
*      March 04, 2018 at 14:41:06
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.31         0.04           2    
kp_pop     :  -0.0545          -           -      
gp_pop     :    -5.26        0.034           1    
gm_pop     :    -3.69        0.051           1    
ltau_pop   :      3.4          -           -      

omega_km   :    0.653         0.03           5    
omega_kp   :        0          -           -      
omega_gp   :    0.559        0.027           5    
omega_gm   :    0.863        0.039           5    
omega_ltau :        0          -           -      
corr_km_gp :    -0.12        0.067          56    
corr_km_gm :    0.782        0.027           3    
corr_gp_gm :   -0.687        0.035           5    

a          :     62.1         0.79           1    
b          :   0.0867       0.0006           1    

correlation matrix (IIV)
km      1       
gp  -0.12       1    
gm   0.78   -0.69       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.04       1    
gm_pop   0.75   -0.66       1 

Eigenvalues (min, max, max/min): 0.016  2  1.2e+02

omega_km      1             
omega_gp  -0.01       1          
omega_gm   0.57    0.44       1       
a            -0      -0      -0       1    
b         -0.01   -0.01   -0.01   -0.39       1 

Eigenvalues (min, max, max/min): 0.28  1.7  6.2

corr_km_gp      1       
corr_km_gm  -0.69       1    
corr_gp_gm   0.77   -0.08       1 

Eigenvalues (min, max, max/min): 0.0019  2.1  1.1e+03


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.3e+03 seconds. 
CPU time is 1.42e+04 seconds. 
