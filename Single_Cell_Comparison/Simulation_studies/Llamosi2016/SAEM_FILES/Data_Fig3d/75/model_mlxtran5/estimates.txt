          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.31453;   0.03959;      1.71;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.25613;   0.03397;      0.65;        NaN
    gm_pop;  -3.68836;   0.05102;      1.38;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.65297;   0.03020;      4.63;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.55876;   0.02669;      4.78;        NaN
  omega_gm;   0.86252;   0.03925;      4.55;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.12039;   0.06704;     55.69;        NaN
corr_km_gm;   0.78223;   0.02659;      3.40;        NaN
corr_gp_gm;  -0.68665;   0.03537;      5.15;        NaN
         a;  62.07107;   0.79263;      1.28;        NaN
         b;   0.08673;   0.00060;      0.70;        NaN
