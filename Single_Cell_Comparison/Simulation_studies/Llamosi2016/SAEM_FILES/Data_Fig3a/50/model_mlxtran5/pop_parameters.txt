******************************************************************
*      model_mlxtran5.mlxtran
*      July 19, 2017 at 22:03:49
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.39         0.071          3    
kp_pop     :  -0.0545           -          -      
gp_pop     :     -5.2         0.072          1    
gm_pop     :    -3.61          0.08          2    
ltau_pop   :      3.4           -          -      

omega_km   :    0.499          0.05         10    
omega_kp   :        0           -          -      
omega_gp   :    0.505         0.051         10    
omega_gm   :    0.566         0.057         10    
omega_ltau :        0           -          -      
corr_km_gp :  -0.0474          0.14        299    
corr_km_gm :    0.698         0.073         10    
corr_gp_gm :   -0.716         0.069         10    

a          :     14.1          0.38          3    
b          :    0.023       0.00032          1    

correlation matrix (IIV)
km      1       
gp  -0.05       1    
gm    0.7   -0.72       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.05       1    
gm_pop    0.7   -0.72       1 

Eigenvalues (min, max, max/min): 0.024  2  85

omega_km      1             
omega_gp      0       1          
omega_gm   0.48    0.51       1       
a            -0      -0      -0       1    
b            -0      -0      -0   -0.36       1 

Eigenvalues (min, max, max/min): 0.29  1.7  5.8

corr_km_gp      1       
corr_km_gm  -0.71       1    
corr_gp_gm   0.69       0       1 

Eigenvalues (min, max, max/min): 0.0034  2  5.9e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 914 seconds. 
CPU time is 3.51e+03 seconds. 
