          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.39469;   0.08222;      3.43;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.10416;   0.05445;      1.07;        NaN
    gm_pop;  -3.66841;   0.07932;      2.16;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.58108;   0.05817;     10.01;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.38226;   0.03877;     10.14;        NaN
  omega_gm;   0.55811;   0.05636;     10.10;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.16603;   0.13862;     83.49;        NaN
corr_km_gm;   0.81392;   0.04837;      5.94;        NaN
corr_gp_gm;  -0.61495;   0.08868;     14.42;        NaN
         a;  13.23635;   0.35239;      2.66;        NaN
         b;   0.02295;   0.00031;      1.37;        NaN
