******************************************************************
*      model_mlxtran3.mlxtran
*      July 19, 2017 at 21:29:20
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.39         0.082          3    
kp_pop     :  -0.0545           -          -      
gp_pop     :     -5.1         0.054          1    
gm_pop     :    -3.67         0.079          2    
ltau_pop   :      3.4           -          -      

omega_km   :    0.581         0.058         10    
omega_kp   :        0           -          -      
omega_gp   :    0.382         0.039         10    
omega_gm   :    0.558         0.056         10    
omega_ltau :        0           -          -      
corr_km_gp :   -0.166          0.14         83    
corr_km_gm :    0.814         0.048          6    
corr_gp_gm :   -0.615         0.089         14    

a          :     13.2          0.35          3    
b          :    0.023       0.00031          1    

correlation matrix (IIV)
km      1       
gp  -0.17       1    
gm   0.81   -0.61       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.16       1    
gm_pop   0.81   -0.62       1 

Eigenvalues (min, max, max/min): 0.056  2.1  38

omega_km      1             
omega_gp   0.03       1          
omega_gm   0.66    0.38       1       
a            -0      -0      -0       1    
b            -0      -0      -0   -0.35       1 

Eigenvalues (min, max, max/min): 0.25  1.8  7.1

corr_km_gp      1       
corr_km_gm   -0.6       1    
corr_gp_gm    0.8   -0.05       1 

Eigenvalues (min, max, max/min): 0.022  2  93


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 956 seconds. 
CPU time is 3.73e+03 seconds. 
