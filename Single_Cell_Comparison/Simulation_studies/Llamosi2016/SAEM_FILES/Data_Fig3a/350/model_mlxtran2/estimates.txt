          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.37750;   0.03509;      1.48;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.30806;   0.03005;      0.57;        NaN
    gm_pop;  -3.54109;   0.04551;      1.29;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.64646;   0.02518;      3.89;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.53490;   0.02206;      4.12;        NaN
  omega_gm;   0.82327;   0.03321;      4.03;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.08833;   0.05660;     64.08;        NaN
corr_km_gm;   0.75172;   0.02481;      3.30;        NaN
corr_gp_gm;  -0.69088;   0.02978;      4.31;        NaN
         a;  62.41695;   0.61618;      0.99;        NaN
         b;   0.08602;   0.00047;      0.55;        NaN
