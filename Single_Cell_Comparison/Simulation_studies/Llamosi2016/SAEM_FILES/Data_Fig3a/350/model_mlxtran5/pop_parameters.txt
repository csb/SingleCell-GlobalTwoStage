******************************************************************
*      model_mlxtran5.mlxtran
*      March 03, 2018 at 04:20:29
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :      2.33        0.036             2 
kp_pop     :   -0.0545          -             -   
gp_pop     :     -5.29        0.029             1 
gm_pop     :     -3.64        0.044             1 
ltau_pop   :       3.4          -             -   

omega_km   :     0.669        0.026             4 
omega_kp   :         0          -             -   
omega_gp   :     0.512        0.021             4 
omega_gm   :     0.801        0.032             4 
omega_ltau :         0          -             -   
corr_km_gp :  -0.00459        0.057      1.25e+03 
corr_km_gm :     0.774        0.023             3 
corr_gp_gm :    -0.604        0.036             6 

a          :      61.8         0.61             1 
b          :    0.0874      0.00047             1 

correlation matrix (IIV)
km      1       
gp     -0       1    
gm   0.77    -0.6       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.01       1    
gm_pop   0.76   -0.63       1 

Eigenvalues (min, max, max/min): 0.021  2  95

omega_km      1             
omega_gp     -0       1          
omega_gm   0.57    0.39       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.38       1 

Eigenvalues (min, max, max/min): 0.31  1.7  5.5

corr_km_gp      1       
corr_km_gm  -0.61       1    
corr_gp_gm   0.77    0.03       1 

Eigenvalues (min, max, max/min): 0.0026  2  7.7e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 7.46e+03 seconds. 
CPU time is 1.97e+04 seconds. 
