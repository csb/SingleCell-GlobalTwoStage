          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.49771;   0.05395;      2.16;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.34359;   0.04320;      0.81;        NaN
    gm_pop;  -3.40898;   0.07085;      2.08;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.65035;   0.03873;      5.95;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.50868;   0.03157;      6.21;        NaN
  omega_gm;   0.84422;   0.05146;      6.10;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.18191;   0.08347;     45.89;        NaN
corr_km_gm;   0.82666;   0.02742;      3.32;        NaN
corr_gp_gm;  -0.67503;   0.04694;      6.95;        NaN
         a;  61.57616;   0.93724;      1.52;        NaN
         b;   0.08536;   0.00071;      0.83;        NaN
