******************************************************************
*      model_mlxtran2.mlxtran
*      March 01, 2018 at 14:02:35
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.36         0.054          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.31         0.047          1    
gm_pop     :    -3.56         0.064          2    
ltau_pop   :      3.4           -          -      

omega_km   :    0.656         0.039          6    
omega_kp   :        0           -          -      
omega_gp   :    0.545         0.034          6    
omega_gm   :    0.757         0.047          6    
omega_ltau :        0           -          -      
corr_km_gp :    0.131         0.086         66    
corr_km_gm :    0.678         0.047          7    
corr_gp_gm :   -0.595         0.056          9    

a          :     63.4          0.95          2    
b          :   0.0852       0.00072          1    

correlation matrix (IIV)
km      1       
gp   0.13       1    
gm   0.68    -0.6       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.12       1    
gm_pop   0.66   -0.62       1 

Eigenvalues (min, max, max/min): 0.03  1.9  62

omega_km      1             
omega_gp   0.01       1          
omega_gm   0.44    0.38       1       
a            -0      -0      -0       1    
b            -0      -0      -0    -0.4       1 

Eigenvalues (min, max, max/min): 0.43  1.6  3.7

corr_km_gp      1       
corr_km_gm  -0.61       1    
corr_gp_gm   0.68    0.16       1 

Eigenvalues (min, max, max/min): 0.0043  1.8  4.3e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 3.49e+03 seconds. 
CPU time is 9.27e+03 seconds. 
