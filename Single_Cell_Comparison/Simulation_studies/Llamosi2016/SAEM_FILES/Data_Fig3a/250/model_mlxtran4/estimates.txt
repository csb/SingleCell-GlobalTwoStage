          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.34551;   0.04800;      2.05;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.32303;   0.03485;      0.65;        NaN
    gm_pop;  -3.55444;   0.06131;      1.72;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.74828;   0.03438;      4.59;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.51616;   0.02575;      4.99;        NaN
  omega_gm;   0.94018;   0.04462;      4.75;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.09941;   0.06783;     68.23;        NaN
corr_km_gm;   0.81339;   0.02297;      2.82;        NaN
corr_gp_gm;  -0.63221;   0.04104;      6.49;        NaN
         a;  62.51144;   0.72622;      1.16;        NaN
         b;   0.08607;   0.00056;      0.65;        NaN
