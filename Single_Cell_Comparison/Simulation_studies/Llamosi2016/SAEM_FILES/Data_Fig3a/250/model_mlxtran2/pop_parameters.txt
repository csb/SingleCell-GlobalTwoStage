******************************************************************
*      model_mlxtran2.mlxtran
*      March 02, 2018 at 12:17:31
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.46         0.046          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.31         0.032          1    
gm_pop     :    -3.46         0.057          2    
ltau_pop   :      3.4           -          -      

omega_km   :    0.711         0.033          5    
omega_kp   :        0           -          -      
omega_gp   :    0.473         0.023          5    
omega_gm   :    0.871         0.041          5    
omega_ltau :        0           -          -      
corr_km_gp :    -0.17         0.066         39    
corr_km_gm :    0.833         0.021          2    
corr_gp_gm :   -0.653         0.039          6    

a          :     62.7          0.74          1    
b          :   0.0862       0.00056          1    

correlation matrix (IIV)
km      1       
gp  -0.17       1    
gm   0.83   -0.65       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.16       1    
gm_pop   0.82   -0.67       1 

Eigenvalues (min, max, max/min): 0.02  2.1  1.1e+02

omega_km      1             
omega_gp   0.03       1          
omega_gm   0.67    0.44       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.39       1 

Eigenvalues (min, max, max/min): 0.21  1.8  8.7

corr_km_gp      1       
corr_km_gm  -0.65       1    
corr_gp_gm   0.83   -0.12       1 

Eigenvalues (min, max, max/min): 0.0032  2.1  6.6e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.66e+03 seconds. 
CPU time is 1.52e+04 seconds. 
