******************************************************************
*      model_mlxtran2.mlxtran
*      March 01, 2018 at 23:23:56
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.36        0.029           1    
kp_pop     :  -0.0545          -           -      
gp_pop     :    -5.27        0.025           0    
gm_pop     :    -3.61        0.039           1    
ltau_pop   :      3.4          -           -      

omega_km   :    0.641        0.021           3    
omega_kp   :        0          -           -      
omega_gp   :    0.518        0.018           4    
omega_gm   :    0.837        0.028           3    
omega_ltau :        0          -           -      
corr_km_gp :  -0.0946        0.048          51    
corr_km_gm :     0.76         0.02           3    
corr_gp_gm :   -0.685        0.026           4    

a          :     63.2         0.52           1    
b          :   0.0866       0.0004           0    

correlation matrix (IIV)
km      1       
gp  -0.09       1    
gm   0.76   -0.69       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.08       1    
gm_pop   0.74    -0.7       1 

Eigenvalues (min, max, max/min): 0.022  2.1  94

omega_km      1             
omega_gp   0.01       1          
omega_gm   0.55    0.49       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.39       1 

Eigenvalues (min, max, max/min): 0.27  1.7  6.4

corr_km_gp      1       
corr_km_gm  -0.69       1    
corr_gp_gm   0.76   -0.05       1 

Eigenvalues (min, max, max/min): 0.0032  2  6.5e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 1.21e+04 seconds. 
CPU time is 3.47e+04 seconds. 
