******************************************************************
*      model_mlxtran4.mlxtran
*      March 02, 2018 at 06:14:29
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :      2.4        0.029           1    
kp_pop     :  -0.0545          -           -      
gp_pop     :    -5.25        0.023           0    
gm_pop     :    -3.57        0.038           1    
ltau_pop   :      3.4          -           -      

omega_km   :    0.646        0.021           3    
omega_kp   :        0          -           -      
omega_gp   :    0.494        0.017           4    
omega_gm   :    0.816        0.028           3    
omega_ltau :        0          -           -      
corr_km_gp :  -0.0769        0.048          62    
corr_km_gm :    0.771        0.019           3    
corr_gp_gm :   -0.658        0.027           4    

a          :     61.3         0.51           1    
b          :   0.0874       0.0004           0    

correlation matrix (IIV)
km      1       
gp  -0.08       1    
gm   0.77   -0.66       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.07       1    
gm_pop   0.75   -0.68       1 

Eigenvalues (min, max, max/min): 0.023  2  89

omega_km      1             
omega_gp      0       1          
omega_gm   0.56    0.45       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.38       1 

Eigenvalues (min, max, max/min): 0.28  1.7  6.2

corr_km_gp      1       
corr_km_gm  -0.66       1    
corr_gp_gm   0.77   -0.03       1 

Eigenvalues (min, max, max/min): 0.0034  2  5.9e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 1.21e+04 seconds. 
CPU time is 3.44e+04 seconds. 
