          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.44181;   0.03952;      1.62;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.25261;   0.03489;      0.66;        NaN
    gm_pop;  -3.53626;   0.05278;      1.49;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.70030;   0.02931;      4.19;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.55814;   0.02796;      5.01;        NaN
  omega_gm;   0.87941;   0.04175;      4.75;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.01465;   0.06644;    453.63;        NaN
corr_km_gm;   0.73924;   0.02920;      3.95;        NaN
corr_gp_gm;  -0.63786;   0.03950;      6.19;        NaN
         a;  43.59929;   0.46989;      1.08;        NaN
         b;   0.35384;   0.00152;      0.43;        NaN
