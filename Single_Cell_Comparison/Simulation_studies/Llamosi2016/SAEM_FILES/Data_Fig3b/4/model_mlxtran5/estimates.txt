          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.34549;   0.03759;      1.60;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.27410;   0.03215;      0.61;        NaN
    gm_pop;  -3.62362;   0.04982;      1.37;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.66926;   0.02780;      4.15;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.48834;   0.02665;      5.46;        NaN
  omega_gm;   0.81775;   0.03998;      4.89;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.03820;   0.06954;    182.07;        NaN
corr_km_gm;   0.78168;   0.02591;      3.31;        NaN
corr_gp_gm;  -0.62089;   0.04315;      6.95;        NaN
         a;  44.12349;   0.47505;      1.08;        NaN
         b;   0.34994;   0.00151;      0.43;        NaN
