          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.38249;   0.03850;      1.62;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.26204;   0.03500;      0.67;        NaN
    gm_pop;  -3.58698;   0.05350;      1.49;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.68198;   0.02855;      4.19;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.54916;   0.02840;      5.17;        NaN
  omega_gm;   0.89039;   0.04239;      4.76;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.06486;   0.06745;    103.98;        NaN
corr_km_gm;   0.77437;   0.02632;      3.40;        NaN
corr_gp_gm;  -0.65308;   0.03891;      5.96;        NaN
         a;  43.51643;   0.46903;      1.08;        NaN
         b;   0.35277;   0.00152;      0.43;        NaN
