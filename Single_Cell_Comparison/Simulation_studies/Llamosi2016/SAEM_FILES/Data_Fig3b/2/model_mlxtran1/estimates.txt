          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.38641;   0.03779;      1.58;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.28591;   0.03055;      0.58;        NaN
    gm_pop;  -3.57558;   0.05089;      1.42;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.68984;   0.02733;      3.96;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.51602;   0.02332;      4.52;        NaN
  omega_gm;   0.90180;   0.03789;      4.20;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.17201;   0.05902;     34.32;        NaN
corr_km_gm;   0.80717;   0.02092;      2.59;        NaN
corr_gp_gm;  -0.69181;   0.03172;      4.58;        NaN
         a;  44.71466;   0.46955;      1.05;        NaN
         b;   0.17840;   0.00081;      0.45;        NaN
