******************************************************************
*      model_mlxtran1.mlxtran
*      March 24, 2018 at 19:10:39
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.39         0.038          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.29         0.031          1    
gm_pop     :    -3.58         0.051          1    
ltau_pop   :      3.4           -          -      

omega_km   :     0.69         0.027          4    
omega_kp   :        0           -          -      
omega_gp   :    0.516         0.023          5    
omega_gm   :    0.902         0.038          4    
omega_ltau :        0           -          -      
corr_km_gp :   -0.172         0.059         34    
corr_km_gm :    0.807         0.021          3    
corr_gp_gm :   -0.692         0.032          5    

a          :     44.7          0.47          1    
b          :    0.178       0.00081          0    

correlation matrix (IIV)
km      1       
gp  -0.17       1    
gm   0.81   -0.69       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.16       1    
gm_pop   0.78   -0.71       1 

Eigenvalues (min, max, max/min): 0.018  2.1  1.2e+02

omega_km      1             
omega_gp   0.02       1          
omega_gm   0.61     0.5       1       
a            -0      -0      -0       1    
b            -0   -0.01   -0.01    -0.2       1 

Eigenvalues (min, max, max/min): 0.22  1.8  8.1

corr_km_gp      1       
corr_km_gm  -0.69       1    
corr_gp_gm   0.81   -0.13       1 

Eigenvalues (min, max, max/min): 0.0026  2.1  8.3e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 1.97e+03 seconds. 
CPU time is 1.79e+04 seconds. 
