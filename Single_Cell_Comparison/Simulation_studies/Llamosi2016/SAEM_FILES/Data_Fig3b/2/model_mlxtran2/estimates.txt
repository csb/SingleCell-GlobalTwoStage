          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.38262;   0.03556;      1.49;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.31104;   0.03115;      0.59;        NaN
    gm_pop;  -3.53339;   0.04753;      1.35;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.64758;   0.02578;      3.98;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.53058;   0.02367;      4.46;        NaN
  omega_gm;   0.83586;   0.03564;      4.26;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.12917;   0.05937;     45.96;        NaN
corr_km_gm;   0.76783;   0.02459;      3.20;        NaN
corr_gp_gm;  -0.70211;   0.03064;      4.36;        NaN
         a;  44.96962;   0.47230;      1.05;        NaN
         b;   0.17794;   0.00081;      0.46;        NaN
