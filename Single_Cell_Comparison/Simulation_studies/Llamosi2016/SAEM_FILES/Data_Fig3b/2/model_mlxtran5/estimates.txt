          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.35737;   0.03728;      1.58;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.23265;   0.03259;      0.62;        NaN
    gm_pop;  -3.66425;   0.05032;      1.37;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.68095;   0.02693;      3.96;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.54528;   0.02500;      4.59;        NaN
  omega_gm;   0.88334;   0.03776;      4.28;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.07661;   0.06093;     79.54;        NaN
corr_km_gm;   0.77170;   0.02462;      3.19;        NaN
corr_gp_gm;  -0.66518;   0.03411;      5.13;        NaN
         a;  44.50220;   0.46689;      1.05;        NaN
         b;   0.18041;   0.00082;      0.45;        NaN
