          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.42901;   0.04123;      1.70;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.29582;   0.03947;      0.75;        NaN
    gm_pop;  -3.56476;   0.06024;      1.69;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.68827;   0.03212;      4.67;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.55234;   0.03563;      6.45;        NaN
  omega_gm;   0.92013;   0.05220;      5.67;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.13636;   0.08005;     58.71;        NaN
corr_km_gm;   0.79733;   0.02784;      3.49;        NaN
corr_gp_gm;  -0.67924;   0.04445;      6.54;        NaN
         a;  43.86170;   0.48069;      1.10;        NaN
         b;   0.63124;   0.00265;      0.42;        NaN
