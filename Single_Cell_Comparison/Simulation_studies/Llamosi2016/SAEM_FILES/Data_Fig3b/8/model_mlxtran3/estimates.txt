          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.39711;   0.03939;      1.64;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.27296;   0.03992;      0.76;        NaN
    gm_pop;  -3.58555;   0.05705;      1.59;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.65629;   0.03080;      4.69;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.55932;   0.03611;      6.46;        NaN
  omega_gm;   0.84117;   0.05109;      6.07;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.01648;   0.08181;    496.56;        NaN
corr_km_gm;   0.74124;   0.03465;      4.67;        NaN
corr_gp_gm;  -0.65504;   0.04757;      7.26;        NaN
         a;  43.09780;   0.47227;      1.10;        NaN
         b;   0.63527;   0.00267;      0.42;        NaN
