******************************************************************
*      model_mlxtran2.mlxtran
*      March 24, 2018 at 22:09:08
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.39        0.038           2    
kp_pop     :  -0.0545          -           -      
gp_pop     :     -5.3        0.036           1    
gm_pop     :    -3.56        0.055           2    
ltau_pop   :      3.4          -           -      

omega_km   :    0.635         0.03           5    
omega_kp   :        0          -           -      
omega_gp   :    0.459        0.035           8    
omega_gm   :    0.805         0.05           6    
omega_ltau :        0          -           -      
corr_km_gp :   -0.124        0.088          71    
corr_km_gm :    0.793         0.03           4    
corr_gp_gm :   -0.665        0.052           8    

a          :     43.2         0.47           1    
b          :    0.633       0.0027           0    

correlation matrix (IIV)
km      1       
gp  -0.12       1    
gm   0.79   -0.66       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.18       1    
gm_pop   0.77   -0.74       1 

Eigenvalues (min, max, max/min): 0.021  2.2  1e+02

omega_km      1             
omega_gp   0.03       1          
omega_gm   0.58    0.54       1       
a         -0.01      -0   -0.01       1    
b         -0.02   -0.04   -0.03   -0.08       1 

Eigenvalues (min, max, max/min): 0.22  1.8  8.1

corr_km_gp      1       
corr_km_gm  -0.66       1    
corr_gp_gm   0.83   -0.13       1 

Eigenvalues (min, max, max/min): 0.0032  2.1  6.6e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 1.62e+03 seconds. 
CPU time is 1.3e+04 seconds. 
