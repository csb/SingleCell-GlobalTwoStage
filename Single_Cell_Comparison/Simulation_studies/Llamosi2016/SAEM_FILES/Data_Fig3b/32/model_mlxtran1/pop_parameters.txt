******************************************************************
*      model_mlxtran1.mlxtran
*      March 24, 2018 at 23:51:26
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.83        0.043           2    
kp_pop     :  -0.0545          -           -      
gp_pop     :    -5.31         0.05           1    
gm_pop     :    -3.65        0.066           2    
ltau_pop   :      3.4          -           -      

omega_km   :     0.64        0.037           6    
omega_kp   :        0          -           -      
omega_gp   :    0.573        0.055          10    
omega_gm   :    0.782        0.074           9    
omega_ltau :        0          -           -      
corr_km_gp :    0.157         0.12          74    
corr_km_gm :    0.687        0.055           8    
corr_gp_gm :   -0.566        0.084          15    

a          :     43.2         0.48           1    
b          :     1.15       0.0047           0    

correlation matrix (IIV)
km      1       
gp   0.16       1    
gm   0.69   -0.57       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.05       1    
gm_pop   0.68   -0.73       1 

Eigenvalues (min, max, max/min): 0.025  2  82

omega_km      1             
omega_gp  -0.01       1          
omega_gm   0.44     0.5       1       
a         -0.01      -0   -0.01       1    
b         -0.03   -0.03   -0.04   -0.04       1 

Eigenvalues (min, max, max/min): 0.33  1.7  5

corr_km_gp      1       
corr_km_gm  -0.57       1    
corr_gp_gm   0.77    0.08       1 

Eigenvalues (min, max, max/min): 0.0031  1.9  6.3e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 1.47e+03 seconds. 
CPU time is 1.09e+04 seconds. 
