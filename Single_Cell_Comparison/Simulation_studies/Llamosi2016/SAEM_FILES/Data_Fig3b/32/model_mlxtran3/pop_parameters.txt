******************************************************************
*      model_mlxtran3.mlxtran
*      March 25, 2018 at 00:43:12
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.92        0.047           2    
kp_pop     :  -0.0545          -           -      
gp_pop     :    -5.33        0.051           1    
gm_pop     :    -3.53        0.069           2    
ltau_pop   :      3.4          -           -      

omega_km   :    0.694         0.04           6    
omega_kp   :        0          -           -      
omega_gp   :    0.644        0.051           8    
omega_gm   :      0.9        0.071           8    
omega_ltau :        0          -           -      
corr_km_gp :   0.0679          0.1         153    
corr_km_gm :    0.732        0.044           6    
corr_gp_gm :   -0.599        0.067          11    

a          :     43.9         0.49           1    
b          :     1.15       0.0047           0    

correlation matrix (IIV)
km      1       
gp   0.07       1    
gm   0.73    -0.6       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.06       1    
gm_pop   0.73    -0.7       1 

Eigenvalues (min, max, max/min): 0.022  2  95

omega_km      1             
omega_gp  -0.01       1          
omega_gm    0.5    0.47       1       
a         -0.01      -0   -0.01       1    
b         -0.03   -0.03   -0.03   -0.04       1 

Eigenvalues (min, max, max/min): 0.31  1.7  5.4

corr_km_gp      1       
corr_km_gm  -0.62       1    
corr_gp_gm   0.78       0       1 

Eigenvalues (min, max, max/min): 0.0024  2  8.4e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 1.52e+03 seconds. 
CPU time is 1.16e+04 seconds. 
