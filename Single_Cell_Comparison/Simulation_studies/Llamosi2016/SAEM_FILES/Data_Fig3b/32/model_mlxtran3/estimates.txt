          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.91891;   0.04652;      1.59;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.33452;   0.05079;      0.95;        NaN
    gm_pop;  -3.52594;   0.06934;      1.97;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.69393;   0.03981;      5.74;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.64408;   0.05066;      7.86;        NaN
  omega_gm;   0.90009;   0.07092;      7.88;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.06788;   0.10370;    152.77;        NaN
corr_km_gm;   0.73234;   0.04394;      6.00;        NaN
corr_gp_gm;  -0.59934;   0.06696;     11.17;        NaN
         a;  43.90281;   0.49089;      1.12;        NaN
         b;   1.14911;   0.00471;      0.41;        NaN
