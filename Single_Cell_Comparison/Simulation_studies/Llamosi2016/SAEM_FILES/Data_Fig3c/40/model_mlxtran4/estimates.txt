          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.40525;   0.03894;      1.62;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.28788;   0.02890;      0.55;        NaN
    gm_pop;  -3.53832;   0.04547;      1.29;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.71871;   0.02789;      3.88;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.51126;   0.02135;      4.18;        NaN
  omega_gm;   0.82035;   0.03328;      4.06;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.11456;   0.05666;     49.46;        NaN
corr_km_gm;   0.80010;   0.02067;      2.58;        NaN
corr_gp_gm;  -0.45614;   0.04542;      9.96;        NaN
         a;  65.15588;   0.64931;      1.00;        NaN
         b;   0.10051;   0.00053;      0.53;        NaN
