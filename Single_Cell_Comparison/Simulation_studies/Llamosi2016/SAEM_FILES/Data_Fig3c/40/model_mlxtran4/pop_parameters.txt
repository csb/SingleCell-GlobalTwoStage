******************************************************************
*      model_mlxtran4.mlxtran
*      March 06, 2018 at 11:30:24
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.41         0.039          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.29         0.029          1    
gm_pop     :    -3.54         0.045          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.719         0.028          4    
omega_kp   :        0           -          -      
omega_gp   :    0.511         0.021          4    
omega_gm   :     0.82         0.033          4    
omega_ltau :        0           -          -      
corr_km_gp :    0.115         0.057         49    
corr_km_gm :      0.8         0.021          3    
corr_gp_gm :   -0.456         0.045         10    

a          :     65.2          0.65          1    
b          :    0.101       0.00053          1    

correlation matrix (IIV)
km      1       
gp   0.11       1    
gm    0.8   -0.46       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop    0.1       1    
gm_pop   0.78   -0.49       1 

Eigenvalues (min, max, max/min): 0.027  1.9  70

omega_km      1             
omega_gp   0.01       1          
omega_gm   0.62    0.23       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.37       1 

Eigenvalues (min, max, max/min): 0.34  1.7  4.8

corr_km_gp      1       
corr_km_gm  -0.47       1    
corr_gp_gm    0.8    0.15       1 

Eigenvalues (min, max, max/min): 0.004  1.9  4.6e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 4.65e+03 seconds. 
CPU time is 1.98e+04 seconds. 
