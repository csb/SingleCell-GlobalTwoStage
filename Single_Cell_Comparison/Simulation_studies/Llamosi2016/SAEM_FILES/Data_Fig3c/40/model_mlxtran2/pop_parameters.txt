******************************************************************
*      model_mlxtran2.mlxtran
*      March 06, 2018 at 08:53:20
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.38         0.034          1    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.34         0.035          1    
gm_pop     :    -3.53         0.044          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.624         0.024          4    
omega_kp   :        0           -          -      
omega_gp   :    0.618         0.025          4    
omega_gm   :    0.797         0.033          4    
omega_ltau :        0           -          -      
corr_km_gp :   0.0757         0.057         75    
corr_km_gm :     0.68         0.031          5    
corr_gp_gm :    -0.64         0.034          5    

a          :     64.1          0.64          1    
b          :   0.0958       0.00052          1    

correlation matrix (IIV)
km      1       
gp   0.08       1    
gm   0.68   -0.64       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.07       1    
gm_pop   0.66   -0.66       1 

Eigenvalues (min, max, max/min): 0.029  1.9  66

omega_km      1             
omega_gp      0       1          
omega_gm   0.44    0.43       1       
a            -0      -0      -0       1    
b            -0      -0      -0   -0.38       1 

Eigenvalues (min, max, max/min): 0.39  1.6  4.2

corr_km_gp      1       
corr_km_gm  -0.65       1    
corr_gp_gm   0.67    0.12       1 

Eigenvalues (min, max, max/min): 0.0041  1.9  4.6e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.1e+03 seconds. 
CPU time is 2.23e+04 seconds. 
