          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.37042;   0.03532;      1.49;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.28801;   0.03193;      0.60;        NaN
    gm_pop;  -3.57812;   0.04740;      1.32;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.64929;   0.02536;      3.91;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.56423;   0.02356;      4.17;        NaN
  omega_gm;   0.85387;   0.03472;      4.07;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.06018;   0.05738;     95.35;        NaN
corr_km_gm;   0.76036;   0.02437;      3.20;        NaN
corr_gp_gm;  -0.66172;   0.03226;      4.87;        NaN
         a;  62.92760;   0.62439;      0.99;        NaN
         b;   0.09570;   0.00051;      0.54;        NaN
