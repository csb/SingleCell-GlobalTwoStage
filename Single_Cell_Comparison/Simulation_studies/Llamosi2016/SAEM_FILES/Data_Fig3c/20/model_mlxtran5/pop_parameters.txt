******************************************************************
*      model_mlxtran5.mlxtran
*      March 06, 2018 at 06:08:47
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.34        0.036           2    
kp_pop     :  -0.0545          -           -      
gp_pop     :    -5.25        0.031           1    
gm_pop     :    -3.67        0.047           1    
ltau_pop   :      3.4          -           -      

omega_km   :    0.671        0.026           4    
omega_kp   :        0          -           -      
omega_gp   :    0.541        0.023           4    
omega_gm   :    0.849        0.034           4    
omega_ltau :        0          -           -      
corr_km_gp :  -0.0407        0.057         141    
corr_km_gm :    0.777        0.023           3    
corr_gp_gm :   -0.628        0.035           6    

a          :       63         0.62           1    
b          :   0.0926       0.0005           1    

correlation matrix (IIV)
km      1       
gp  -0.04       1    
gm   0.78   -0.63       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.04       1    
gm_pop   0.76   -0.65       1 

Eigenvalues (min, max, max/min): 0.021  2  96

omega_km      1             
omega_gp     -0       1          
omega_gm   0.57    0.42       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.38       1 

Eigenvalues (min, max, max/min): 0.29  1.7  5.8

corr_km_gp      1       
corr_km_gm  -0.63       1    
corr_gp_gm   0.77       0       1 

Eigenvalues (min, max, max/min): 0.0027  2  7.4e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 4.92e+03 seconds. 
CPU time is 2.13e+04 seconds. 
