******************************************************************
*      model_mlxtran1.mlxtran
*      March 06, 2018 at 00:33:52
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.35         0.037          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.27         0.031          1    
gm_pop     :    -3.64         0.049          1    
ltau_pop   :      3.4           -          -      

omega_km   :     0.68         0.026          4    
omega_kp   :        0           -          -      
omega_gp   :    0.542         0.023          4    
omega_gm   :    0.891         0.036          4    
omega_ltau :        0           -          -      
corr_km_gp :  -0.0874         0.058         66    
corr_km_gm :    0.783         0.023          3    
corr_gp_gm :   -0.657         0.033          5    

a          :     62.6          0.62          1    
b          :   0.0953       0.00051          1    

correlation matrix (IIV)
km      1       
gp  -0.09       1    
gm   0.78   -0.66       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop  -0.07       1    
gm_pop   0.76   -0.68       1 

Eigenvalues (min, max, max/min): 0.02  2.1  1e+02

omega_km      1             
omega_gp      0       1          
omega_gm   0.57    0.45       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.37       1 

Eigenvalues (min, max, max/min): 0.27  1.7  6.4

corr_km_gp      1       
corr_km_gm  -0.66       1    
corr_gp_gm   0.78   -0.05       1 

Eigenvalues (min, max, max/min): 0.0027  2  7.6e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.44e+03 seconds. 
CPU time is 2.4e+04 seconds. 
