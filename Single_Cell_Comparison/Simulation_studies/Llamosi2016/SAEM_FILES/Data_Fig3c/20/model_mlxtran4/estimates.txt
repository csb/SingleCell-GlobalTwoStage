          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.41691;   0.03853;      1.59;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.25596;   0.03211;      0.61;        NaN
    gm_pop;  -3.55847;   0.04796;      1.35;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.71083;   0.02760;      3.88;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.56236;   0.02378;      4.23;        NaN
  omega_gm;   0.86255;   0.03516;      4.08;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.03976;   0.05774;    145.22;        NaN
corr_km_gm;   0.74659;   0.02563;      3.43;        NaN
corr_gp_gm;  -0.60015;   0.03691;      6.15;        NaN
         a;  63.33196;   0.62614;      0.99;        NaN
         b;   0.09237;   0.00050;      0.54;        NaN
