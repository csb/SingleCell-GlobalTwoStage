******************************************************************
*      model_mlxtran4.mlxtran
*      March 07, 2018 at 01:39:32
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.43         0.038          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :     -5.3         0.031          1    
gm_pop     :    -3.51         0.045          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.706         0.028          4    
omega_kp   :        0           -          -      
omega_gp   :    0.537         0.023          4    
omega_gm   :    0.805         0.033          4    
omega_ltau :        0           -          -      
corr_km_gp :    0.133         0.057         43    
corr_km_gm :     0.78         0.023          3    
corr_gp_gm :   -0.469         0.045         10    

a          :     68.1          0.68          1    
b          :    0.111       0.00058          1    

correlation matrix (IIV)
km      1       
gp   0.13       1    
gm   0.78   -0.47       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.12       1    
gm_pop   0.76   -0.51       1 

Eigenvalues (min, max, max/min): 0.028  1.9  67

omega_km      1             
omega_gp   0.01       1          
omega_gm   0.58    0.25       1       
a            -0      -0      -0       1    
b            -0   -0.01      -0   -0.36       1 

Eigenvalues (min, max, max/min): 0.37  1.6  4.4

corr_km_gp      1       
corr_km_gm  -0.49       1    
corr_gp_gm   0.78    0.16       1 

Eigenvalues (min, max, max/min): 0.0041  1.9  4.5e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 4.61e+03 seconds. 
CPU time is 1.97e+04 seconds. 
