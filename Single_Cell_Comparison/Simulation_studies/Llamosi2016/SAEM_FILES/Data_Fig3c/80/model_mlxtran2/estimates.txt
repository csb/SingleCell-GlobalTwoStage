          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.42253;   0.03379;      1.39;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.30183;   0.03546;      0.67;        NaN
    gm_pop;  -3.53622;   0.04936;      1.40;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.61668;   0.02457;      3.98;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.63067;   0.02614;      4.14;        NaN
  omega_gm;   0.88760;   0.03633;      4.09;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.07938;   0.05731;     72.19;        NaN
corr_km_gm;   0.74100;   0.02633;      3.55;        NaN
corr_gp_gm;  -0.69268;   0.02980;      4.30;        NaN
         a;  65.75622;   0.65872;      1.00;        NaN
         b;   0.10798;   0.00057;      0.53;        NaN
