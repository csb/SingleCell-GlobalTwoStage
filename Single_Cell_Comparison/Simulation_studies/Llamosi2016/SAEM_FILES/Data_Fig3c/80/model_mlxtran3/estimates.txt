          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.38362;   0.03638;      1.53;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.28635;   0.03387;      0.64;        NaN
    gm_pop;  -3.58042;   0.04824;      1.35;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.66886;   0.02616;      3.91;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.59768;   0.02506;      4.19;        NaN
  omega_gm;   0.86666;   0.03544;      4.09;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.03674;   0.05761;    156.79;        NaN
corr_km_gm;   0.77840;   0.02300;      2.95;        NaN
corr_gp_gm;  -0.55598;   0.03971;      7.14;        NaN
         a;  64.29078;   0.64422;      1.00;        NaN
         b;   0.11004;   0.00057;      0.52;        NaN
