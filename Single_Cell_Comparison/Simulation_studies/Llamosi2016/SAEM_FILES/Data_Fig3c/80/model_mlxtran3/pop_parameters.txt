******************************************************************
*      model_mlxtran3.mlxtran
*      March 07, 2018 at 00:20:08
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.38         0.036          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.29         0.034          1    
gm_pop     :    -3.58         0.048          1    
ltau_pop   :      3.4           -          -      

omega_km   :    0.669         0.026          4    
omega_kp   :        0           -          -      
omega_gp   :    0.598         0.025          4    
omega_gm   :    0.867         0.035          4    
omega_ltau :        0           -          -      
corr_km_gp :   0.0367         0.058        157    
corr_km_gm :    0.778         0.023          3    
corr_gp_gm :   -0.556          0.04          7    

a          :     64.3          0.64          1    
b          :     0.11       0.00057          1    

correlation matrix (IIV)
km      1       
gp   0.04       1    
gm   0.78   -0.56       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.04       1    
gm_pop   0.75   -0.59       1 

Eigenvalues (min, max, max/min): 0.028  1.9  70

omega_km      1             
omega_gp     -0       1          
omega_gm   0.57    0.34       1       
a            -0      -0      -0       1    
b            -0      -0      -0   -0.35       1 

Eigenvalues (min, max, max/min): 0.34  1.7  4.9

corr_km_gp      1       
corr_km_gm  -0.57       1    
corr_gp_gm   0.77    0.08       1 

Eigenvalues (min, max, max/min): 0.0041  1.9  4.7e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 4.67e+03 seconds. 
CPU time is 2e+04 seconds. 
