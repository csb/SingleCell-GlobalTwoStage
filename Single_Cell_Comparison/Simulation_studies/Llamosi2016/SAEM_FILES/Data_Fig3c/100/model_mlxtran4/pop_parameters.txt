******************************************************************
*      model_mlxtran4.mlxtran
*      March 07, 2018 at 08:42:32
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

             parameter     s.e. (lin)   r.s.e.(%) 
km_pop     :     2.45         0.038          2    
kp_pop     :  -0.0545           -          -      
gp_pop     :    -5.28         0.033          1    
gm_pop     :    -3.53         0.049          1    
ltau_pop   :      3.4           -          -      

omega_km   :      0.7         0.027          4    
omega_kp   :        0           -          -      
omega_gp   :    0.586         0.025          4    
omega_gm   :    0.886         0.036          4    
omega_ltau :        0           -          -      
corr_km_gp :   0.0879         0.058         66    
corr_km_gm :    0.776         0.023          3    
corr_gp_gm :   -0.517         0.042          8    

a          :     66.1          0.67          1    
b          :    0.121       0.00062          1    

correlation matrix (IIV)
km      1       
gp   0.09       1    
gm   0.78   -0.52       1 

______________________________________________
correlation matrix of the estimates(linearization)

km_pop      1       
gp_pop   0.08       1    
gm_pop   0.75   -0.55       1 

Eigenvalues (min, max, max/min): 0.027  1.9  70

omega_km      1             
omega_gp   0.01       1          
omega_gm   0.56     0.3       1       
a            -0      -0      -0       1    
b            -0      -0      -0   -0.33       1 

Eigenvalues (min, max, max/min): 0.37  1.6  4.5

corr_km_gp      1       
corr_km_gm  -0.53       1    
corr_gp_gm   0.77    0.12       1 

Eigenvalues (min, max, max/min): 0.0038  1.9  4.9e+02


Population parameters and Fisher Information Matrix estimation...

Elapsed time is 5.36e+03 seconds. 
CPU time is 2.39e+04 seconds. 
