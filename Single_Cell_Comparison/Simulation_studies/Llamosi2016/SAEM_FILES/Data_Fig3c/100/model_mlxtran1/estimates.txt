          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.36226;   0.03675;      1.56;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.30817;   0.03286;      0.62;        NaN
    gm_pop;  -3.60398;   0.04779;      1.33;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.67524;   0.02643;      3.91;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.57535;   0.02446;      4.25;        NaN
  omega_gm;   0.85589;   0.03521;      4.11;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;   0.05641;   0.05802;    102.85;        NaN
corr_km_gm;   0.78501;   0.02252;      2.87;        NaN
corr_gp_gm;  -0.53117;   0.04164;      7.84;        NaN
         a;  65.84258;   0.66033;      1.00;        NaN
         b;   0.11418;   0.00059;      0.52;        NaN
