Simulations: GTS on Llamosi model simulations. .png images of the prediction quantiles are in the results folder


SAEM_FILES: SAEM files for estimation from simulated data. .png images of the prediction quantiles are in the results folder


Naive_STS: NTS method