#pragma once
#include <string>
#include <Eigen/Dense>
#include <odeSD/DefaultSensitivityModel>

class Llamosi2016_o2Model :
	public odeSD::DefaultSensitivityModel
{
public:
	odeSD::SensitivityState initialState;

	virtual void observeIntermediateSensitivityState(Eigen::DenseIndex stepIndex, const odeSD::SensitivityState& state)
	{
		return;
	}

	virtual void observeIntermediateState(Eigen::DenseIndex stepIndex, const odeSD::State& state)
	{
		return;
	}

	Llamosi2016_o2Model() :
		initialState(12, 4)
	{
		initialState.x << 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.;
		
		initialState.s.setZero();

		parameters = Eigen::VectorXd(initialState.nParameters);
		parameters << 4., 0.008, 0.008, 3.e+01;

		tspan = Eigen::VectorXd(200);
		for (Eigen::DenseIndex i = 0; i < tspan.rows(); i++)
		{
			tspan(i) = i * ##tEnd## / (tspan.rows() - 1.0);
		}

		states = Eigen::MatrixXd(initialState.nStates, tspan.rows());
		sensitivities = Eigen::MatrixXd(initialState.nStates * initialState.nParameters, tspan.rows());
	}

	odeSD::SensitivityState& getInitialState()
	{
		return initialState;
	}
    
	// time, current state, f, f'
	virtual void calculateRHS(odeSD::State& state)
	{
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& dfdt = state.fPrime;
		Eigen::Map<Eigen::VectorXd>& x = state.x;
        
		/***********************************************
		* 1st DERIVATIVE EQUATIONS (f)
		**********************************************/

		f[0] = p[1]*x[0]*-1.0+p[0]*x[2];
		f[1] = x[0]*9.47E-1-p[2]*x[1]*1.0;
		f[2] = x[2]*-9.225E-1+((tanh(p[3]*1.0E2-time*1.0E2+3.0E2)*5.0E-1+5.0E-1)*(p[3]-time*1.0+2.0)+(tanh(p[3]*1.0E2-time*1.0E2+3.0E2)*5.0E-1-5.0E-1)*(tanh(p[3]*1.0E2-time*1.0E2+1.0E3)*5.0E-1-(tanh(p[3]*1.0E2-time*1.0E2+1.4E3)*5.0E-1+5.0E-1)*(tanh(p[3]*1.0E2-time*1.0E2+1.0E3)*5.0E-1-5.0E-1)*(p[3]*2.5E-1-time*2.5E-1+3.5)*1.0+5.0E-1))*(tanh(p[3]*1.0E2-time*1.0E2+2.0E2)*5.0E-1-5.0E-1)*3.968E-1;
		f[3] = x[2]-p[1]*x[3]*1.0+p[0]*x[5];
		f[4] = x[3]*9.47E-1-p[2]*x[4]*1.0;
		f[5] = x[5]*-9.225E-1;
		f[6] = x[0]*-1.0-p[1]*x[6]*1.0+p[0]*x[8];
		f[7] = x[6]*9.47E-1-p[2]*x[7]*1.0;
		f[8] = x[8]*-9.225E-1;
		f[9] = p[1]*x[9]*-1.0+p[0]*x[11];
		f[10] = x[1]*-1.0+x[9]*9.47E-1-p[2]*x[10]*1.0;
		f[11] = x[11]*-9.225E-1;


		/***********************************************
		* 2nd DERIVATIVE EQUATIONS (Jfx*f = g)
		**********************************************/

		dfdt[0] = f[0]*p[1]*-1.0+f[2]*p[0];
		dfdt[1] = f[0]*9.47E-1-f[1]*p[2]*1.0;
		dfdt[2] = f[2]*-9.225E-1;
		dfdt[3] = f[2]-f[3]*p[1]*1.0+f[5]*p[0];
		dfdt[4] = f[3]*9.47E-1-f[4]*p[2]*1.0;
		dfdt[5] = f[5]*-9.225E-1;
		dfdt[6] = f[0]*-1.0-f[6]*p[1]*1.0+f[8]*p[0];
		dfdt[7] = f[6]*9.47E-1-f[7]*p[2]*1.0;
		dfdt[8] = f[8]*-9.225E-1;
		dfdt[9] = f[9]*p[1]*-1.0+p[0]*f[11];
		dfdt[10] = f[1]*-1.0+f[9]*9.47E-1-f[10]*p[2]*1.0;
		dfdt[11] = f[11]*-9.225E-1;

	}

	virtual void calculateJac(odeSD::JacobianState& jacState, odeSD::State& state)
	{
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& x = state.x;

		jacState.dfdx.setZero();
		Eigen::Map<Eigen::VectorXd>& dfdxVector = jacState.dfdxVector;

		dfdxVector[0] = p[1]*-1.0;
		dfdxVector[1] = 9.47E-1;
		dfdxVector[6] = -1.0;
		dfdxVector[13] = p[2]*-1.0;
		dfdxVector[22] = -1.0;
		dfdxVector[24] = p[0];
		dfdxVector[26] = -9.225E-1;
		dfdxVector[27] = 1.0;
		dfdxVector[39] = p[1]*-1.0;
		dfdxVector[40] = 9.47E-1;
		dfdxVector[52] = p[2]*-1.0;
		dfdxVector[63] = p[0];
		dfdxVector[65] = -9.225E-1;
		dfdxVector[78] = p[1]*-1.0;
		dfdxVector[79] = 9.47E-1;
		dfdxVector[91] = p[2]*-1.0;
		dfdxVector[102] = p[0];
		dfdxVector[104] = -9.225E-1;
		dfdxVector[117] = p[1]*-1.0;
		dfdxVector[118] = 9.47E-1;
		dfdxVector[130] = p[2]*-1.0;
		dfdxVector[141] = p[0];
		dfdxVector[143] = -9.225E-1;


		jacState.dfPrimedx = jacState.dfdx * jacState.dfdx;
		Eigen::Map<Eigen::VectorXd>& dfPrimedxVector = jacState.dfPrimedxVector;
        

	}

	virtual void calculateJacAndSensitivities(odeSD::JacobianState& jacState, odeSD::SensitivityState& state)
	{
		calculateJac(jacState, state);
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& x = state.x;

		state.dfdp.setZero();
		Eigen::Map<Eigen::VectorXd>& dfdpVector = state.dfdpVector;

		dfdpVector[0] = x[2];
		dfdpVector[3] = x[5];
		dfdpVector[6] = x[8];
		dfdpVector[9] = x[11];
		dfdpVector[12] = x[0]*-1.0;
		dfdpVector[15] = x[3]*-1.0;
		dfdpVector[18] = x[6]*-1.0;
		dfdpVector[21] = x[9]*-1.0;
		dfdpVector[25] = x[1]*-1.0;
		dfdpVector[28] = x[4]*-1.0;
		dfdpVector[31] = x[7]*-1.0;
		dfdpVector[34] = x[10]*-1.0;


		state.dfPrimedp = jacState.dfdx * state.dfdp;
		Eigen::Map<Eigen::VectorXd>& dfPrimedpVector = state.dfPrimedpVector;

		dfPrimedpVector[0] += f[2];
		dfPrimedpVector[3] += f[5];
		dfPrimedpVector[6] += f[8];
		dfPrimedpVector[9] += f[11];
		dfPrimedpVector[12] += f[0]*-1.0;
		dfPrimedpVector[15] += f[3]*-1.0;
		dfPrimedpVector[18] += f[6]*-1.0;
		dfPrimedpVector[21] += f[9]*-1.0;
		dfPrimedpVector[25] += f[1]*-1.0;
		dfPrimedpVector[28] += f[4]*-1.0;
		dfPrimedpVector[31] += f[7]*-1.0;
		dfPrimedpVector[34] += f[10]*-1.0;

	}


};
