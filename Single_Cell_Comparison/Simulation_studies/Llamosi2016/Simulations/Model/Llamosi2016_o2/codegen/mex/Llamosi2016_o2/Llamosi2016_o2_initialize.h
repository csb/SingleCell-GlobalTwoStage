/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Llamosi2016_o2_initialize.h
 *
 * Code generation for function 'Llamosi2016_o2_initialize'
 *
 */

#ifndef LLAMOSI2016_O2_INITIALIZE_H
#define LLAMOSI2016_O2_INITIALIZE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "Llamosi2016_o2_types.h"

/* Function Declarations */
extern void Llamosi2016_o2_initialize(void);

#endif

/* End of code generation (Llamosi2016_o2_initialize.h) */
