/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_jac_Llamosi2016_o2_mex.c
 *
 * Code generation for function '_coder_jac_Llamosi2016_o2_mex'
 *
 */

/* Include files */
#include "jac_Llamosi2016_o2.h"
#include "_coder_jac_Llamosi2016_o2_mex.h"
#include "jac_Llamosi2016_o2_terminate.h"
#include "_coder_jac_Llamosi2016_o2_api.h"
#include "jac_Llamosi2016_o2_initialize.h"
#include "jac_Llamosi2016_o2_data.h"

/* Function Declarations */
static void jac_Llamosi2016_o2_mexFunction(int32_T nlhs, mxArray *plhs[4],
  int32_T nrhs, const mxArray *prhs[4]);

/* Function Definitions */
static void jac_Llamosi2016_o2_mexFunction(int32_T nlhs, mxArray *plhs[4],
  int32_T nrhs, const mxArray *prhs[4])
{
  int32_T n;
  const mxArray *inputs[4];
  const mxArray *outputs[4];
  int32_T b_nlhs;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 4) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 4, 4,
                        18, "jac_Llamosi2016_o2");
  }

  if (nlhs > 4) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 18,
                        "jac_Llamosi2016_o2");
  }

  /* Temporary copy for mex inputs. */
  for (n = 0; n < nrhs; n++) {
    inputs[n] = prhs[n];
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(&st);
    }
  }

  /* Call the function. */
  jac_Llamosi2016_o2_api(inputs, outputs);

  /* Copy over outputs to the caller. */
  if (nlhs < 1) {
    b_nlhs = 1;
  } else {
    b_nlhs = nlhs;
  }

  emlrtReturnArrays(b_nlhs, plhs, outputs);

  /* Module termination. */
  jac_Llamosi2016_o2_terminate();
}

void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const mxArray
                 *prhs[])
{
  mexAtExit(jac_Llamosi2016_o2_atexit);

  /* Initialize the memory manager. */
  /* Module initialization. */
  jac_Llamosi2016_o2_initialize();

  /* Dispatch the entry-point. */
  jac_Llamosi2016_o2_mexFunction(nlhs, plhs, nrhs, prhs);
}

emlrtCTX mexFunctionCreateRootTLS(void)
{
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  return emlrtRootTLSGlobal;
}

/* End of code generation (_coder_jac_Llamosi2016_o2_mex.c) */
