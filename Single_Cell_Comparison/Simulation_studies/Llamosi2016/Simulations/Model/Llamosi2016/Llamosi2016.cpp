#pragma once
#include <string>
#include <Eigen/Dense>
#include <odeSD/DefaultSensitivityModel>

class Llamosi2016Model :
	public odeSD::DefaultSensitivityModel
{
public:
	odeSD::SensitivityState initialState;

	virtual void observeIntermediateSensitivityState(Eigen::DenseIndex stepIndex, const odeSD::SensitivityState& state)
	{
		return;
	}

	virtual void observeIntermediateState(Eigen::DenseIndex stepIndex, const odeSD::State& state)
	{
		return;
	}

	Llamosi2016Model() :
		initialState(3, 4)
	{
		initialState.x << 0., 0., 0.;
		
		initialState.s.setZero();

		parameters = Eigen::VectorXd(initialState.nParameters);
		parameters << 4., 0.008, 0.008, 3.e+01;

		tspan = Eigen::VectorXd(200);
		for (Eigen::DenseIndex i = 0; i < tspan.rows(); i++)
		{
			tspan(i) = i * ##tEnd## / (tspan.rows() - 1.0);
		}

		states = Eigen::MatrixXd(initialState.nStates, tspan.rows());
		sensitivities = Eigen::MatrixXd(initialState.nStates * initialState.nParameters, tspan.rows());
	}

	odeSD::SensitivityState& getInitialState()
	{
		return initialState;
	}
    
	// time, current state, f, f'
	virtual void calculateRHS(odeSD::State& state)
	{
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& dfdt = state.fPrime;
		Eigen::Map<Eigen::VectorXd>& x = state.x;
        
		/***********************************************
		* 1st DERIVATIVE EQUATIONS (f)
		**********************************************/

		f[0] = p[1]*x[0]*-1.0+p[0]*x[2];
		f[1] = x[0]*9.47E-1-p[2]*x[1]*1.0;
		f[2] = x[2]*-9.225E-1+((tanh(p[3]*1.0E2-time*1.0E2+3.0E2)*5.0E-1+5.0E-1)*(p[3]-time*1.0+2.0)+(tanh(p[3]*1.0E2-time*1.0E2+3.0E2)*5.0E-1-5.0E-1)*(tanh(p[3]*1.0E2-time*1.0E2+1.0E3)*5.0E-1-(tanh(p[3]*1.0E2-time*1.0E2+1.4E3)*5.0E-1+5.0E-1)*(tanh(p[3]*1.0E2-time*1.0E2+1.0E3)*5.0E-1-5.0E-1)*(p[3]*2.5E-1-time*2.5E-1+3.5)*1.0+5.0E-1))*(tanh(p[3]*1.0E2-time*1.0E2+2.0E2)*5.0E-1-5.0E-1)*3.968E-1;


		/***********************************************
		* 2nd DERIVATIVE EQUATIONS (Jfx*f = g)
		**********************************************/

		dfdt[0] = f[0]*p[1]*-1.0+f[2]*p[0];
		dfdt[1] = f[0]*9.47E-1-f[1]*p[2]*1.0;
		dfdt[2] = f[2]*-9.225E-1;

	}

	virtual void calculateJac(odeSD::JacobianState& jacState, odeSD::State& state)
	{
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& x = state.x;

		jacState.dfdx.setZero();
		Eigen::Map<Eigen::VectorXd>& dfdxVector = jacState.dfdxVector;

		dfdxVector[0] = p[1]*-1.0;
		dfdxVector[1] = 9.47E-1;
		dfdxVector[4] = p[2]*-1.0;
		dfdxVector[6] = p[0];
		dfdxVector[8] = -9.225E-1;


		jacState.dfPrimedx = jacState.dfdx * jacState.dfdx;
		Eigen::Map<Eigen::VectorXd>& dfPrimedxVector = jacState.dfPrimedxVector;
        

	}

	virtual void calculateJacAndSensitivities(odeSD::JacobianState& jacState, odeSD::SensitivityState& state)
	{
		calculateJac(jacState, state);
		Eigen::VectorXd& p = parameters;
		Eigen::Map<Eigen::VectorXd>& f = state.f;
		Eigen::Map<Eigen::VectorXd>& x = state.x;

		state.dfdp.setZero();
		Eigen::Map<Eigen::VectorXd>& dfdpVector = state.dfdpVector;

		dfdpVector[0] = x[2];
		dfdpVector[3] = x[0]*-1.0;
		dfdpVector[7] = x[1]*-1.0;


		state.dfPrimedp = jacState.dfdx * state.dfdp;
		Eigen::Map<Eigen::VectorXd>& dfPrimedpVector = state.dfPrimedpVector;

		dfPrimedpVector[0] += f[2];
		dfPrimedpVector[3] += f[0]*-1.0;
		dfPrimedpVector[7] += f[1]*-1.0;

	}


};
