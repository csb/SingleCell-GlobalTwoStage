#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N make_plot
##$ -pe openmpi624 8
#$ -cwd

sh bash_plot_submit.sh $1 $2 $3 $SGE_TASK_ID


