for i in 50 150 250 350 500
do 
qsub  -t 1-5 submit_SecondStage.sh Data_Fig3a $i AR
done

for i in 50 150 250 350 500
do
qsub  -t 1-5 submit_SecondStage.sh Data_Fig3a $i PL
done

for i in 2 4 8 16 32
do
qsub  -t 1-5 submit_SecondStage.sh Data_sigmas $i AR
done

for i in 2 4 8 16 32
do
qsub  -t 1-5 submit_SecondStage.sh Data_sigmas $i PL
done

for i in 20 40 60 80 100
do
qsub  -t 1-5 submit_SecondStage.sh Data_scrambleds $i AR
done

for i in 20 40 60 80 100
do
qsub  -t 1-5 submit_SecondStage.sh Data_scrambleds $i PL
done

for i in 25 50 75 100
do
qsub  -t 1-5 submit_SecondStage.sh Data_scrambled_bios $i AR
done

for i in 25 50 75 100
do
qsub  -t 1-5 submit_SecondStage.sh Data_scrambled_bios $i PL
done



