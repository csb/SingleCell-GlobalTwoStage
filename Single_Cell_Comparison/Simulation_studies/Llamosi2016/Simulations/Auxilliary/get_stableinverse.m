function [InvOut,err]=get_stableinverse(E)
%%%------------------------------------------------------------------------
%
%   function get_stableinverse.m
%	Function that gets the inverse of a matrix. First Cholesky is tried. If that fails, 
% 	and the condition number is zero, then a small number is added to the diagonal of the 
%	matrix. Then, the 'inv', based on LU decomposition is used.  
% 

%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

[U,err]=chol(E); 		% Invert using Cholesky Factorization first
if(err==0)
InvOut=inv(U)*inv(U)';
else
    
disp('WARNING:Cholesky inverse failed, issues in matrix inversion likely: in get_stableinverse()');
% This is a serious thing. While sometimes you cannot do much about it, it
% is worthwhie to find its cause: 
    % in the models looked at so far they seem to stem from a) correlations dominant or b) insensitive parameters
    
% If matrix is working precision
if rcond(E)==0 
        E=E+eye(size(E,1))*10^-10;
end


% Invert using LU decomposition
InvOut=inv(E);

    
end
