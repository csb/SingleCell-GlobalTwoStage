function [f_t, dydp, dy2dpdq,us, H1, H2, residuals,dx2dpdq] = get_secondordgradient(t,para,ydata,noise_params)
ME=[]; % Store error
try
% Parameters passed 
k_m=para(1); %m production
g_m=para(2); %m degradation
g_p=para(3); %p degradation
theta_hat0=noise_params(1);
theta_hat1=noise_params(2);

k_p=exp(-0.0545);
tau=exp(3.4);

addon=[0 2 3 10 14];% defines the time where the integration input changes

% times when input is added
t_in=[30  60.0000   90.0000  120.0000  150.0000  180.0000  210.0000  241.9064  290.4352  372.1597  455.3190 510.8225  546.0478];
t0=0;
x0=[1.0e-10 1.0e-10 0]';
times=t0;

% intial values
xs=[x0', zeros(1,9)]';
ys=1e-10;
us=0;

xsensitivities_1=0;
xsensitivities_2=0;
xsensitivities_3=0;

sensitivities_1=0;
sensitivities_2=0;
sensitivities_3=0;
% input time
inp=30;

options = amioption('sensi',2,'sensi_meth','forward',...
'atol',1e-8,'rtol',1e-4,'ism',1,'tstart',0,'maxsteps',10000,'x0',xs);


% Seond order sensitivities: intialise empty array
Hessian=zeros(1,1,3,3);
Hessian2=Hessian;
for i=1:length(t_in)
for j=1:length(addon)

    simulation_time=linspace(t0, t_in(i)+addon(j)+tau);
    simulated=simulate_Lamosi2o2exp(simulation_time,[([k_m, g_m, g_p])],[k_p,tau, inp+tau],[],options);
    t0=t_in(i)+addon(j)+tau;
    inp=t_in(i);
    x0=simulated.x(end,:)';
    times=cat(1,times,simulated.t(2:end));
    xs=cat(2,xs,simulated.x(2:end,:)');
    ys=cat(1,ys,simulated.y(2:end,1));
    us=cat(1,us,simulated.y(2:end,2));
    
    % first order sens: for observation
    sensitivities_1=cat(1,sensitivities_1, simulated.sy(2:end,1,1).*k_m);
    sensitivities_2=cat(1,sensitivities_2,simulated.sy(2:end,1,2).*g_m);
    sensitivities_3=cat(1,sensitivities_3,simulated.sy(2:end,1,3).*g_p);

    % first order sens: for second state
    xsensitivities_1=cat(1,xsensitivities_1, simulated.sx(2:end,2,1).*k_m);
    xsensitivities_2=cat(1,xsensitivities_2,simulated.sx(2:end,2,2).*g_m);
    xsensitivities_3=cat(1,xsensitivities_3,simulated.sx(2:end,2,3).*g_p);
    
    % Second order    
    Hessian=cat(1,Hessian(:,:,:,:),simulated.s2y(2:end,1,:,:)) ;  
    Hessian2=cat(1,Hessian2(:,:,:,:),simulated.s2x(2:end,2,:,:)) ;  

    % initialize sensitivity also for next iteration
    init_sx=squeeze(simulated.sx(end,:,:));

    options = amioption('sensi',2,'sensi_meth','forward',...
    'atol',1e-8,'rtol',1e-4,'ism',2,'tstart',t0,'maxsteps',10000,'x0',x0,'sx0',init_sx);
end
end
simulation_time=linspace(546.0478+14+tau, 650);
simulated=simulate_Lamosi2o2exp(simulation_time,([k_m, g_m, g_p]),[k_p,tau, inp+tau],[],options);

    times=cat(1,times,simulated.t(2:end));
    xs=cat(2,xs,simulated.x(2:end,:)');
    ys=cat(1,ys,simulated.y(2:end,1));
    us=cat(1,us,simulated.y(2:end,2));

    % Observation sensitivities
    sensitivities_1=cat(1,sensitivities_1,simulated.sy(2:end,1,1).*k_m);
    sensitivities_2=cat(1,sensitivities_2,simulated.sy(2:end,1,2).*g_m);
    sensitivities_3=cat(1,sensitivities_3,simulated.sy(2:end,1,3).*g_p);

    % first order sens: for second state
    xsensitivities_1=cat(1,xsensitivities_1, simulated.sx(2:end,2,1).*k_m);
    xsensitivities_2=cat(1,xsensitivities_2,simulated.sx(2:end,2,2).*g_m);
    xsensitivities_3=cat(1,xsensitivities_3,simulated.sx(2:end,2,3).*g_p);
    
    % Second order       
    Hessian=cat(1,Hessian,simulated.s2y(2:end,1,:,:))  ; 
    Hessian2=cat(1,Hessian2(:,:,:,:),simulated.s2x(2:end,2,:,:)) ;
    
    f_t=interp1(times,ys(:,1),t);
    dydp=[sensitivities_1 sensitivities_2 sensitivities_3];
    dxdp=[xsensitivities_1 xsensitivities_2 xsensitivities_3];

    dydp=interp1(times,dydp,t);
    dxdp=interp1(times,dxdp,t);
    dy2dpdq=interp1(times,Hessian,t);
    dx2dpdq=interp1(times,Hessian2,t);
    us=interp1(times,us,t);

     % noise model
     sigma_y=theta_hat0+f_t*theta_hat1;

     % Check NaNs in the data and exclude them.
     ids=1:length(ydata);
     nonans=~isnan(ydata);
     dydp=dydp(ids(nonans),[1,2,3]);
     dy2dpdq=dy2dpdq(ids(nonans),:,:,:);
     sigma_y=sigma_y(ids(nonans),:);
     
     % Hessian using the first order sensitivities: 
     H1= dydp'*get_stableinverse(diag(sigma_y.^2))*dydp;
     
    % Hessian using the second order sensitivities: 
    residuals=ydata(ids(nonans))-f_t(ids(nonans));
    H2=zeros(3,3);
    for n=1:3
        for m=1:3
            for t=1:length(sigma_y)
       H2(m,n)=H2(m,n)-residuals(t).*(1./sigma_y(t).^2).*dy2dpdq(t,:,m,n).*para(m).*para(n);
            end
         dy2dpdq(:,:,m,n)= dy2dpdq(:,:,m,n).*para(m).*para(n) ;
        end
    end
    H2=H2+dydp'*get_stableinverse(diag(sigma_y.^2))*dydp;


catch ME
disp(ME)
end


if isempty(ME)==false
    f_t=zeros(size(t))+Inf;
    f_t=f_t';
    dydp=[];
    dy2dpdq=[];
    us=[];
    H1=Inf;
    H2=Inf;
    dxdp=[];
end
end

