function [gradients,noisefuns,C_inv]=getgradient_odesd(t,p0,ehat,d,sigma2,int_opts)
%%%------------------------------------------------------------------------
%
%   function getgradient_odesd.m
% 	Gets sensitivity of the ODE model and calculates the gradients of the response
% 	with respect to the kinetic parameters. Note we are not caring about the parameters
% 	that are assumed fixed. Then the sensitivity is used to get the FIM. 
%  	y=exp(-gp*tau)*f_t_tau, then based on uv rule
% 	dy_dp=exp(-gp*tau)*df_dp+d{exp(gp*tau)}*f

%   Tools needed: 
        % a) ODE SOLVER

%  INPUT==> t: Time values in experiment,
%		  	p0: kinetic parameters
% 		  ehat: Noise parameters, theta in the model
%			d: data
% 		sigma2: sigma^2
%		int_opts: Options passed to integrator, structure.

%  OUTPUT==> gradients: the gradients wrt parameters
%			 noisefuns:	the 'g' in manuscript
%			 C_inv:		the FIM

%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------


% Parameters passed 

k_m=p0(1); %mRNA production
g_m=p0(2); %mRNA degradation
g_p=p0(3); %protein degradation
tau=exp(3.4);


f_name='Llamosi2016';

% Same options as used for calculating the ODE trajectories
options=odeset('AbsTol',int_opts.abstol,'RelTol',int_opts.rtol);
addon=[0 2 3 10 14];
t_in=[30  60.0000   90.0000  120.0000  150.0000  180.0000  210.0000  241.9064  290.4352  372.1597  455.3190 510.8225  546.0478];
t0=0;
x0=[1.0e-10 1.0e-10 0]';
times=t0;
xs=[0,0,0,0];
ss=x0';
inp=30;
for i=1:length(t_in)
for j=1:length(addon)

[T1 ,X1,S1]=odeSD_wrapper_hybrid(f_name,linspace(t0, t_in(i)+addon(j)+tau),x0,options,[k_m,g_m,g_p,inp+tau]');
t0=t_in(i)+addon(j)+tau;
inp=t_in(i);
x0=X1(end,:)';
times=cat(2,times,T1(2:end));
ss=cat(1,ss,X1(2:end,:));
sens=squeeze( S1 (2 ,: ,:))';
xs=cat(1,xs,sens(2:end,:));


end
end
    [T1 ,X1,S1]=odeSD_wrapper_hybrid(f_name,linspace(546.0478+14+tau, 650),x0,options,[k_m,g_m,g_p,inp+tau]');
    times=cat(2,times,T1(2:end));
    ss=cat(1,ss,X1(2:end,:));
    sens=squeeze( S1 (2 ,: ,:))';
    xs=cat(1,xs,sens(2:end,:));

% Observation model la
sensitivities_1=interp1(times,xs(:,1),t)*k_m;
sensitivities_2=interp1(times,xs(:,2),t)*g_m;
sensitivities_4=interp1(times,xs(:,3),t)*g_p;
X1=interp1(times,ss(:,2),t);

gradients=[sensitivities_1*exp(-g_p*tau), sensitivities_2*exp(-g_p*tau),sensitivities_4*exp(-g_p*tau)+X1.*(-tau)*exp(-g_p*tau)*g_p];
ids=1:length(d);
nonans=~isnan(d);
gradient=gradients(ids(~isnan(d)),[1,2,3]);


% Noise function
noisefuns=sqrt(sigma2)*ehat(1)+sqrt(sigma2)*ehat(2).*X1.*exp(-g_p*tau);
noisefun=noisefuns(ids(~isnan(d)),:);


% Asymptotic covariance matrix eqn: 2.33 and page 138 Ci=E_i
C_inv=(gradient'*(get_stableinverse(diag(noisefun).^2))*gradient);


end
