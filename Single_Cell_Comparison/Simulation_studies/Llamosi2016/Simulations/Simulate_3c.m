%% Simulation:
% Simulates the data for Figure 3c
% Tracking error introduced. 


clear all
cellnum=350;
%percent=[0.04,0.12,0.36,0.48,0.60,1];
%label={'4','12','36','48','60','100'};
percent=[0.2,0.4,0.6,0.8,1];
label={'20','40','60','80','100'};
seed=1;
for kk=1:length(percent)
    pp=percent(kk);
for J=1:5
load(sprintf('./Simulated_Data/Data_Fig3a/350/Simulated_%d.mat',J),'p','Data')%end
Data_orig=Data; 
t=unique(Data.TIME);
rng(seed);
to_scramble=randperm(cellnum,round(pp*cellnum));
scramble_pairs=reshape(to_scramble,[length(to_scramble)/2,2]);

All_cuts=[];
for i=1:size(scramble_pairs,1)
    % Sample a time point
    s_1=seed*10000;
    rng(s_1);
    time_cut=t(randperm(length(t)-12,1)+12);
    All_cuts=cat(2,All_cuts,time_cut);
    temp_1=Data.Y(Data.ID==scramble_pairs(i,1)&Data.TIME>time_cut);
    temp_2=Data.Y(Data.ID==scramble_pairs(i,2)&Data.TIME>time_cut);
    Data.Y(Data.ID==scramble_pairs(i,1)&Data.TIME>time_cut)=temp_2;
    Data.Y(Data.ID==scramble_pairs(i,2)&Data.TIME>time_cut)=temp_1;
    seed=seed+1;
end

%Data.Y(ismember(Data.TIME,t(tt)))=nan;
mkdir('./Simulated_Data/Data_Fig3c/');
mkdir(sprintf('./Simulated_Data/Data_Fig3c/%s',label{kk}));

save(sprintf('./Simulated_Data/Data_Fig3c/%s/Simulated_%d.mat',label{kk},J),'pp','scramble_pairs','Data_orig','Data','All_cuts')%end
writetable(Data,sprintf('./Simulated_Data/Data_Fig3c/%s/Simulated%d.txt',label{kk},J))
end
%% PLOTS
end

