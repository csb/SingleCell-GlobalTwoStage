function []=Plot_prediction(folder,subfolder,residual_type0,trial)
%%%------------------------------------------------------------------------
%
%   function Plot_prediction.m
%   Generates the predictions for the results from GTS. And saves it as an array, 
%   where each coloumn is the time and the rows are independent simulated trajectories. 

%   Tools needed: 
        % a) ODE SOLVER

%   Funtions, objective functions used: 
        % c) Model integration, and evaluating the error model  
%   
%   Lines that have to modified:
%       % a) the paths to ODE integrator, and model
        % b) Output directory
		% c) Path to GTS results
		% Number of simulations 
%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

rng('shuffle') 										% Set seed
addpath(genpath('~/Repos/odeSD/'))					% USER MUST MODIFY
addpath(genpath('./Model'))

% Output directory
    out_dir=sprintf('SecondStage_Results/%s/%s/plots',folder,subfolder);                         % path to store the results (USER MUST MODIFY)
    mkdir(out_dir)

   

% Input file name
filename=sprintf('SecondStage_Results/%s/%s/%s_Result_%d.mat',folder,subfolder,residual_type0,trial)		% USER MUST MODIFY

% Number of simulated trajectories					
nsims=10000; 										% USER MUST MODIFY									
load(sprintf('./%s',filename));
int_opts=struct();
int_opts.abstol=1e-6;
int_opts.reltol=1e-4;

Model_prediction=[];

%% PLot GTS predictions
Dhat=round(Dhat,4) % Incase the matrix is too stiff to sample from. 

for i=1:nsims
paras0=exp(mvnrnd(betahat,Dhat));
[f_t]=Run_Llamosi2016(0:600,[paras0(1:2),paras0(3)],int_opts);
% noise model
f_t=f_t+sqrt(sigma2).*(theta_hat(1)+exp(theta_hat(2)).*f_t).*randn(size(f_t));
Model_prediction=cat(1,Model_prediction,f_t);
end

%% Save results, 										USER MUST MODIFY
save(sprintf('%s/%s_plot_data_%d.mat',out_dir,residual_type0,trial),'Model_prediction','Dhat','betahat','betahati')
end

