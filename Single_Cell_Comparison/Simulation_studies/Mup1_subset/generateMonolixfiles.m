clear all
% Make the corresponding files needed by SAEM
% by matching cell IDS
% Lekshmi D
% Sept 2018

% Comment relevant parts based on the results loaded
% GTS results
df=readtable('../../Mup1_Endocytosis/Data/single_cell2017_all_timeshift.txt','Delimiter','\t','TreatAsEmpty','NA');
ids=unique(df.ID);
%df=struct2table(df);

%
mkdir('SAEM_Files')
for i=1:1:10
load(sprintf('Pub_Results_150/FirstStageNLMD_%d.mat',i))
idx=ismember(df.ID,cell_IDs);
temp=df(ismember(df.ID,cell_IDs),:);
writetable(temp,sprintf('./SAEM_Files/data_800_%d.txt',i),'Delimiter','\t')
end

