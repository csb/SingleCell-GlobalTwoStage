#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N Plot_preds
#$ -cwd

matlab -nosplash  -r "Plot_predictions($SGE_TASK_ID); exit"
