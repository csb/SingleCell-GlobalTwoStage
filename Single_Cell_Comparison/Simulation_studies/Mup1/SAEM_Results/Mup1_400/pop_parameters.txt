******************************************************************
*      Mup1_400.mlxtran
*      October 03, 2018 at 07:39:52
*      Monolix version: 4.4.0
******************************************************************

Estimation of the population parameters

                         parameter 
k2l_pop                :     -7.93 
k3l_pop                :     -2.16 
k4l_pop                :     -2.74 
k5l_pop                :     -2.11 
K_Ml_pop               :    -0.678 
alpha_Rl_pop           :   -0.0419 
T_Pl_pop               :      3.72 
T_El_pop               :      2.03 
alpha_El_pop           :     -3.92 
mu_Cl_pop              :     -6.09 
mu_Ml_pop              :     -6.31 
V_EMl_pop              :      1.85 
V_ECl_pop              :       4.7 
deltal_pop             :     0.782 
VC0l_pop               :      9.05 
VM0l_pop               :      8.34 
NE0l_pop               :       -20 
IC0n_pop               :      2.53 
IM0n_pop               :      1.12 
nE0l_pop               :       -20 

omega_k2l              :      1.33 
omega_k3l              :      1.02 
omega_k4l              :      1.08 
omega_k5l              :      0.77 
omega_K_Ml             :      0.38 
omega_alpha_Rl         :      0.34 
omega_T_Pl             :      0.85 
omega_T_El             :       0.6 
omega_alpha_El         :      0.94 
omega_mu_Cl            :      0.79 
omega_mu_Ml            :      0.74 
omega_V_EMl            :      0.48 
omega_V_ECl            :      0.65 
omega_deltal           :      0.52 
omega_VC0l             :      0.41 
omega_VM0l             :      0.29 
omega_NE0l             :         0 
omega_IC0n             :      0.95 
omega_IM0n             :      0.26 
omega_nE0l             :         0 
corr_k2l_k3l           :    -0.147 
corr_k2l_k4l           :    -0.143 
corr_k3l_k4l           :     0.921 
corr_k2l_k5l           :    0.0539 
corr_k3l_k5l           :     0.259 
corr_k4l_k5l           :     0.167 
corr_k2l_K_Ml          :     -0.32 
corr_k3l_K_Ml          :     0.239 
corr_k4l_K_Ml          :      0.31 
corr_k5l_K_Ml          :    -0.144 
corr_k2l_alpha_Rl      :    0.0168 
corr_k3l_alpha_Rl      :      0.27 
corr_k4l_alpha_Rl      :     0.228 
corr_k5l_alpha_Rl      :    -0.277 
corr_K_Ml_alpha_Rl     :     0.159 
corr_k2l_T_Pl          :    -0.137 
corr_k3l_T_Pl          :       0.5 
corr_k4l_T_Pl          :     0.437 
corr_k5l_T_Pl          :     0.208 
corr_K_Ml_T_Pl         :    0.0466 
corr_alpha_Rl_T_Pl     :     0.176 
corr_k2l_T_El          :     0.449 
corr_k3l_T_El          :    -0.321 
corr_k4l_T_El          :    -0.367 
corr_k5l_T_El          :     -0.22 
corr_K_Ml_T_El         :    -0.681 
corr_alpha_Rl_T_El     :     0.147 
corr_T_Pl_T_El         :    -0.177 
corr_k2l_alpha_El      :     0.164 
corr_k3l_alpha_El      :    0.0621 
corr_k4l_alpha_El      :    -0.022 
corr_k5l_alpha_El      :     0.408 
corr_K_Ml_alpha_El     :    -0.248 
corr_alpha_Rl_alpha_El :      0.12 
corr_T_Pl_alpha_El     :    0.0319 
corr_T_El_alpha_El     :    0.0732 
corr_k2l_mu_Cl         :    -0.174 
corr_k3l_mu_Cl         :   -0.0447 
corr_k4l_mu_Cl         :     -0.23 
corr_k5l_mu_Cl         :    0.0922 
corr_K_Ml_mu_Cl        :    -0.241 
corr_alpha_Rl_mu_Cl    :   -0.0206 
corr_T_Pl_mu_Cl        :     0.122 
corr_T_El_mu_Cl        :     0.176 
corr_alpha_El_mu_Cl    :    0.0472 
corr_k2l_mu_Ml         :    -0.147 
corr_k3l_mu_Ml         :   -0.0712 
corr_k4l_mu_Ml         :     -0.27 
corr_k5l_mu_Ml         :    0.0576 
corr_K_Ml_mu_Ml        :    -0.268 
corr_alpha_Rl_mu_Ml    :   -0.0324 
corr_T_Pl_mu_Ml        :    0.0452 
corr_T_El_mu_Ml        :     0.235 
corr_alpha_El_mu_Ml    :    0.0633 
corr_mu_Cl_mu_Ml       :     0.978 
corr_k2l_V_EMl         :    -0.134 
corr_k3l_V_EMl         :    0.0378 
corr_k4l_V_EMl         :     0.158 
corr_k5l_V_EMl         :      0.12 
corr_K_Ml_V_EMl        :     0.133 
corr_alpha_Rl_V_EMl    :     0.238 
corr_T_Pl_V_EMl        :    0.0209 
corr_T_El_V_EMl        :      -0.1 
corr_alpha_El_V_EMl    :    -0.154 
corr_mu_Cl_V_EMl       :    -0.265 
corr_mu_Ml_V_EMl       :    -0.335 
corr_k2l_V_ECl         :   -0.0944 
corr_k3l_V_ECl         :   -0.0356 
corr_k4l_V_ECl         :     0.122 
corr_k5l_V_ECl         :     -0.37 
corr_K_Ml_V_ECl        :     0.438 
corr_alpha_Rl_V_ECl    :     0.278 
corr_T_Pl_V_ECl        :   -0.0598 
corr_T_El_V_ECl        :    -0.385 
corr_alpha_El_V_ECl    :   -0.0763 
corr_mu_Cl_V_ECl       :    -0.455 
corr_mu_Ml_V_ECl       :    -0.506 
corr_V_EMl_V_ECl       :     0.394 
corr_k2l_deltal        :    -0.173 
corr_k3l_deltal        :    -0.212 
corr_k4l_deltal        :    -0.246 
corr_k5l_deltal        :    -0.162 
corr_K_Ml_deltal       :     -0.29 
corr_alpha_Rl_deltal   :     0.174 
corr_T_Pl_deltal       :    -0.103 
corr_T_El_deltal       :     0.729 
corr_alpha_El_deltal   :   -0.0549 
corr_mu_Cl_deltal      :     0.228 
corr_mu_Ml_deltal      :     0.259 
corr_V_EMl_deltal      :     0.103 
corr_V_ECl_deltal      :    -0.313 
corr_k2l_VC0l          :      0.16 
corr_k3l_VC0l          :   -0.0548 
corr_k4l_VC0l          :     0.205 
corr_k5l_VC0l          :    -0.271 
corr_K_Ml_VC0l         :     0.288 
corr_alpha_Rl_VC0l     :   -0.0289 
corr_T_Pl_VC0l         :    -0.134 
corr_T_El_VC0l         :    -0.108 
corr_alpha_El_VC0l     :    -0.175 
corr_mu_Cl_VC0l        :    -0.672 
corr_mu_Ml_VC0l        :    -0.682 
corr_V_EMl_VC0l        :     0.427 
corr_V_ECl_VC0l        :     0.508 
corr_deltal_VC0l       :    -0.132 
corr_k2l_VM0l          :     0.153 
corr_k3l_VM0l          :   -0.0514 
corr_k4l_VM0l          :     0.214 
corr_k5l_VM0l          :    -0.264 
corr_K_Ml_VM0l         :     0.295 
corr_alpha_Rl_VM0l     :   -0.0265 
corr_T_Pl_VM0l         :    -0.125 
corr_T_El_VM0l         :    -0.119 
corr_alpha_El_VM0l     :    -0.175 
corr_mu_Cl_VM0l        :    -0.663 
corr_mu_Ml_VM0l        :    -0.679 
corr_V_EMl_VM0l        :     0.435 
corr_V_ECl_VM0l        :      0.52 
corr_deltal_VM0l       :    -0.136 
corr_VC0l_VM0l         :     0.996 
corr_k2l_IC0n          :    -0.132 
corr_k3l_IC0n          :     0.155 
corr_k4l_IC0n          :     0.236 
corr_k5l_IC0n          :     0.514 
corr_K_Ml_IC0n         :     0.451 
corr_alpha_Rl_IC0n     :   -0.0933 
corr_T_Pl_IC0n         :    0.0929 
corr_T_El_IC0n         :    -0.537 
corr_alpha_El_IC0n     :    0.0776 
corr_mu_Cl_IC0n        :    -0.308 
corr_mu_Ml_IC0n        :    -0.403 
corr_V_EMl_IC0n        :     0.467 
corr_V_ECl_IC0n        :     0.421 
corr_deltal_IC0n       :    -0.213 
corr_VC0l_IC0n         :     0.257 
corr_VM0l_IC0n         :     0.276 
corr_k2l_IM0n          :   -0.0718 
corr_k3l_IM0n          :    0.0258 
corr_k4l_IM0n          :     0.249 
corr_k5l_IM0n          :   -0.0808 
corr_K_Ml_IM0n         :     0.317 
corr_alpha_Rl_IM0n     : -0.000524 
corr_T_Pl_IM0n         :   -0.0369 
corr_T_El_IM0n         :    -0.293 
corr_alpha_El_IM0n     :    -0.107 
corr_mu_Cl_IM0n        :    -0.231 
corr_mu_Ml_IM0n        :    -0.295 
corr_V_EMl_IM0n        :    0.0795 
corr_V_ECl_IM0n        :       0.3 
corr_deltal_IM0n       :    -0.211 
corr_VC0l_IM0n         :    0.0873 
corr_VM0l_IM0n         :     0.125 
corr_IC0n_IM0n         :      0.23 

a1                     :       721 
a2                     :       293 
a3                     :    0.0443 
b3                     :    0.0162 
a4                     :    0.0621 
b4                     :    0.0171 
a5                     :     0.177 
b5                     :     0.491 
a6                     :    0.0449 
b6                     :    0.0263 

correlation matrix (IIV)
k2l           1                                                    
k3l       -0.15       1                                                 
k4l       -0.14    0.92       1                                              
k5l        0.05    0.26    0.17       1                                           
K_Ml      -0.32    0.24    0.31   -0.14       1                                        
alpha_Rl   0.02    0.27    0.23   -0.28    0.16       1                                     
T_Pl      -0.14     0.5    0.44    0.21    0.05    0.18       1                                  
T_El       0.45   -0.32   -0.37   -0.22   -0.68    0.15   -0.18       1                               
alpha_El   0.16    0.06   -0.02    0.41   -0.25    0.12    0.03    0.07       1                            
mu_Cl     -0.17   -0.04   -0.23    0.09   -0.24   -0.02    0.12    0.18    0.05       1                         
mu_Ml     -0.15   -0.07   -0.27    0.06   -0.27   -0.03    0.05    0.24    0.06    0.98       1                      
V_EMl     -0.13    0.04    0.16    0.12    0.13    0.24    0.02    -0.1   -0.15   -0.27   -0.34       1                   
V_ECl     -0.09   -0.04    0.12   -0.37    0.44    0.28   -0.06   -0.39   -0.08   -0.45   -0.51    0.39       1                
deltal    -0.17   -0.21   -0.25   -0.16   -0.29    0.17    -0.1    0.73   -0.05    0.23    0.26     0.1   -0.31       1             
VC0l       0.16   -0.05    0.21   -0.27    0.29   -0.03   -0.13   -0.11   -0.18   -0.67   -0.68    0.43    0.51   -0.13       1          
VM0l       0.15   -0.05    0.21   -0.26     0.3   -0.03   -0.12   -0.12   -0.18   -0.66   -0.68    0.44    0.52   -0.14       1       1       
IC0n      -0.13    0.16    0.24    0.51    0.45   -0.09    0.09   -0.54    0.08   -0.31    -0.4    0.47    0.42   -0.21    0.26    0.28       1    
IM0n      -0.07    0.03    0.25   -0.08    0.32      -0   -0.04   -0.29   -0.11   -0.23   -0.29    0.08     0.3   -0.21    0.09    0.13    0.23       1 


Population parameters estimation...

Elapsed time is 7.33e+04 seconds. 
CPU time is 3.46e+05 seconds. 
