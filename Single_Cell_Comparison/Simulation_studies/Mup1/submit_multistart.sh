#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N Mup1
##$ -o /links/grid/scratch/dlekshmi/
##$ -e /links/grid/scratch/dlekshmi/
#$ -cwd
##$ -l h_rt=00:20:00

for i in $(seq $SGE_TASK_STEPSIZE);  do
 index=$(($SGE_TASK_ID+$i))
 echo "Submitted $index"
 matlab -nosplash  -r "MultiStart_WLS($(($SGE_TASK_ID+$i))-1); exit"
done

