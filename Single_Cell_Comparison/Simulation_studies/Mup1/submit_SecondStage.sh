#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N SecondStage
#$ -cwd

matlab -nosplash  -r "SecondStage($SGE_TASK_ID); exit"
