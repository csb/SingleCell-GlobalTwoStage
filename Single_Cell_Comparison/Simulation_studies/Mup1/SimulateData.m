% Simulate the data needed for the simulation stuy
% Uses the previous results on the full data
% According to GTS with 50 multi starts.@class  

% Needs manual modification of the PATHS> 
% Lekshmi D
% Sept 2018

clear all
% Modify paths: 
addpath(genpath('./'))

addpath(genpath('../../Mup1_Endocytosis/Model')) 
addpath(genpath('~/Repos/AMICI'))
addpath(genpath('../../Mup1_Endocytosis/Auxilliary')) 



%% Comment relevant parts based on the results loaded
% GTS results: USER Must change
load('../../Mup1_Endocytosis/Pub_Results/NLMD50_5.mat')
mkdir('Data')
dirname='Data';
err=[all_theta0s,all_theta1s];
n_cells=1400;  % modify

VC=[];
VM=[];
IC=[];
IM=[];
IS=[];
ICP=[];
IDS=[];
TIME=[];

%% Simulate parameters and model
gts_preds=struct;
for i=1:n_cells
rng(i)
time=linspace(-10,78,200);
%params_k=[randsample(1:length(data.Vol_cell),1),randsample(1:length(data.Vol_mem),1)];
params_sim=mvnrnd(betahat,Dhat);

int_opts.atol=1e-6;
int_opts.rtol=1e-3;
[f,t,simulated]=Endo_eq(params_sim',time,[],[],int_opts);
%[f,t,simulated]=Endo_eq(GLS_params(2).params',time,[],[],int_opts);
if ~isempty(simulated)
VC=cat(2,VC,simulated(:,1)');
VM=cat(2,VM,simulated(:,2)');
IC=cat(2,IC,simulated(:,3)');
IM=cat(2,IM,simulated(:,4)');
IS=cat(2,IS,simulated(:,5)');
ICP=cat(2,ICP,simulated(:,6)');
TIME=cat(2,TIME,time);
IDS=cat(2,IDS,repmat(i,1,length(time)));
plot(time,simulated(:,1))
hold on 
end
end

%%
mkdir(dirname)
all_eas=err(:,1)';
all_ebs=err(:,2)';
VC=VC+normrnd(0,1,size(VC)).*(all_eas(1));
VM=VM+normrnd(0,1,size(VM)).*(all_eas(2));
IC=IC+normrnd(0,1,size(IC)).*(all_eas(3)+IC.*all_ebs(3));
IM=IM+normrnd(0,1,size(IM)).*(all_eas(4)+IM.*all_ebs(4));
IS=IS+normrnd(0,1,size(IS)).*(all_eas(5)+IS.*all_ebs(5));
ICP=ICP+normrnd(0,1,size(ICP)).*(all_eas(6)+ICP.*all_ebs(6));
data=[TIME',IDS',VC',VM',IC',IM',IS',ICP'];
data=array2table(data,'VariableNames',{'Time','Track_index','VC','VM','IC','IM','IS','ICP'});
save(sprintf('./Data/data%d.mat',n_cells),'data','n_cells','err','betahat','Dhat')


