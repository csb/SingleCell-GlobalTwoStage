#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N FirstStage
#$ -pe openmpi624 30
##$ -o ./FirstStage.out
##$ -e ./FirstStage.err
#$ -cwd

matlab -nosplash  -r "FirstStageNLMD2($SGE_TASK_ID); exit"
