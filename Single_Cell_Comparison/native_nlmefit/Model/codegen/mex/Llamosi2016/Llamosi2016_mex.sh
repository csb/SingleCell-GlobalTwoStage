MATLAB="/net/bs-gridsw/sw-repo/bsse/MATLAB_R2016b"
Arch=glnxa64
ENTRYPOINT=mexFunction
MAPFILE=$ENTRYPOINT'.map'
PREFDIR="/home/dlekshmi/.matlab/R2016b"
OPTSFILE_NAME="./setEnv.sh"
. $OPTSFILE_NAME
COMPILER=$CC
. $OPTSFILE_NAME
echo "# Make settings for Llamosi2016" > Llamosi2016_mex.mki
echo "CC=$CC" >> Llamosi2016_mex.mki
echo "CFLAGS=$CFLAGS" >> Llamosi2016_mex.mki
echo "CLIBS=$CLIBS" >> Llamosi2016_mex.mki
echo "COPTIMFLAGS=$COPTIMFLAGS" >> Llamosi2016_mex.mki
echo "CDEBUGFLAGS=$CDEBUGFLAGS" >> Llamosi2016_mex.mki
echo "CXX=$CXX" >> Llamosi2016_mex.mki
echo "CXXFLAGS=$CXXFLAGS" >> Llamosi2016_mex.mki
echo "CXXLIBS=$CXXLIBS" >> Llamosi2016_mex.mki
echo "CXXOPTIMFLAGS=$CXXOPTIMFLAGS" >> Llamosi2016_mex.mki
echo "CXXDEBUGFLAGS=$CXXDEBUGFLAGS" >> Llamosi2016_mex.mki
echo "LDFLAGS=$LDFLAGS" >> Llamosi2016_mex.mki
echo "LDOPTIMFLAGS=$LDOPTIMFLAGS" >> Llamosi2016_mex.mki
echo "LDDEBUGFLAGS=$LDDEBUGFLAGS" >> Llamosi2016_mex.mki
echo "Arch=$Arch" >> Llamosi2016_mex.mki
echo "LD=$LD" >> Llamosi2016_mex.mki
echo OMPFLAGS= >> Llamosi2016_mex.mki
echo OMPLINKFLAGS= >> Llamosi2016_mex.mki
echo "EMC_COMPILER=gcc" >> Llamosi2016_mex.mki
echo "EMC_CONFIG=optim" >> Llamosi2016_mex.mki
"/net/bs-gridsw/sw-repo/bsse/MATLAB_R2016b/bin/glnxa64/gmake" -B -f Llamosi2016_mex.mk
