/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Llamosi2016_types.h
 *
 * Code generation for function 'Llamosi2016'
 *
 */

#ifndef LLAMOSI2016_TYPES_H
#define LLAMOSI2016_TYPES_H

/* Include files */
#include "rtwtypes.h"
#endif

/* End of code generation (Llamosi2016_types.h) */
