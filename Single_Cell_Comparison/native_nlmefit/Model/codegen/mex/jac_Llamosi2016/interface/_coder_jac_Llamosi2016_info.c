/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_jac_Llamosi2016_info.c
 *
 * Code generation for function '_coder_jac_Llamosi2016_info'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "jac_Llamosi2016.h"
#include "_coder_jac_Llamosi2016_info.h"

/* Function Definitions */
mxArray *emlrtMexFcnProperties(void)
{
  mxArray *xResult;
  mxArray *xEntryPoints;
  const char * fldNames[4] = { "Name", "NumberOfInputs", "NumberOfOutputs",
    "ConstantInputs" };

  mxArray *xInputs;
  const char * b_fldNames[4] = { "Version", "ResolvedFunctions", "EntryPoints",
    "CoverageInfo" };

  xEntryPoints = emlrtCreateStructMatrix(1, 1, 4, fldNames);
  xInputs = emlrtCreateLogicalMatrix(1, 3);
  emlrtSetField(xEntryPoints, 0, "Name", mxCreateString("jac_Llamosi2016"));
  emlrtSetField(xEntryPoints, 0, "NumberOfInputs", mxCreateDoubleScalar(3.0));
  emlrtSetField(xEntryPoints, 0, "NumberOfOutputs", mxCreateDoubleScalar(4.0));
  emlrtSetField(xEntryPoints, 0, "ConstantInputs", xInputs);
  xResult = emlrtCreateStructMatrix(1, 1, 4, b_fldNames);
  emlrtSetField(xResult, 0, "Version", mxCreateString("9.1.0.441655 (R2016b)"));
  emlrtSetField(xResult, 0, "ResolvedFunctions", (mxArray *)
                emlrtMexFcnResolvedFunctionsInfo());
  emlrtSetField(xResult, 0, "EntryPoints", xEntryPoints);
  return xResult;
}

const mxArray *emlrtMexFcnResolvedFunctionsInfo(void)
{
  const mxArray *nameCaptureInfo;
  const char * data[15] = {
    "789ced5b4d6fe3441876d15216a15d2d488016c1422f0869b575ba0d25b96dbe9aa6db7c35493f5281e58f7132f5d893789c8ff6d423120716ad107040ea2fe0"
    "0212e2cc850bb745e207f00ff8037812a7cd4ecdba1b2769761d4b56f2da79c7cffb781ecffbce38dc4226cbd9db4d7b3fdde2b845fbf3babdbfc2f5b7571d7b",
    "c1dedf713efbc7af71371cfb6b7b97b16181aed53f69883ae0069b8275688886553e6a00ce0404a336507a6754884019ea600b0f191bd036f4f5a15367063d45"
    "bf27ea40d64a2d9d33ebe4ec321c1a367af17ccf9dc773cd251e3014cf2dc73e487dced7b10e7805018dd475c86f2151c74468987c0c21410608113ed3d4f912",
    "346a080809fb8090c07a433421c186f37328dc0fadacf159ac00c41f8ab2e01ca6479775075fc403df75061fb57553816da880cbf82f32fe8bbdbbd19210e85f"
    "ff4b0fff0ae34fed83ccd69e4d91012c5e22f76a26544887279d7b266860fb08017c3656de8ac5856d1aaac45b1823097779a0231e4189d7450b89128f1b841f",
    "84b2dc70e163d105cfc2109ed79de31c77f2eb8ffffc151bddbfbff9f0ff217e7b3cd7dff0b81fc3fabbe5d89dd51df32897abc81a8ec5cc5aa9158f28bb897e"
    "7b9f0cb5b7e0d21e37f439caef67b9ffb8e1798dc1436da78d4bf9fbd55399f12f8f978f011dfae0fe8ca8a7c78bc52bd5d3d8fcbdf4f436733fa81de914d5ca",
    "5e78edb38d36da8e2623912d5c91b9d9d0d3b4fb4fc103cf870c1e6acbf688672e433b19300d112d43126f4164658c5c4b072694c7a233af715d66fc659f3cd1"
    "fd6e2f30feee20329e8dac4f9b9f7efbcbcd3f9f0442776ee35812c1153192acad87a5682ba2e40d7d2d5ed898ebce0dcf470c1e6a33ba23b28840b7d1cb4b2d",
    "682b671aba53187fc5274faebabb1099dff18e7b3caefcedaafd47d19dd4ddcf6daba2b653cc3ecc6d6e03ab142ae503aa3b2ffede62f0509bd19ddde4797c93"
    "ce277718ff1d9ffcb8eacd8e8832e4239ffc2a28f9a49bbeb2152bb49fd98d1946651b1648315723e5666a3afa9af5f90f2f3edf64f051dbeea3826e411d10a1",
    "0e50039843f1869ed1de601b6e6fe0f7c803c7178c1fb5c7f81cba10924fbd3df9f9b7e0eaad5a7ba8c6e0710a146ac954b4a8173743249d9e8ede4e3df02206"
    "2fb527db8f9664acebd810643a634a0638e775ddbcae0b425d37abcf752f1edf657051dba5be13cd546d2cbaf3e2e980f13ff0c9d3ffd6753422ff7abbf5773e",
    "b8e35f3e9d4e34c3f912412bbbbbcd354522b008a7b41e30abfde88107ae1b0c2e6a4362f49fff165db61c4f3de7c54f95f1affae4873e8f54d8054a03dbf4f0"
    "4f85e4d0e3a79f7e1be47993f4aa76546ad6204a9413da76335e2b6b955432d83a1b653d1b1215da6366fd455dcf7e4a5ffd505cd7f7e7babae8efb6fed66e82",
    "54a32d93e85e6715e30229adacee57b9d9982f994afd71aeab25159ac452e1193eafbaed0e838fda3409b5db16546c228c1b026e035345b8d32f09fdcd9b9c7a"
    "e0d1183fcd275f43f9b543df3342f39d479e7c17141dba8d6f38d16d16d56c35563771d24c6d59bb9d6c754a79e41f1e78bf61f0527b8afd6ae9d93f78be79c9",
    "05e7d8793cd77a63e298ea446828a09b31ac4b8d8f45a6bde2187865c6c741da391f1fb9d17459b85f0e1d178e8cbd6ef370f56873e3b05c92aa1bb3b15e37ed"
    "fef3c003cf65ebbae7d5a75c17cd793d77c1dfeff5c7e6ffa2d5732fdb78374a1d67b7af8b5d7f7abc8afa6fc023b2bff0fd20fcd67f81ce3b5177ad0a4287f9",
    "5005c643e5cef18a1846d35a2f98ebb0af4368bc043a84c65c87dce83afc3493d51a5a0e74ad8d6ca956483f2ca443283e9f87f153cf4d7a7e735ebfcdbeaeae"
    "b27e9bd5f570af79cddb0c2e6a33f31c1212c972b706f4e1fe39a9ba6edc3cb93e7feed290f85e483edf07fbe9e347c1d55b5d471135128e6b5ab40854b9dc5a",
    "dd03b1d4cba9b7cbf6232fbdbdc7e0a2b69bdea081a001cee39dd4fb9702e3274c8ea77e4836513efaeb9d7f4f82fbbe575c1263249e0b17da2852342c2bb2ab"
    "359b537afff245d5db25deb7ece9ad45806002957ef7a5b799f81f4f9fa7a1907cff8f27d0ba3363d97029a626f2728a90437127538f26f529add79d7ae03518",
    "bcd49eacee962440e745acba09481d23e5723afc80c1496d371d0e35eb2bcff4d2a1c4e09126c7db594863584748847e0f6ebe995576a2f95ca4955edf54724d"
    "9c2f5492c7ab01cf37bd789cbfef3c7fdf7954bdcdf2fbceb39a6fbecfe0a236a3b7414e7636a532c9714e64f08813e1c909e99caab3fb38e2bcca1b275a7075",
    "a78278278a1b2b7bcd62c38a94f47a673dd69982eefe033eca607c", "" };

  nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(data, 19632U, &nameCaptureInfo);
  return nameCaptureInfo;
}

/* End of code generation (_coder_jac_Llamosi2016_info.c) */
