/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * jac_Llamosi2016_terminate.h
 *
 * Code generation for function 'jac_Llamosi2016_terminate'
 *
 */

#ifndef JAC_LLAMOSI2016_TERMINATE_H
#define JAC_LLAMOSI2016_TERMINATE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "jac_Llamosi2016_types.h"

/* Function Declarations */
extern void jac_Llamosi2016_atexit(void);
extern void jac_Llamosi2016_terminate(void);

#endif

/* End of code generation (jac_Llamosi2016_terminate.h) */
