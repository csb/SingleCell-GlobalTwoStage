/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_Llamosi2016_info.c
 *
 * Code generation for function '_coder_Llamosi2016_info'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Llamosi2016.h"
#include "_coder_Llamosi2016_info.h"

/* Function Definitions */
mxArray *emlrtMexFcnProperties(void)
{
  mxArray *xResult;
  mxArray *xEntryPoints;
  const char * fldNames[4] = { "Name", "NumberOfInputs", "NumberOfOutputs",
    "ConstantInputs" };

  mxArray *xInputs;
  const char * b_fldNames[4] = { "Version", "ResolvedFunctions", "EntryPoints",
    "CoverageInfo" };

  xEntryPoints = emlrtCreateStructMatrix(1, 1, 4, fldNames);
  xInputs = emlrtCreateLogicalMatrix(1, 3);
  emlrtSetField(xEntryPoints, 0, "Name", mxCreateString("Llamosi2016"));
  emlrtSetField(xEntryPoints, 0, "NumberOfInputs", mxCreateDoubleScalar(3.0));
  emlrtSetField(xEntryPoints, 0, "NumberOfOutputs", mxCreateDoubleScalar(6.0));
  emlrtSetField(xEntryPoints, 0, "ConstantInputs", xInputs);
  xResult = emlrtCreateStructMatrix(1, 1, 4, b_fldNames);
  emlrtSetField(xResult, 0, "Version", mxCreateString("9.1.0.441655 (R2016b)"));
  emlrtSetField(xResult, 0, "ResolvedFunctions", (mxArray *)
                emlrtMexFcnResolvedFunctionsInfo());
  emlrtSetField(xResult, 0, "EntryPoints", xEntryPoints);
  return xResult;
}

const mxArray *emlrtMexFcnResolvedFunctionsInfo(void)
{
  const mxArray *nameCaptureInfo;
  const char * data[17] = {
    "789ced5ccd8fe3481577a3a1d915da65596959968f85b9ac40a36da72799dee4d6f9ea74a63b5f9da4bb27031b1cbb9c54bb5ce5b89c8feed31c9138b0688580"
    "03d2700321710109715d2e5cb82d686f5cb870e61f58573eba3335de714fecb83de358b2e2e7e4957fefb97e7eef5595236c144b82bdbd6eefc90f0561d3fe7c",
    "c5debf244cb72fcfe40d7bfff6ec737afe96f0da4cfeb9bdcb045b606c4dbfc4920e84f9a6101d62095b8d73030826a0040d8132f946850834a00e0ec982b00f"
    "6d41df5bf8ea52605fb1e36c0fc85a7da00b668f5e5e46408bc2c49ec7c2953db71cecd117ec7963263fccff58ec111d880a021aede9503c44924e68db30c534",
    "426d192044c5625f17eb1077116867ed13ed2cd10dc98494e0d9cf61fb6e6c7b472c1105a0f9a9c99985e32d7d8633e682f3d613386f0996847b93fbe5a2b7c9"
    "d9b739b91b830e02d3ebfed445bfcee933f961f1f0d476110696d8a1ef774da8d0914847ef9bc020f6190ac452ba7198ceb48f98891dd1220475c858043a1211",
    "ec88ba6421a92302a40eb0c80c997861d6ff16f06c3ae0d958c0f3eaecbcbdfd61f7bd7fa53de87bbdbe6ffafbc2b3efc7378427ef079377606118cf5592386b"
    "943e00d2c92013372ff6a6edfd60a1bd0d87f68485cf657e1fb6fe5375c1f32d0e0f93659ba1e616b41f5e2696d016952524995bace12b3b9fc5cff9b6d8ee5c",
    "ef23173c6d4eafedd13f6cbf333148bc33b748bc333569eeabe5fbe9a33ffeef93e8f2cc38c0f924386849f583f24e4a1b82bbe341311b4d9efdc405cf0f393c"
    "4ce6782619063aaf4f7ae6de00cb1624b888ab4892e77903bbceaecb75bec65d87c9eaacb5764fc276085f3e0ff09b875f6cf1651eb064bf6efd271e5d5e2670",
    "13df8389518c4a75d2198186969472b96078f9d8056f58fad5aae2a2d7fc731d1f5f1e1ede647c7cec82372c75deae0b4ea7787626c9ed85b67ce1ddef5df4fb"
    "9c7e3f007f7176ceb20d4ffdf947dffcddbf23cbc70b23d123194d4534d6d0f4bd52cee8caf702e2e38bdcbf9cf0bec2e165b26e2a70081510c4384c93d367b2",
    "4ff93d31a8383765cb70f0c773c4bfbffef6bf371aff7e9379c79feb2fc3b751fcd83c2f979bb246d269b35b1f6492ca4948eac39bec3f4e78bec2e161f2ac0d"
    "5fe29b9b3f1a9c7ec35f7fccdda1cfefcf927cfa78b3168d7cf22dee7e303939aaa9cdd3c4ce07fb437494ca259387a4290be1e053d0fdc7ad7efb1e8787c95c",
    "fd066966009155c4e5810e4c28fbc2b35fbbe8cb9cbeecd14f8ef51b6f99f7f984bfbcfecf4f23c13ba7389643705b4ae6ba7b894e6a90542a58dfc954f7d7bc"
    "73c2f37d0e0f931dc64dc0d898e4a916b4991304ef144e5ff1e82747de3d6599d778277cec57fe76d3facbf0ae337e503e5225edb8563a28df3f02563d56af44",
    "94776efe7b93c3c3648e77769357f6ad3a9f3ce6f48f3dfac7916fb645cc431ef2c99f45259f74e257a969c51e144fd218378f6095d6ca5ddae8e7d7e3214ee3"
    "216efefd3a8797c9769f6deb16d4016df7003280b9d0deaae6cf3fe4f498ece373e929933cf2efd33fff2dbafc6b750fd434bcc8836a37974fd5f4dafd182d14",
    "c2313f8038bc4c5e6d3fba2d135d27b82db3956c748e735de7adebbc28d479617daebbf9f16d0e17931de7c9f3dd40e6c71f72fa0f3dfae90beb3c669177bebd"
    "f15925baf1af522864fb894a9da2ed9393fe8ed2a1b006039a1f086b3fda75c1f51a878bc990e2e9f3df62cbc9835987d2e2f45b1efdc39e472a1c03c520b67b",
    "c4274c9a27e21efae92fa33c8e52886be7f57e17a26c23ab1df533dd86d6cc07b41e2cac3c5b667e1b5215da313390f70c56313ff904bfa6a6785d6719195e39"
    "cdc70dfb206f0c659a3a1dc509a9d2fa76fc414b088657a1a83fae78755b8526b5547889cfad6e7b97c3c7649684da6db7556222428c361902534564342d09bd",
    "8d9b3c76c1a3717a9a477f2dae179fbaef19a679ce231ffd2a2a3c748a6f243beed7d4522bdd3349cecc1f5a27a3522ba03cf21f2e787fc1e1657280fdeaf6b3"
    "7ff07ce3921bc2d3efddb140e2539d08b102c6456c5d2b3ed6b8f66a3ef8958b8f7eacb78c4c7c74e265f56e2376513dc7a7e3fe59fcfcfefe59a3de69ed8763",
    "fe2ee8feb3eb82e7ba75ddf3f253ee49e6ba9e7b4adfebf57dd37fd1eab9972dde2d53c7d9edebd2d81b1f6fa2febb7c7fd13e10a74678adff229d77a2f14e0b"
    "c4ce2ab126ccc41aa38b6d2981829a2f58f370ca43885f021e42bce6a1b03c0fef154b9aa195c1d8da2fd5bbd5c241b5104399f5388c977a6ed5e39bebfa2dfc",
    "bcbac9fa2dacf3e16ee39aef70b898cc8d73749044b7c65da02ff6cf55d5757efbc9f97d7166923831c9e37ab03fbdf75174f9d6d351524d26329a96aa01556e"
    "0ce2a7209d7f39f976dd7ee4c3ff344cf8063182185cd9bbaaf597c1fc3fc3c44f53933cfd3f83f0eeff1f4577bd57a623a569a69ca80e51b2862d2b79a2f5fb",
    "01adbf7c51f9768df59613be0d28689b4065c79ef8168af77aa67e5a30c9f37b3d91e69d992e25ea69355b91f3949e49c7c55e2aa787e47f51308797c9abe5dd"
    "ed0e60e32256cf04b44790723d1e7e97c3c964271e2e34eb29cf74e36187c3d3599ddf2e4df2611e211bfb7b74f3cd92729caa949383c2de7da5dc27956a3377",
    "118f78bee9e6c7f57ae7f57ae765f916e6f5ce61cd37bfc3e16232c7b7794e7639a4b2ca38277178a495f86966d295ab2eefe392e32a5f7da44597772ac88c52"
    "c4d83eedd70c2b59d77ba3bdf42800de7d0e9bea5893", "" };

  nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(data, 24136U, &nameCaptureInfo);
  return nameCaptureInfo;
}

/* End of code generation (_coder_Llamosi2016_info.c) */
