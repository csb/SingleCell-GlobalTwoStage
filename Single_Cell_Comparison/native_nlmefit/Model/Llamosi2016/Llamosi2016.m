function [f,g,Jfx,Jgx,Jfp,Jgp] = Llamosi2016( varargin )
    additionalArgs = {};

    if nargin<1
        f = [0;0;0];
        g = [4.47;0.008;0.008;30];
        return
    elseif nargin==2 
        time = varargin{1};
        x = varargin{2};
        p = [4.47;0.008;0.008;30];
    else
        time = varargin{1};
        x = varargin{2};
        p = varargin{3};
        if nargin > 3
            additionalArgs = varargin(4:end);
        end
    end 

    f = [p(1)*x(3) - 1.0*p(2)*x(1);
		0.947*x(1) - 1.0*p(3)*x(2);
		0.3968*((0.5*tanh(100.0*p(4) - 100.0*time + 300.0) + 0.5)*(p(4) - 1.0*time + 2.0) + (0.5*tanh(100.0*p(4) - 100.0*time + 300.0) - 0.5)*(0.5*tanh(100.0*p(4) - 100.0*time + 1000.0) - 1.0*(0.5*tanh(100.0*p(4) - 100.0*time + 1400.0) + 0.5)*(0.5*tanh(100.0*p(4) - 100.0*time + 1000.0) - 0.5)*(0.25*p(4) - 0.25*time + 3.5) + 0.5))*(0.5*tanh(100.0*p(4) - 100.0*time + 200.0) - 0.5) - 0.9225*x(3)];

    if nargout<2 
        return
    end

    g = [f(3)*p(1) - 1.0*f(1)*p(2);
		0.947*f(1) - 1.0*f(2)*p(3);
		-0.9225*f(3)];

    if nargout<3 
         return 
    elseif nargout==3 
         [Jfx] = jac_Llamosi2016(time, x, f, p, additionalArgs{:});
    elseif nargout==4 
         [Jfx,Jgx] = jac_Llamosi2016(time, x, f, p, additionalArgs{:});
    elseif nargout==5 
          [Jfx,Jgx,Jfp] = jac_Llamosi2016(time, x, f, p, additionalArgs{:});
    elseif nargout==6 
          [Jfx,Jgx,Jfp,Jgp] = jac_Llamosi2016(time, x, f, p, additionalArgs{:});
    elseif nargout>6 
         error('Too many output arguments');
    end
end
