function [model] = sym_Llamosi_model()

% 
%model.param = 'log';


% create state syms
syms m pr inp



%% PARAMETERS ( for these sensitivities will be computed )

% create parameter syms
syms k_m g_m g_p k_p tau u_c t_in 

x=[m pr inp];

% CONSTANTS
k=[k_p tau t_in];

% create parameter vector 
p = [k_m, g_m, g_p];

% create symbolic variable for t
syms t


%% SYSTEM EQUATIONS
xdot = sym(zeros(size(x)));

%% INITIAL CONDITIONS

x0 = sym(zeros(size(x)));
x0(1)=1e-10;
x0(2)=1e-10;
x0(3)=1e-10;

u_c=((0.5*tanh(100.0*t_in - 100.0*t + 300.0) + 0.5)*(t_in - 1.0*t + 2.0) + (0.5*tanh(100.0*t_in - 100.0*t + 300.0) - 0.5)*(0.5*tanh(100.0*t_in - 100.0*t + 1000.0) - 1.0*(0.5*tanh(100.0*t_in - 100.0*t + 1400.0) + 0.5)*(0.5*tanh(100.0*t_in - 100.0*t + 1000.0) - 0.5)*(0.25*t_in - 0.25*t + 3.5) + 0.5))*(0.5*tanh(100.0*t_in - 100.0*t + 200.0) - 0.5);

%% SYSTEM EQUATIONS
xdot(1)=k_m*inp - g_m*m ;   %mRNA
xdot(2)=k_p*m - g_p*pr;    %Protein
xdot(3)=0.3968*u_c - 0.9225*inp;  %Actual input


%% OBSERVABLES

y = sym(zeros(2,1));
y(1)= exp(-g_p*tau)*pr;% W
y(2)=u_c;

%% SYSTEM STRUCT
model.sym.x = x;
model.sym.xdot = xdot;
model.sym.p = p;
model.sym.x0 = x0;
model.sym.y = y;
model.sym.k=k;
%model.sym.sigma_y=theta0+y(1).*theta1;
