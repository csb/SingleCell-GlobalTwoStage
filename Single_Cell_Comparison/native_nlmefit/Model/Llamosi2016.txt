********** MODEL NAME
HOG  osmosis model txt format: Does not include the observation model

********** MODEL NOTES
Implementation of the ODE model in Llamos et al, 2016 (Plos Comp. Bio)

********** MODEL STATES
d/dt(m)=k_m*u - g_m*m    %mRNA
d/dt(p)=k_p*m - g_p*p    %Protein
d/dt(u)=0.3968*u_c - 0.9225*u  %Actual input                                                                    



u(0)=0
m(0)=0
p(0)=0



********** MODEL PARAMETERS

k_m=4.47
g_m=0.008
g_p=0.008
t_in=30

********** MODEL VARIABLES
k_p=0.947
tau=29.96
u_c=piecewiseIQM(0,le(time,t_in+2),piecewiseIQM((time-t_in-2),lt(time,t_in+3),piecewiseIQM(1,le(time,t_in+10),piecewiseIQM(0,gt(time,t_in+14),-0.25*(time-t_in-14)))))

********** MODEL REACTIONS


********** MODEL FUNCTIONS


********** MODEL EVENTS


********** MODEL MATLAB FUNCTIONS
