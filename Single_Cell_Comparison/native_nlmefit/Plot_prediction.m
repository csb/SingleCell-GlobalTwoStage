function []=Plot_prediction()
%%%------------------------------------------------------------------------
%
%   function Plot_prediction.m
%   Generates the predictions for the results from GTS. And saves it as an array, 
%   where each coloumn is the time and the rows are independent simulated trajectories. 
% 
%   Tools needed: 
%         a) ODE SOLVER
% 
%   Funtions, objective functions used: 
%         c) Model integration, and evaluating the error model  
%   
%   Lines that have to modified:
%       % a) the paths to ODE integrator, and model
%         b) Output directory
% 		c) Path to GTS results
% 		Number of simulations 
%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

rng('shuffle') 										% Set seed
addpath(genpath('~/Repos/odeSD'))					% USER MUST MODIFY
addpath(genpath('./Model'))

% Input file name
filename='nlmefit_Result.mat';                          % USER MUST MODIFY
% Number of simulated trajectories					
nsims=10000; 										    % USER MUST MODIFY									
load(sprintf('./Pubs_Results/%s',filename));
int_opts=struct();
int_opts.abstol=1e-6;
int_opts.reltol=1e-4;

theta_hat=[stats.errorparam];
Model_prediction=[];

%% PLot GTS predictions
Dhat=(Dhat+Dhat)./2; % Incase the matrix is too stiff to sample from.
tic
for i=1:nsims
paras0=(mvnrnd(betahat,Dhat));
[f_t]=Run_Llamosi2016wosens(0:600,exp(paras0),int_opts);
%[f_t]=RunLlamosi2016cvodes(0:600,paras0,int_opts);
% with noise model
f_t=f_t+(theta_hat(1)+(theta_hat(2)).*f_t).*randn(size(f_t));
Model_prediction=cat(1,Model_prediction,f_t);
end
toc
%% Save results, 										USER MUST MODIFY
mkdir('Pubs_Results/plots_data')	
dated=date();
save(sprintf('Pubs_Results/plots_data/%s_%s.mat',filename,dated),'Model_prediction','Dhat','betahat','betahati')
end
