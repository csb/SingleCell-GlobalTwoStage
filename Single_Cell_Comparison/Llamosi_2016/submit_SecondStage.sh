#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N SecondStage_LlamosiModel
#$ -o ./SecondStage-LM.out
#$ -e ./SecondStage-LM.err
#$ -cwd
matlab -nosplash  -r 'SecondStage; exit'
