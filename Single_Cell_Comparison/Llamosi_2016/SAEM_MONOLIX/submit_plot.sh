#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N plot_LlamosiModel
#$ -o ./plot-LM.out
#$ -e ./plot-LM.err
#$ -cwd
matlab -nosplash  -r 'plot_SAEMPreds; exit'
