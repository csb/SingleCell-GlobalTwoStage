          ; parameter;  s.e._lin;r.s.e._lin;pvalues_lin
    km_pop;   2.41052;   0.04166;      1.73;        NaN
    kp_pop;  -0.05450;   0.00000;      0.00;        NaN
    gp_pop;  -5.16907;   0.03618;      0.70;        NaN
    gm_pop;  -3.67769;   0.06096;      1.66;        NaN
  ltau_pop;   3.40000;   0.00000;      0.00;        NaN
  omega_km;   0.67113;   0.03194;      4.76;        NaN
  omega_kp;   0.00000;   0.00000;      0.00;        NaN
  omega_gp;   0.57395;   0.02803;      4.88;        NaN
  omega_gm;   1.00364;   0.04713;      4.70;        NaN
omega_ltau;   0.00000;   0.00000;      0.00;        NaN
corr_km_gp;  -0.32883;   0.06649;     20.22;        NaN
corr_km_gm;   0.83619;   0.02239;      2.68;        NaN
corr_gp_gm;  -0.77235;   0.02883;      3.73;        NaN
         a;  62.27601;   1.18453;      1.90;        NaN
         b;   0.08644;   0.00064;      0.74;        NaN
