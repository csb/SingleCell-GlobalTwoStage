    ID	    km_mean	   kp_mean	   gp_mean	   gm_mean	   ltau_mean	   km_sd	   kp_sd	   gp_sd	   gm_sd	   ltau_sd	
     1	     2.1699	   -0.0545	   -4.5386	    -4.331	         3.4	0.071932	       0	 0.19294	 0.19687	         0	
     2	     2.0079	   -0.0545	   -4.9409	   -4.4365	         3.4	0.039605	       0	 0.12191	 0.13047	         0	
     3	     1.4896	   -0.0545	   -5.2427	   -4.5008	         3.4	0.038403	       0	 0.17737	  0.1793	         0	
     4	      2.293	   -0.0545	   -4.8847	   -4.0744	         3.4	0.048027	       0	 0.10932	 0.13544	         0	
     5	     1.4286	   -0.0545	   -5.0576	   -4.7049	         3.4	7.885e-08	       0	1.4901e-08	6.9254e-07	         0	
     6	     1.6169	   -0.0545	   -4.7771	   -4.9419	         3.4	0.070512	       0	 0.16331	 0.15863	         0	
     7	     1.5224	   -0.0545	   -5.5437	   -4.3857	         3.4	1.1542e-07	       0	0.0042332	0.00071065	         0	
     8	     1.7363	   -0.0545	   -4.8689	   -4.7138	         3.4	0.045491	       0	 0.19102	  0.1931	         0	
     9	      1.592	   -0.0545	   -5.3362	   -4.2175	         3.4	0.067405	       0	 0.39815	 0.38843	         0	
    10	     2.4196	   -0.0545	    -4.444	   -4.6126	         3.4	0.047947	       0	0.088424	0.087326	         0	
    11	     2.5114	   -0.0545	   -4.5689	    -4.176	         3.4	0.042803	       0	0.085106	0.096962	         0	
    12	     1.4296	   -0.0545	   -4.8656	   -5.0897	         3.4	0.022215	       0	0.046495	0.032558	         0	
    13	     2.0468	   -0.0545	   -4.6847	   -4.6294	         3.4	0.046593	       0	 0.13028	 0.13203	         0	
    14	     1.9173	   -0.0545	   -4.6878	   -4.7046	         3.4	0.041428	       0	0.073002	0.076401	         0	
    15	     1.5423	   -0.0545	   -4.9101	   -4.8494	         3.4	1.4901e-08	       0	5.4302e-07	1.4901e-08	         0	
    16	     1.5048	   -0.0545	   -5.4705	   -4.3579	         3.4	0.0085247	       0	0.010213	0.0056847	         0	
    17	      1.858	   -0.0545	   -4.8034	   -4.7469	         3.4	0.043423	       0	0.091822	0.092733	         0	
    18	     2.2776	   -0.0545	   -4.6334	   -4.4388	         3.4	0.042941	       0	 0.14359	 0.15751	         0	
    19	     1.5392	   -0.0545	   -5.0087	   -4.8532	         3.4	0.038327	       0	 0.10556	0.097783	         0	
    20	     2.0843	   -0.0545	   -4.6992	   -4.5539	         3.4	0.005144	       0	0.0069416	3.8628e-07	         0	
    21	     1.8993	   -0.0545	   -5.5845	    -3.777	         3.4	0.071906	       0	0.092986	 0.12435	         0	
    22	     1.8963	   -0.0545	   -4.6388	   -4.5254	         3.4	0.051257	       0	 0.12218	 0.12142	         0	
    23	     1.8929	   -0.0545	   -4.7772	   -4.5274	         3.4	0.048853	       0	 0.14257	 0.13697	         0	
    24	     1.5752	   -0.0545	   -4.8835	   -4.8669	         3.4	0.046932	       0	 0.13975	 0.13273	         0	
    25	     2.2962	   -0.0545	   -4.6228	   -4.3753	         3.4	 0.06102	       0	 0.19227	 0.19504	         0	
    26	     1.8992	   -0.0545	   -5.5912	   -3.8949	         3.4	0.046224	       0	 0.07207	0.085918	         0	
    27	     1.3471	   -0.0545	   -4.9788	   -4.9357	         3.4	0.047169	       0	 0.12718	  0.1322	         0	
    28	     1.6349	   -0.0545	   -4.8898	   -4.6568	         3.4	 0.04214	       0	0.079564	 0.08808	         0	
    29	     1.8237	   -0.0545	   -4.5941	   -4.7125	         3.4	  0.0834	       0	 0.10927	0.095086	         0	
    30	     1.5077	   -0.0545	   -5.4499	   -4.3589	         3.4	0.012785	       0	0.021905	0.015127	         0	
    31	     1.6277	   -0.0545	   -4.7638	   -4.8372	         3.4	0.064654	       0	 0.18693	  0.1831	         0	
    32	      1.456	   -0.0545	   -4.7412	   -4.9604	         3.4	1.6859e-07	       0	1.4901e-08	1.4901e-08	         0	
    33	     1.9829	   -0.0545	   -4.7541	   -4.5631	         3.4	0.063864	       0	 0.11374	 0.12196	         0	
    34	     2.0522	   -0.0545	   -4.2712	   -4.9534	         3.4	 0.21246	       0	  0.2483	 0.18006	         0	
    35	     1.9979	   -0.0545	     -5.81	   -3.6181	         3.4	 0.05653	       0	0.072199	 0.06676	         0	
    36	     1.9706	   -0.0545	   -4.8086	   -4.4784	         3.4	0.071939	       0	 0.18812	 0.19288	         0	
    37	     2.8748	   -0.0545	   -5.2719	   -3.1409	         3.4	 0.18713	       0	0.080513	 0.24761	         0	
    38	     1.1297	   -0.0545	   -5.0491	    -5.237	         3.4	0.0067294	       0	0.0054761	0.0033429	         0	
    39	      2.924	   -0.0545	   -4.4775	   -3.9551	         3.4	0.083914	       0	 0.16411	 0.20492	         0	
    40	     2.2945	   -0.0545	   -4.7302	   -4.2211	         3.4	 0.10374	       0	 0.21904	 0.22053	         0	
    41	     2.4002	   -0.0545	   -4.7574	   -4.2068	         3.4	 0.10163	       0	 0.24623	 0.30031	         0	
    42	      3.456	   -0.0545	   -5.6481	   -2.2428	         3.4	 0.41586	       0	0.087691	 0.44817	         0	
    43	     1.9725	   -0.0545	   -4.8438	   -4.3799	         3.4	   0.124	       0	 0.22639	 0.31556	         0	
    44	     1.9278	   -0.0545	   -5.0995	   -4.1683	         3.4	 0.14366	       0	 0.29038	 0.37978	         0	
    45	     2.6051	   -0.0545	   -4.3166	   -4.4199	         3.4	0.091917	       0	 0.17664	 0.17324	         0	
    46	     2.6703	   -0.0545	   -4.6036	   -4.1577	         3.4	0.089858	       0	 0.11106	 0.17401	         0	
    47	     3.2372	   -0.0545	   -5.9914	   -1.9228	         3.4	 0.47518	       0	 0.11664	 0.48961	         0	
    48	     2.5816	   -0.0545	   -5.4515	    -3.088	         3.4	  0.6429	       0	 0.36144	 0.89201	         0	
    49	     2.1326	   -0.0545	   -5.0976	   -3.9576	         3.4	 0.31301	       0	 0.33848	 0.56216	         0	
    50	      2.834	   -0.0545	    -6.317	   -2.3489	         3.4	 0.53091	       0	 0.15818	 0.57721	         0	
    51	     2.8011	   -0.0545	   -4.8688	   -3.4841	         3.4	 0.36991	       0	 0.30967	 0.65648	         0	
    52	     3.3839	   -0.0545	   -5.6247	   -2.2577	         3.4	 0.72478	       0	 0.12224	 0.73553	         0	
    53	     3.3325	   -0.0545	   -5.2883	   -2.4416	         3.4	 0.24987	       0	0.088749	 0.26713	         0	
    54	     3.2044	   -0.0545	      -5.9	   -2.2988	         3.4	 0.17775	       0	 0.10178	 0.19141	         0	
    55	     2.4302	   -0.0545	   -4.9882	   -3.7816	         3.4	 0.31879	       0	 0.27854	  0.5175	         0	
    56	     2.9945	   -0.0545	   -4.4909	   -3.8628	         3.4	 0.10522	       0	 0.11608	  0.1341	         0	
    57	     2.6479	   -0.0545	   -4.6565	   -3.9982	         3.4	 0.22838	       0	 0.22486	 0.41119	         0	
    58	     2.6132	   -0.0545	   -5.1908	   -3.2663	         3.4	 0.36091	       0	 0.12384	 0.36547	         0	
    59	     2.3206	   -0.0545	   -4.8233	   -4.1321	         3.4	 0.25704	       0	 0.23779	 0.44294	         0	
    60	     2.6831	   -0.0545	   -4.8341	   -3.8531	         3.4	 0.22014	       0	 0.12148	 0.27084	         0	
    61	     3.2419	   -0.0545	   -5.6014	     -2.44	         3.4	 0.38692	       0	 0.10734	 0.41273	         0	
    62	     2.9736	   -0.0545	   -5.6158	   -2.8624	         3.4	 0.31294	       0	 0.10046	  0.3225	         0	
    63	     2.0581	   -0.0545	   -4.7486	   -4.4732	         3.4	 0.10197	       0	0.056238	 0.08765	         0	
    64	      2.528	   -0.0545	   -4.8513	   -3.8723	         3.4	 0.27563	       0	 0.14846	 0.34624	         0	
    65	     2.0607	   -0.0545	   -4.7955	   -4.2922	         3.4	 0.36737	       0	 0.30876	 0.59341	         0	
    66	     3.2487	   -0.0545	   -5.0404	   -2.9671	         3.4	  0.2884	       0	 0.11398	  0.2904	         0	
    67	     3.0156	   -0.0545	   -4.9754	   -3.4273	         3.4	 0.33519	       0	 0.12533	 0.28648	         0	
    68	     2.4599	   -0.0545	   -4.1695	   -4.5444	         3.4	 0.22175	       0	 0.28702	 0.26589	         0	
    69	     2.2793	   -0.0545	    -4.765	   -4.2164	         3.4	 0.18707	       0	 0.16113	 0.22544	         0	
    70	     3.1837	   -0.0545	    -5.336	   -2.8524	         3.4	 0.64701	       0	 0.19595	 0.65636	         0	
    71	     2.8212	   -0.0545	   -5.2097	   -3.2642	         3.4	 0.42172	       0	 0.10768	 0.38936	         0	
    72	     3.4185	   -0.0545	    -5.748	   -2.2292	         3.4	 0.60019	       0	 0.18883	 0.52625	         0	
    73	     2.7907	   -0.0545	   -5.2413	   -3.2947	         3.4	 0.49174	       0	 0.13559	 0.49466	         0	
    74	     2.8631	   -0.0545	   -4.5271	   -3.6944	         3.4	 0.20862	       0	  0.2379	 0.36401	         0	
    75	     2.4789	   -0.0545	    -4.675	   -4.1189	         3.4	 0.14523	       0	 0.13173	 0.19016	         0	
    76	     3.3663	   -0.0545	   -5.8633	   -1.9929	         3.4	 0.54341	       0	 0.32301	 0.56768	         0	
    77	     2.7434	   -0.0545	   -4.4169	    -4.193	         3.4	  0.1815	       0	 0.14989	 0.14632	         0	
    78	     2.7073	   -0.0545	   -4.6701	   -3.9624	         3.4	 0.24288	       0	 0.22321	  0.3245	         0	
    79	     2.8567	   -0.0545	   -4.6222	   -3.9183	         3.4	 0.24611	       0	 0.19271	 0.22818	         0	
    80	     1.8321	   -0.0545	   -5.0489	   -4.5147	         3.4	 0.27243	       0	 0.32185	 0.48477	         0	
    81	     2.4219	   -0.0545	    -4.746	   -4.1268	         3.4	  0.3092	       0	 0.24481	 0.34074	         0	
    82	     3.3206	   -0.0545	   -4.9234	    -3.127	         3.4	 0.32765	       0	 0.15109	 0.25523	         0	
    83	     2.7535	   -0.0545	   -4.9766	   -3.4572	         3.4	 0.27683	       0	 0.20265	 0.26084	         0	
    84	     3.0066	   -0.0545	   -5.5341	   -2.5259	         3.4	 0.37799	       0	 0.29617	 0.49513	         0	
    85	     2.6048	   -0.0545	   -4.5629	   -4.1926	         3.4	  0.2874	       0	  0.2511	 0.27228	         0	
    86	     3.1685	   -0.0545	   -5.7962	   -2.4123	         3.4	 0.53379	       0	 0.20531	 0.58742	         0	
    87	      1.742	   -0.0545	   -4.3026	   -4.9371	         3.4	 0.08399	       0	 0.12538	 0.13747	         0	
    88	     2.2612	   -0.0545	   -5.2559	   -3.6788	         3.4	0.057591	       0	0.079281	 0.11027	         0	
    89	     1.9138	   -0.0545	   -3.8983	   -5.3851	         3.4	 0.09341	       0	0.080354	0.070879	         0	
    90	     2.3567	   -0.0545	   -5.6729	   -3.0383	         3.4	 0.12162	       0	0.081518	 0.16529	         0	
    91	     1.9183	   -0.0545	   -4.4609	   -4.7767	         3.4	0.092359	       0	 0.33399	 0.34856	         0	
    92	     2.0408	   -0.0545	   -5.5666	   -3.6655	         3.4	0.080676	       0	0.097872	 0.14245	         0	
    93	     2.7909	   -0.0545	    -5.719	   -2.6373	         3.4	 0.15189	       0	  0.0636	 0.18242	         0	
    94	     1.3529	   -0.0545	   -4.8948	   -4.9737	         3.4	0.083521	       0	 0.24793	 0.24361	         0	
    95	     1.3797	   -0.0545	   -4.6054	   -5.3086	         3.4	 0.12786	       0	 0.23109	 0.22222	         0	
    96	     1.1964	   -0.0545	   -4.7215	   -5.1073	         3.4	0.045835	       0	 0.10092	 0.12496	         0	
    97	     1.6343	   -0.0545	   -4.6709	   -4.6926	         3.4	 0.07319	       0	 0.15425	  0.1389	         0	
    98	     2.1313	   -0.0545	   -5.9114	   -3.2643	         3.4	 0.23883	       0	 0.11957	 0.30169	         0	
    99	     2.9405	   -0.0545	   -5.7646	   -2.6108	         3.4	 0.35654	       0	0.074068	 0.38172	         0	
   100	     2.7603	   -0.0545	   -5.5073	   -2.9347	         3.4	 0.17833	       0	0.064266	 0.19864	         0	
   101	     0.9944	   -0.0545	   -4.9003	   -5.2261	         3.4	 0.11163	       0	 0.24923	 0.24445	         0	
   102	     3.2278	   -0.0545	   -5.9977	   -2.1369	         3.4	 0.42568	       0	0.074142	 0.43362	         0	
   103	     3.4994	   -0.0545	   -5.5599	   -1.8978	         3.4	  0.2975	       0	0.081874	 0.28183	         0	
   104	     3.0114	   -0.0545	   -5.8453	   -2.2372	         3.4	 0.33212	       0	0.093675	  0.3278	         0	
   105	     2.0267	   -0.0545	   -4.5643	   -4.3894	         3.4	 0.11318	       0	 0.17162	  0.2245	         0	
   106	     2.8818	   -0.0545	   -5.5834	   -2.6802	         3.4	 0.61912	       0	   0.105	 0.65878	         0	
   107	     2.1947	   -0.0545	   -4.8963	   -4.0021	         3.4	 0.19057	       0	 0.18793	 0.32923	         0	
   108	     2.6274	   -0.0545	   -5.2343	   -3.1294	         3.4	 0.35213	       0	0.097489	 0.34728	         0	
   109	     3.2089	   -0.0545	   -5.2211	   -2.7641	         3.4	 0.30522	       0	 0.07925	 0.32912	         0	
   110	     2.4264	   -0.0545	   -5.7293	   -2.9264	         3.4	 0.25185	       0	0.088998	 0.25816	         0	
   111	     2.2795	   -0.0545	   -5.3375	   -3.5098	         3.4	 0.34983	       0	 0.12594	 0.38256	         0	
   112	      1.688	   -0.0545	   -5.4192	   -3.6105	         3.4	 0.17269	       0	 0.15589	  0.1621	         0	
   113	      2.121	   -0.0545	   -3.9168	   -5.1468	         3.4	  0.2394	       0	 0.13939	 0.11237	         0	
   114	     3.0266	   -0.0545	   -5.7192	   -2.5419	         3.4	  0.4037	       0	 0.13907	 0.41163	         0	
   115	     2.9071	   -0.0545	   -5.7779	   -2.3756	         3.4	 0.49206	       0	 0.14937	 0.50103	         0	
   116	     2.5554	   -0.0545	   -5.8859	   -2.7158	         3.4	  0.2438	       0	 0.17264	 0.18424	         0	
   117	     2.6318	   -0.0545	   -5.0097	   -3.5527	         3.4	 0.26102	       0	 0.14509	  0.3051	         0	
   118	     1.8616	   -0.0545	   -5.1588	   -4.1696	         3.4	 0.79265	       0	 0.61003	  1.3659	         0	
   119	     2.0647	   -0.0545	   -4.6038	   -4.4611	         3.4	 0.33247	       0	 0.37093	 0.56942	         0	
   120	     2.0892	   -0.0545	   -4.9348	   -4.1155	         3.4	 0.65481	       0	 0.62754	  1.2017	         0	
   121	     2.1241	   -0.0545	   -4.5283	   -4.4092	         3.4	1.4901e-08	       0	1.4901e-08	1.4901e-08	         0	
   122	     1.8988	   -0.0545	   -5.4135	   -3.8912	         3.4	 0.28497	       0	  1.0744	  1.4484	         0	
   123	     2.0991	   -0.0545	   -5.2798	   -3.6019	         3.4	 0.42034	       0	 0.50313	 0.91777	         0	
   124	     3.8003	   -0.0545	   -6.5383	   -1.0668	         3.4	 0.18574	       0	 0.16424	 0.20653	         0	
   125	     2.3569	   -0.0545	   -5.6053	   -3.2577	         3.4	 0.52064	       0	 0.22899	 0.59258	         0	
   126	     2.5512	   -0.0545	   -5.7136	   -2.9421	         3.4	 0.53393	       0	 0.25105	 0.60066	         0	
   127	     2.8835	   -0.0545	   -6.2835	    -2.243	         3.4	 0.39205	       0	 0.17896	 0.38184	         0	
   128	     2.9954	   -0.0545	   -6.0816	   -2.2885	         3.4	 0.35047	       0	 0.24556	 0.28788	         0	
   129	     1.9183	   -0.0545	   -5.1062	   -4.1353	         3.4	 0.93148	       0	   1.059	  1.9906	         0	
   130	      2.633	   -0.0545	   -5.1805	   -3.3805	         3.4	 0.49953	       0	 0.26785	    0.62	         0	
   131	     1.6395	   -0.0545	   -5.3859	   -4.3776	         3.4	0.0054464	       0	0.019434	0.0066693	         0	
   132	     2.1177	   -0.0545	   -5.1269	   -4.0458	         3.4	0.045834	       0	0.098652	 0.12101	         0	
   133	     2.3464	   -0.0545	   -4.6066	   -4.4958	         3.4	0.0081421	       0	0.010419	0.033326	         0	
   134	     1.8894	   -0.0545	    -4.645	   -4.7418	         3.4	0.041907	       0	0.089183	 0.10253	         0	
   135	     2.1017	   -0.0545	   -4.7226	   -4.5386	         3.4	0.00042044	       0	0.0091284	0.0044015	         0	
   136	     1.5074	   -0.0545	   -4.9709	   -4.9242	         3.4	0.040084	       0	0.078546	0.079607	         0	
   137	     1.8166	   -0.0545	   -4.7634	   -4.8199	         3.4	0.033689	       0	0.057299	0.067538	         0	
   138	     2.0492	   -0.0545	   -4.7638	   -4.3691	         3.4	0.052996	       0	 0.14759	 0.15759	         0	
   139	     2.7642	   -0.0545	   -5.6408	   -3.0149	         3.4	0.081223	       0	0.062645	 0.10992	         0	
   140	     1.5648	   -0.0545	   -4.8197	   -4.8339	         3.4	0.061706	       0	 0.13023	 0.12592	         0	
   141	     1.9837	   -0.0545	   -4.7855	   -4.5068	         3.4	1.5196e-07	       0	0.0011048	1.4901e-08	         0	
   142	     1.1506	   -0.0545	   -5.0123	   -5.2065	         3.4	0.0066867	       0	0.015748	0.018192	         0	
   143	     1.5261	   -0.0545	   -4.8321	   -4.9161	         3.4	 0.08232	       0	0.072375	0.083494	         0	
   144	     2.0831	   -0.0545	   -5.3732	   -3.9296	         3.4	0.056547	       0	0.094796	 0.12041	         0	
   145	     2.0447	   -0.0545	   -4.6751	    -4.446	         3.4	0.010318	       0	0.036997	0.019135	         0	
   146	     2.0389	   -0.0545	   -4.8255	   -4.5416	         3.4	1.4901e-08	       0	1.4901e-08	6.1943e-07	         0	
   147	     1.0577	   -0.0545	    -5.605	   -4.9897	         3.4	0.010645	       0	0.013657	0.0014385	         0	
   148	    0.80383	   -0.0545	   -4.9668	   -5.1932	         3.4	0.056333	       0	 0.18907	 0.23538	         0	
   149	     1.9386	   -0.0545	   -4.9486	    -4.429	         3.4	0.0023836	       0	0.014015	0.0084693	         0	
   150	     1.9217	   -0.0545	    -5.481	   -3.7769	         3.4	0.051441	       0	 0.07274	0.083763	         0	
   151	     2.1379	   -0.0545	   -4.6142	   -4.5183	         3.4	0.042894	       0	 0.10495	 0.10852	         0	
   152	     1.9302	   -0.0545	   -4.8125	   -4.7805	         3.4	0.042996	       0	0.080064	0.077094	         0	
   153	     1.7955	   -0.0545	   -4.7507	   -4.6593	         3.4	0.040019	       0	0.094815	 0.10153	         0	
   154	     1.7041	   -0.0545	   -4.8801	   -4.7626	         3.4	0.037262	       0	 0.12748	 0.13128	         0	
   155	     2.0454	   -0.0545	   -4.8229	   -4.4882	         3.4	1.4901e-08	       0	1.4901e-08	1.4901e-08	         0	
   156	     2.2421	   -0.0545	   -4.6737	   -4.4313	         3.4	0.041207	       0	 0.10384	 0.12432	         0	
   157	     1.8432	   -0.0545	   -5.1703	   -4.3929	         3.4	0.023347	       0	0.032793	0.013691	         0	
   158	     2.3772	   -0.0545	   -4.4349	   -4.3953	         3.4	0.081948	       0	 0.19518	 0.21491	         0	
   159	     2.4479	   -0.0545	   -4.6628	   -4.2996	         3.4	0.046473	       0	 0.13292	 0.16901	         0	
   160	     2.2994	   -0.0545	   -4.8541	   -3.9548	         3.4	0.091612	       0	 0.28703	 0.35089	         0	
   161	     1.8488	   -0.0545	    -5.145	    -4.306	         3.4	 0.13265	       0	 0.39824	 0.46685	         0	
   162	     1.9799	   -0.0545	   -4.7758	   -4.5552	         3.4	0.071731	       0	 0.13509	  0.1708	         0	
   163	     1.9738	   -0.0545	   -4.8461	   -4.6109	         3.4	0.057441	       0	 0.10242	 0.12715	         0	
   164	     2.0045	   -0.0545	   -4.9959	   -4.4025	         3.4	0.014882	       0	0.0037139	0.0092444	         0	
   165	     2.5908	   -0.0545	   -4.4132	   -4.2334	         3.4	0.068295	       0	0.098811	 0.10042	         0	
   166	     2.3548	   -0.0545	   -4.6657	    -4.137	         3.4	  0.1141	       0	 0.21679	 0.24713	         0	
   167	     1.4468	   -0.0545	   -4.7668	   -4.9577	         3.4	0.097345	       0	 0.27625	 0.28311	         0	
   168	     2.6404	   -0.0545	   -4.5245	   -4.1839	         3.4	0.079284	       0	 0.13012	  0.1687	         0	
   169	     2.3504	   -0.0545	   -5.3482	   -3.6377	         3.4	 0.17051	       0	0.073067	   0.214	         0	
   170	     2.2553	   -0.0545	   -4.6159	   -4.3386	         3.4	 0.15104	       0	  0.3092	 0.38463	         0	
   171	     2.2024	   -0.0545	   -4.4967	   -4.3383	         3.4	 0.24103	       0	 0.71067	 0.98962	         0	
   172	     2.3151	   -0.0545	   -4.9025	   -3.9824	         3.4	 0.18795	       0	 0.30335	 0.48186	         0	
   173	     2.1408	   -0.0545	   -4.5575	   -4.3181	         3.4	 0.10732	       0	 0.20659	 0.23853	         0	
   174	     1.8356	   -0.0545	   -4.9367	   -4.4261	         3.4	0.017549	       0	0.011475	0.011328	         0	
   175	     1.9559	   -0.0545	   -5.1564	   -4.1278	         3.4	 0.19144	       0	  0.4537	 0.58984	         0	
   176	     2.7173	   -0.0545	   -4.4873	    -4.318	         3.4	0.076061	       0	0.094785	 0.14612	         0	
   177	     2.5272	   -0.0545	    -5.147	   -3.5331	         3.4	 0.35926	       0	 0.09717	  0.3954	         0	
   178	       2.45	   -0.0545	   -4.7319	   -4.0111	         3.4	 0.11302	       0	  0.2079	 0.27981	         0	
   179	     2.5332	   -0.0545	   -4.8628	     -3.79	         3.4	 0.25101	       0	 0.21317	 0.43005	         0	
   180	     2.6113	   -0.0545	   -5.5376	   -3.1324	         3.4	  0.2478	       0	0.085265	 0.26031	         0	
   181	     2.6034	   -0.0545	   -5.2065	   -3.4618	         3.4	 0.25714	       0	0.097713	 0.27934	         0	
   182	     2.4542	   -0.0545	   -5.0194	   -3.9367	         3.4	 0.12991	       0	0.091849	 0.18026	         0	
   183	     2.3957	   -0.0545	   -4.7176	   -4.3369	         3.4	 0.10944	       0	 0.20461	 0.20375	         0	
   184	     2.5949	   -0.0545	   -4.7902	   -4.0308	         3.4	 0.11545	       0	 0.14648	 0.23439	         0	
   185	     3.2567	   -0.0545	   -5.0746	   -3.1869	         3.4	 0.14987	       0	0.076642	 0.18762	         0	
   186	     3.5216	   -0.0545	   -5.1363	   -2.4976	         3.4	 0.33439	       0	0.098412	 0.36215	         0	
   187	     2.2304	   -0.0545	   -4.6884	   -4.2714	         3.4	 0.13152	       0	 0.13602	 0.25432	         0	
   188	     2.9615	   -0.0545	   -4.8994	   -3.3907	         3.4	 0.16822	       0	0.085011	 0.17219	         0	
   189	     2.6622	   -0.0545	   -5.0386	   -3.5333	         3.4	 0.26996	       0	 0.12823	 0.25469	         0	
   190	     3.2443	   -0.0545	   -5.3798	    -2.601	         3.4	 0.47235	       0	   0.124	 0.45397	         0	
   191	     1.6241	   -0.0545	   -3.9866	   -5.4957	         3.4	 0.20708	       0	 0.14988	 0.12593	         0	
   192	     2.7776	   -0.0545	   -4.5267	   -4.0908	         3.4	0.085304	       0	 0.12516	 0.17881	         0	
   193	     3.1187	   -0.0545	   -4.4372	   -3.7394	         3.4	 0.13425	       0	 0.14365	 0.23822	         0	
   194	     3.0845	   -0.0545	    -4.505	   -3.7143	         3.4	  0.1619	       0	 0.14028	 0.18616	         0	
   195	     2.6383	   -0.0545	   -4.4474	   -4.3481	         3.4	 0.15235	       0	 0.12827	 0.17118	         0	
   196	     2.3818	   -0.0545	   -4.3727	    -4.337	         3.4	 0.10237	       0	 0.11459	 0.14765	         0	
   197	     2.9718	   -0.0545	    -4.293	   -4.1799	         3.4	 0.15148	       0	 0.10823	 0.12471	         0	
   198	      2.491	   -0.0545	   -4.5384	   -4.2787	         3.4	 0.15327	       0	 0.14917	 0.22243	         0	
   199	     2.5176	   -0.0545	   -4.8178	   -4.1034	         3.4	 0.16669	       0	 0.22353	 0.32073	         0	
   200	     4.0516	   -0.0545	   -5.2463	   -2.0464	         3.4	 0.36984	       0	 0.18326	 0.32958	         0	
   201	     2.5407	   -0.0545	   -4.8346	   -4.1725	         3.4	 0.28518	       0	 0.17673	 0.27078	         0	
   202	     2.8606	   -0.0545	   -4.4508	   -3.9944	         3.4	 0.29893	       0	 0.19739	 0.22589	         0	
   203	     2.9059	   -0.0545	   -4.6739	    -3.727	         3.4	 0.21356	       0	  0.1484	 0.19159	         0	
   204	     2.8969	   -0.0545	   -5.5326	   -2.8685	         3.4	 0.52757	       0	 0.25755	 0.41044	         0	
   205	     2.7751	   -0.0545	    -5.047	   -3.4281	         3.4	 0.35454	       0	 0.24552	 0.25634	         0	
   206	     2.9204	   -0.0545	   -4.6199	   -3.7461	         3.4	  0.1929	       0	 0.12495	  0.2036	         0	
   207	     2.5613	   -0.0545	   -4.4396	   -4.2563	         3.4	 0.23475	       0	 0.13985	 0.13714	         0	
   208	     2.7106	   -0.0545	   -4.6649	   -4.0483	         3.4	 0.18221	       0	 0.15377	 0.25285	         0	
   209	     2.5547	   -0.0545	   -4.5841	   -4.1043	         3.4	 0.19249	       0	 0.18426	 0.23217	         0	
   210	     2.9846	   -0.0545	    -4.742	   -3.6133	         3.4	 0.24865	       0	  0.1819	 0.24301	         0	
   211	     2.6895	   -0.0545	   -4.5267	   -4.0432	         3.4	 0.28866	       0	  0.1933	 0.29775	         0	
   212	     2.6553	   -0.0545	   -4.4641	   -4.1191	         3.4	 0.20706	       0	 0.23777	 0.25113	         0	
   213	     1.9192	   -0.0545	   -4.5962	   -4.6946	         3.4	0.066577	       0	 0.22211	 0.23055	         0	
   214	     2.1158	   -0.0545	   -5.3179	    -3.938	         3.4	0.054968	       0	 0.12437	 0.14969	         0	
   215	     2.5801	   -0.0545	   -5.4242	   -3.2457	         3.4	 0.10442	       0	0.086647	 0.16185	         0	
   216	     1.1032	   -0.0545	   -4.6873	   -5.2755	         3.4	0.088458	       0	 0.17463	 0.17598	         0	
   217	     2.1367	   -0.0545	   -5.2074	   -3.7599	         3.4	0.063571	       0	0.098158	 0.13766	         0	
   218	     1.7162	   -0.0545	   -4.9651	   -4.6095	         3.4	 0.03977	       0	  0.1146	  0.1136	         0	
   219	     1.9658	   -0.0545	   -5.1549	   -4.2344	         3.4	0.045773	       0	 0.15619	 0.17477	         0	
   220	     2.2642	   -0.0545	   -5.6675	   -3.4999	         3.4	0.087383	       0	0.078628	 0.13006	         0	
   221	     1.6795	   -0.0545	   -4.8108	    -4.787	         3.4	0.040268	       0	 0.14887	 0.15813	         0	
   222	     2.1869	   -0.0545	   -4.5136	    -4.605	         3.4	0.074951	       0	 0.34086	 0.36644	         0	
   223	     1.0243	   -0.0545	   -4.9742	   -5.1087	         3.4	0.060536	       0	 0.14798	 0.14269	         0	
   224	     2.2081	   -0.0545	   -4.6178	   -4.4047	         3.4	 0.08916	       0	 0.27947	 0.29546	         0	
   225	     1.8687	   -0.0545	   -5.5174	    -3.658	         3.4	  0.3542	       0	 0.62682	 0.93744	         0	
   226	     2.7529	   -0.0545	   -5.5072	   -2.9633	         3.4	 0.21472	       0	0.091523	 0.26629	         0	
   227	     2.3393	   -0.0545	   -6.1952	   -2.8988	         3.4	 0.20723	       0	0.089383	 0.22277	         0	
   228	     2.4732	   -0.0545	    -5.578	   -3.5069	         3.4	   0.123	       0	0.073175	 0.15403	         0	
   229	     2.8566	   -0.0545	   -5.4265	   -2.9993	         3.4	 0.35619	       0	0.062678	 0.37831	         0	
   230	    0.88753	   -0.0545	   -5.2281	   -5.0512	         3.4	0.056263	       0	  0.1046	 0.10917	         0	
   231	     2.4345	   -0.0545	   -5.1582	   -3.6142	         3.4	 0.20713	       0	 0.15008	 0.33993	         0	
   232	     2.7193	   -0.0545	   -5.5027	    -3.024	         3.4	 0.24827	       0	0.085368	 0.27123	         0	
   233	     2.4532	   -0.0545	   -5.8597	   -2.5505	         3.4	 0.28848	       0	  0.1086	 0.31139	         0	
   234	     2.7982	   -0.0545	    -5.859	   -2.5717	         3.4	 0.37806	       0	 0.11404	  0.3776	         0	
   235	     2.9892	   -0.0545	   -5.9911	   -2.3504	         3.4	 0.28746	       0	0.092015	 0.29732	         0	
   236	     3.1597	   -0.0545	   -6.9054	   -1.4251	         3.4	 0.21843	       0	 0.14465	 0.21578	         0	
   237	     3.4139	   -0.0545	   -5.8874	   -2.1792	         3.4	  0.3433	       0	 0.10682	 0.32685	         0	
   238	     2.8605	   -0.0545	   -5.8669	   -2.4444	         3.4	 0.37759	       0	 0.11838	 0.37114	         0	
   239	     2.2135	   -0.0545	   -4.5636	   -4.4963	         3.4	 0.23172	       0	 0.39707	 0.45992	         0	
   240	     2.9411	   -0.0545	   -5.9227	   -2.3062	         3.4	  0.2692	       0	 0.13602	 0.30599	         0	
   241	     3.4394	   -0.0545	   -5.6433	   -2.2798	         3.4	 0.50651	       0	0.098316	 0.47392	         0	
   242	      3.192	   -0.0545	    -5.693	    -2.372	         3.4	 0.58705	       0	 0.14494	 0.51209	         0	
   243	     1.5858	   -0.0545	   -5.0498	   -4.5264	         3.4	 0.19371	       0	  0.3753	 0.49552	         0	
   244	     3.2979	   -0.0545	   -5.6363	   -2.5465	         3.4	 0.30243	       0	 0.12215	 0.30609	         0	
   245	     2.2701	   -0.0545	   -5.3178	    -3.548	         3.4	 0.66386	       0	 0.50552	   1.033	         0	
   246	     2.1584	   -0.0545	   -4.7545	   -4.3062	         3.4	 0.21508	       0	 0.19704	 0.33011	         0	
   247	      3.047	   -0.0545	   -5.8263	   -2.3987	         3.4	 0.32563	       0	 0.16516	 0.31155	         0	
   248	      2.362	   -0.0545	   -5.2543	   -3.5506	         3.4	 0.45111	       0	 0.26644	 0.62866	         0	
   249	     3.1484	   -0.0545	   -6.5046	   -1.5946	         3.4	 0.79537	       0	 0.15058	 0.80681	         0	
   250	     2.5654	   -0.0545	   -5.2073	   -3.4316	         3.4	 0.40637	       0	 0.17541	   0.386	         0	
   251	     2.4768	   -0.0545	   -4.7092	   -4.0835	         3.4	 0.30089	       0	 0.15655	 0.30348	         0	
   252	     2.8753	   -0.0545	   -5.5722	    -2.803	         3.4	 0.38931	       0	 0.17383	 0.39528	         0	
   253	     2.8458	   -0.0545	    -5.471	   -2.9641	         3.4	 0.47858	       0	 0.23163	 0.50839	         0	
   254	     2.6227	   -0.0545	    -6.595	    -1.996	         3.4	 0.36237	       0	 0.22504	 0.40836	         0	
   255	     3.2915	   -0.0545	   -5.6092	   -2.2851	         3.4	 0.57961	       0	 0.27447	  0.5826	         0	
   256	     2.5765	   -0.0545	   -4.8928	   -3.6796	         3.4	 0.36052	       0	 0.31984	 0.36732	         0	
   257	       3.25	   -0.0545	   -5.6355	    -2.462	         3.4	 0.51807	       0	 0.25983	 0.56915	         0	
   258	     2.9665	   -0.0545	     -5.15	   -3.0562	         3.4	 0.36466	       0	 0.20121	 0.30177	         0	
   259	      2.922	   -0.0545	   -5.9669	   -2.4157	         3.4	 0.56939	       0	  0.2694	 0.63504	         0	
   260	     1.2412	   -0.0545	   -5.2436	   -4.9139	         3.4	0.035692	       0	 0.13419	 0.13297	         0	
   261	     2.0712	   -0.0545	   -6.0205	   -3.1254	         3.4	0.097303	       0	0.092661	 0.12999	         0	
   262	     1.9763	   -0.0545	   -5.1435	   -4.0689	         3.4	0.047772	       0	 0.11673	 0.14607	         0	
   263	     1.9927	   -0.0545	   -4.3176	   -4.6368	         3.4	0.084084	       0	  0.1261	 0.11424	         0	
   264	     1.4576	   -0.0545	   -5.4958	   -4.2865	         3.4	0.040995	       0	 0.16953	 0.15491	         0	
   265	     1.7366	   -0.0545	     -4.79	   -4.7265	         3.4	0.046283	       0	 0.12331	 0.12703	         0	
   266	     1.7129	   -0.0545	   -4.7384	   -4.6997	         3.4	0.040821	       0	 0.10631	 0.11582	         0	
   267	     1.6908	   -0.0545	   -4.8269	   -4.6926	         3.4	0.047528	       0	0.097984	0.092096	         0	
   268	     1.3303	   -0.0545	   -5.9056	   -3.9966	         3.4	0.061314	       0	 0.15174	 0.14156	         0	
   269	     2.6107	   -0.0545	   -5.2564	   -3.0443	         3.4	  0.1175	       0	0.086798	 0.17312	         0	
   270	     1.8715	   -0.0545	   -5.8202	   -3.9719	         3.4	0.045778	       0	0.077222	0.083997	         0	
   271	     2.2754	   -0.0545	   -5.2159	   -3.7449	         3.4	0.049674	       0	0.067341	0.094575	         0	
   272	    0.66179	   -0.0545	   -5.2924	   -5.3613	         3.4	0.052032	       0	 0.10913	0.074207	         0	
   273	     2.2493	   -0.0545	   -4.5487	   -4.4259	         3.4	0.054917	       0	 0.16184	 0.17612	         0	
   274	     1.7236	   -0.0545	   -4.7355	   -4.7747	         3.4	0.052067	       0	 0.22988	 0.23326	         0	
   275	     1.8024	   -0.0545	   -4.9723	   -4.5789	         3.4	0.037678	       0	 0.14387	 0.16361	         0	
   276	     1.9699	   -0.0545	   -5.6741	   -3.4143	         3.4	0.085589	       0	0.064637	0.098951	         0	
   277	     2.1702	   -0.0545	   -5.5063	    -3.641	         3.4	 0.13604	       0	0.093467	  0.2004	         0	
   278	     1.8118	   -0.0545	   -5.6522	   -3.7128	         3.4	 0.18223	       0	 0.16265	 0.29339	         0	
   279	     2.0773	   -0.0545	   -5.6938	   -3.7938	         3.4	0.078282	       0	0.077499	 0.12002	         0	
   280	     2.3593	   -0.0545	   -5.0321	   -3.7345	         3.4	 0.10875	       0	 0.10098	 0.20072	         0	
   281	      2.312	   -0.0545	   -5.4035	   -3.4036	         3.4	 0.19289	       0	0.087174	 0.24674	         0	
   282	     2.0116	   -0.0545	   -5.5976	   -3.6247	         3.4	 0.11406	       0	0.099837	 0.17306	         0	
   283	      2.454	   -0.0545	   -5.9577	    -2.842	         3.4	 0.22615	       0	0.071003	 0.24868	         0	
   284	     3.8741	   -0.0545	   -6.0171	   -1.4946	         3.4	 0.22021	       0	0.069539	  0.2183	         0	
   285	     1.7179	   -0.0545	   -5.0826	   -4.4604	         3.4	 0.07219	       0	 0.16479	 0.19081	         0	
   286	      2.293	   -0.0545	   -5.1782	    -3.698	         3.4	 0.20569	       0	 0.13984	 0.31205	         0	
   287	     2.1206	   -0.0545	   -4.6332	   -4.4392	         3.4	 0.08442	       0	 0.23862	 0.27848	         0	
   288	     2.5267	   -0.0545	   -6.6997	   -2.3897	         3.4	 0.17908	       0	0.097387	 0.18965	         0	
   289	     2.8042	   -0.0545	   -5.6041	   -2.8879	         3.4	 0.34672	       0	0.076576	 0.36688	         0	
   290	     3.5295	   -0.0545	    -5.553	   -2.0965	         3.4	 0.15661	       0	0.080232	 0.17641	         0	
   291	      2.609	   -0.0545	   -5.1478	   -3.2938	         3.4	 0.16766	       0	0.069651	 0.18486	         0	
   292	       2.28	   -0.0545	      -4.8	   -3.9648	         3.4	 0.15767	       0	 0.29534	 0.44229	         0	
   293	     3.3529	   -0.0545	   -6.0669	    -1.902	         3.4	 0.20226	       0	 0.11323	 0.21152	         0	
   294	     3.5075	   -0.0545	   -5.9701	   -1.8088	         3.4	 0.14022	       0	 0.14173	 0.10538	         0	
   295	     3.7878	   -0.0545	   -5.4243	   -1.8836	         3.4	0.083181	       0	0.068272	0.083505	         0	
   296	     3.0958	   -0.0545	   -5.5422	   -2.4261	         3.4	 0.33736	       0	0.089431	 0.34385	         0	
   297	     3.1237	   -0.0545	   -5.9185	   -2.2667	         3.4	 0.48026	       0	 0.12336	 0.50011	         0	
   298	     3.5815	   -0.0545	   -5.9096	   -1.8591	         3.4	 0.38842	       0	 0.12492	 0.34716	         0	
   299	     4.0276	   -0.0545	   -5.4309	   -1.7495	         3.4	  0.2515	       0	 0.11235	 0.21853	         0	
   300	     3.5659	   -0.0545	   -5.9823	   -1.7963	         3.4	 0.47319	       0	 0.11798	 0.46307	         0	
   301	     3.2993	   -0.0545	   -5.8052	   -2.0754	         3.4	 0.38719	       0	 0.11792	 0.41974	         0	
   302	     2.7644	   -0.0545	   -6.1019	   -2.8987	         3.4	 0.18259	       0	 0.10792	 0.18276	         0	
   303	     3.2397	   -0.0545	   -5.2968	   -2.5861	         3.4	 0.24051	       0	  0.1425	 0.25193	         0	
   304	     2.9039	   -0.0545	   -5.7588	   -2.7472	         3.4	 0.39138	       0	0.090536	 0.38044	         0	
   305	     2.7549	   -0.0545	   -5.1956	   -3.2956	         3.4	 0.51847	       0	 0.20216	 0.62891	         0	
   306	      2.554	   -0.0545	   -4.5469	   -3.9982	         3.4	 0.15713	       0	 0.17167	 0.29423	         0	
   307	     2.9207	   -0.0545	   -5.5577	   -2.5153	         3.4	 0.54812	       0	 0.19178	 0.54676	         0	
   308	     2.8386	   -0.0545	    -5.644	   -2.6911	         3.4	 0.34143	       0	 0.19233	 0.33612	         0	
   309	     4.2524	   -0.0545	   -6.1313	   -1.0792	         3.4	 0.35916	       0	 0.18448	 0.39951	         0	
   310	     2.2617	   -0.0545	    -4.793	   -4.0914	         3.4	 0.24673	       0	 0.28321	 0.44387	         0	
   311	     3.5402	   -0.0545	   -5.6065	   -2.0715	         3.4	 0.40988	       0	 0.19537	 0.37999	         0	
   312	      3.972	   -0.0545	   -6.1841	   -1.3103	         3.4	 0.10304	       0	 0.19798	 0.15643	         0	
   313	     2.3639	   -0.0545	   -5.5041	   -3.3453	         3.4	 0.58986	       0	 0.21708	 0.69022	         0	
   314	      2.932	   -0.0545	   -5.9313	   -2.5955	         3.4	 0.44987	       0	 0.20341	 0.49091	         0	
   315	     3.3987	   -0.0545	    -6.069	   -1.7869	         3.4	 0.52044	       0	 0.18406	 0.57251	         0	
   316	     2.7617	   -0.0545	   -5.4275	   -2.9111	         3.4	 0.41795	       0	 0.29639	 0.37921	         0	
   317	     4.0554	   -0.0545	   -6.4194	   -1.0219	         3.4	 0.32457	       0	 0.18148	 0.28721	         0	
   318	     2.5966	   -0.0545	   -4.4727	   -4.1737	         3.4	 0.13474	       0	 0.10879	 0.11177	         0	
   319	      2.894	   -0.0545	   -6.5763	   -2.1563	         3.4	 0.30931	       0	 0.16624	 0.30191	         0	
   320	     3.6366	   -0.0545	   -6.4705	   -1.3942	         3.4	 0.17427	       0	 0.12535	  0.1653	         0	
   321	     3.0809	   -0.0545	   -6.7212	   -1.6003	         3.4	 0.21227	       0	 0.15144	 0.23261	         0	
   322	     3.0401	   -0.0545	    -4.496	   -3.7457	         3.4	 0.11306	       0	  0.1503	 0.17582	         0	
   323	     2.9916	   -0.0545	   -5.8943	   -2.4548	         3.4	  0.3076	       0	 0.18209	  0.3116	         0	
   324	     2.2832	   -0.0545	   -4.4385	   -4.4967	         3.4	 0.12755	       0	 0.11518	 0.14077	         0	
   325	     3.0591	   -0.0545	   -5.7663	   -2.5023	         3.4	 0.41164	       0	 0.31478	  0.3423	         0	
