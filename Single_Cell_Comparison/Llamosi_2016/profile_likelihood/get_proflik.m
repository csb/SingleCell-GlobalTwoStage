%%%---------------------------------------------------------------------------------------------------------
%
%   function get_proflik.m
% Calculates the profliklihood confidence intervals for the noise parameters
% The input file used is the results from the first stage and data.
% The result .mat file contains the information needed to draw the profile likelihoods. 
% A figure is generated. 
%   Tools needed: 
%         a) ODE SOLVER
%         b) Model
%   Funtions, objective functions used: 
%         a) GLS estimates
%         b) calc_AR: the objective function used, if AR pool was used to get the theta_hats, 
% 			 else use calc_PL
%   
%   Lines that have to modified:
%       % a) likleihood calculation
% 		b) folder containing results of the first stage

%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------------------------------------------
%% 

%
clear all
addpath(genpath('~/Repos/odeSD/'))  % path to ODESD
addpath(genpath('../Model'))        % path to Model
addpath(genpath('~/TOOLBOXES/nlopt-2.4.2/'))

t1=cputime;
load('../Pubs_Results/AR_Result1.mat')
fname='AR_pl';                     % (USER MUST MODIFY): name of the output file

% Load the data 
data = readtable('../Data/Di_monolix_full.csv','Delimiter',',', 'TreatAsEmpty','NA');
ids = unique(data.ID);

% define integration options
int_opts.abstol=1e-8;
int_opts.reltol=1e-4;

% Structures to store information from ODE integration and likelihood
% calculation. 
f_t=struct();
for_pl=struct();

for i = 1:325
    t=data.TIME(data.ID==i,:);
    [f_t(i).f_t]=Run_Llamosi2016(t,exp(betai_hat(i,:))',int_opts);
    %[for_pl(i).res,for_pl(i).h,for_pl(i).gm]=WLS_gm(t,betai_hat,theta_hat,data.Y(data.ID==i,:),int_opts);
end


theta0_0=theta_hat(1); 				% estimated value of theta0
theta1=theta_hat(2);				% estimated value of theta_1

%% Fix theta0;
trial=1000; 							% number of points to vary
R_theta0 =[linspace(500,700,trial)];% The points of theta0 used to construct the interval
fval_theta0=zeros(trial,1);			% function values returned
x2_theta0=zeros(trial,1);			% the value of theta0 that minimizess the function value. 
exflg_theta0=zeros(trial,1);		% Exit flag.  


for iter=1:trial
	theta0=R_theta0(iter);
	% Change this line to change the likelihood
	get_profile=@(p1) calc_AR(f_t,theta0,p1,data,scaling); %calc_PL(f_t,theta0,p1,data); 
	opt=struct();
	opt.algorithm = NLOPT_LN_NELDERMEAD;
	opt.min_objective = get_profile;
	opt.ftol_rel = 1e-3;
	opt.xtol_rel = 1e-3;
	[x2_theta0(iter),fval_theta0(iter),exflg_theta0(iter)]=nlopt_optimize(opt,theta1);
end

%% Fix theta1:
trial=1000;								% number of points to vary
theta1_0=theta_hat(2);
theta0=theta0_0;
R_theta1 =linspace(-0.03,0.03,trial);	% The points of theta1 used to construct the interval
fval_theta1=zeros(trial,1);				% function values returned
x2_theta1=zeros(trial,1);				% the value of theta0 that minimizess the function value. 
exflg_theta1=zeros(trial,1);			% Exit flag.  

for iter=1:trial
	theta1=R_theta1(iter);
	% Change this line to change the likelihood
	get_profile=@(p1) calc_AR(f_t,p1,theta1,data,scaling);
	opt=struct();
	opt.algorithm = NLOPT_LN_NELDERMEAD;
	opt.min_objective = get_profile;
	opt.ftol_rel = 1e-3;
	opt.xtol_rel = 1e-3;
	[x2_theta1(iter),fval_theta1(iter),exflg_theta1(iter)]=nlopt_optimize(opt,theta0);
end

%% Plot
lk0=calc_AR(f_t,theta0_0,theta1_0,data,scaling); % likelihood that we initially obtained
tcrit=3.841;                                     % Chi-sq statisic, one degree of freedom

%% Plot figure 
figure()
subplot(1,2,1)
plot(sqrt(sigma2)*R_theta0,-fval_theta0,'-')
hold on
plot(sqrt(sigma2)*theta0_0,-lk0,'*r')
h=refline([0,-lk0-tcrit/2]);
h.Color='r';
xlabel('theta0')
ylabel('L1')

subplot(1,2,2)
plot((sqrt(sigma2)*exp(R_theta1)),-fval_theta1,'-')
hold on
plot(sqrt(sigma2)*exp(theta1_0),-lk0,'*r')
h=refline([0,-lk0-tcrit/2]);
h.Color='r';
xlabel('theta1')
ylabel('L1')
t2=cputime-t1; % Time taken to calculate this. 

%save(sprintf('%s.mat',fname),'sigma2','t2','R_theta1','R_theta0','lk0','theta0_0','theta1_0','fval_theta1','fval_theta0')
