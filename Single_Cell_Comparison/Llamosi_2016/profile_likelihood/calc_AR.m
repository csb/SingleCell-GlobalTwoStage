function vals=calc_AR(f_t,theta0,theta1,data,scaling)
%%%---------------------------------------------------------------------------------------------------------
%
%   function calc_AR.m
% 
%   Calculate the likeilhood using pooled AR likelihood. 
%   
% 
%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------------------------------------------
%% 

vals=0;

for i =1:length(f_t)
d=data.Y(data.ID==i);
nonans=~isnan(d); 

h=scaling*(theta0+exp(theta1).*f_t(i).f_t); % Account for the Laplace scale factor
if (any(h==Inf)||sum(log(h)==-Inf)||~any(isreal(log(h))))
res=Inf;
else
res=sum(log(h(nonans))+abs(d(nonans)-f_t(i).f_t(nonans))./(h(nonans)));
end
vals=vals+res;

end