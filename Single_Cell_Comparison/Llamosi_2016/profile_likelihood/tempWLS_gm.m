function [res,h,gm]=WLS_gm(t,kinetic_params,noise_params,d,int_opts)

nonans=~isnan(d); % NaN values

if any(exp(kinetic_params)==Inf) %If the parameter is too big 
res=Inf;
else

% Run MODEL
[f_t]=Run_Llamosi2016(t,exp(kinetic_params),int_opts);
h=var_fun(noise_params,f_t);

% Evaluate the cost function : ignoring NaN values (5.1 in book, note the 0.5 is 
% here because in the first component the root of S is taken)

if (any(h==Inf)||sum(log(h)==-Inf)||~any(isreal(log(h))))
res=Inf;gm=Inf;h=Inf;
else  
	res= d(nonans)-f_t(nonans);
	h=h(nonans);
	gm=1/length(h)*sum(log(h));
end

end
