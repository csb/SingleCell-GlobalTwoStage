function FirstStage_CVODES()
%%%------------------------------------------------------------------------
%
%   function FirstStage_CVODES.m
%   Implements the First stage for the analysis of the data from Llamosi et al, 2016. Reads in 
%   the data used in Data/ and saves the results in output folder.
%   If using an SGE (Sun Grid Engine) cluster, this can be run using the accompanying .sh script
%       qsub submit_FirstStage.sh
%   The resulting mat files store a lot of information, the estimates, function values, return code,
%   data and model simulation for each sell in a structure 'GLS_params'. Followed by information on the convergence of parameters, CPU time 'CPU_end' and 
%   noise parameters ('theta_hat', 'sigma2') and the individual FIMs 'C_inv'. 
%   Tools needed: 
%         a) ODE SOLVER: CVODES from AMICI
%         b) Optimizer: NLOPT
%   Funtions, objective functions used: 
%         a) OLS estimates
%         b) WLS
%         c) POOLED GLS 
%         d) Model integration, and evaluating the error model
%         e) Auxilliary functions to calculate the fisher information matrix.      
%   
%   Lines that have to modified:
%         a) the paths to ODE integrator, and Optimizer. 
%         b) Output directory
%         c) parallellization options

%   Lekshmi Dharmarajan, July 2017
%   Checked, January 2018
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------

    addpath(genpath('~/Repos/AMICI/'))              % path to ODESD, ODE SOLVER (USER MUST MODIFY) 
    addpath(genpath('Model/'))                      % path to ODE model, variance model
    addpath(genpath('~/TOOLBOXES/nlopt-2.4.2/'))    % path to NLOPT (USER MODIFIABLE)
    addpath(genpath('Auxilliary'))                  % path to helper functions
    addpath(genpath('FirstStage_objs'))             % path to objective functions used
    addpath(genpath('Data'))                        % path to objective functions used

   
    % Output directory
    out_dir='Pubs_Results';                         % path to store the results (USER MUST MODIFY)

    % Parallel ability: To enable parralelisation, start parpool
    c = parcluster('local'); % Change 'local' to any other cluster profile (USER MUST MODIFY)
    c.NumWorkers = 100;
    parpool(c, c.NumWorkers);
    
    % add the paths in the workers, if not using local parpool but rather a cluster
    % else comment out. 
%     parfor i=1:c.NumWorkers 
%     % give the name and directory 
%     addpath(genpath('/usr/local/stelling/el6/nlopt-2.4.2/lib'))
%     addpath(genpath('/usr/local/stelling/el6/nlopt-2.4.2/matlab'))
%     addpath(genpath('/usr/local/stelling/el6/nlopt-2.4.2/include'))
%     loadlibrary('libnlopt.so','nlopt.h');
%     addpath(genpath('./Model')) 
%     addpath(genpath('~/Repos/AMICI'))
%     addpath(genpath('./Auxilliary')) 
%     addpath(genpath('./FirstStage_objs')) 
%     end

    % Load the data: data should contain columns Y, ID and TIME at the most. 
    data = readtable('Data/Di_monolix_full.csv','Delimiter',',', 'TreatAsEmpty','NA');
    ids = unique(data.ID);

    %%%%%%%%%% Tweaking parameters: %%%%%%%%%%
    % Initial parameters: tau is set constant in the paper, in natural log (ln)
    k_m = 2.30;
    g_m = -1.22;
    k_p = -5.45 *10^-2;
    g_p = -5.52;
    tau = 3.40;
    theta_0 = 5.99;     %e_a in publication
    theta_1  = -1.20;   %e_b in publication

    p0=[k_m g_m g_p theta_0 theta_1]; % starting points of the optimization
    x0 = [0 0 0]; % m p u             % initial conditions for ODE integration, if not part of the parameters.  
    % Initialise
    beta_init = [k_m g_m  g_p]; % in log
    theta_init = [exp(theta_0) theta_1]; % theta_1 in log
    n_cells=length(ids);
    n_kinetic_params=length(beta_init);

    % Parameters that can be tuned:
    % tolerances for optimizer convergence
    x_tol=1e-5;
    f_tol=1e-5;
    x_tol_gls=1e-5;
    f_tol_gls=1e-5;

    % tolerances to the ODE SOLVER 
    int_opts=struct();
    int_opts.abstol=1e-8; 
    int_opts.reltol=1e-4;

    % Convergences
    max_iter_GLS=10;
    min_iter_GLS=2;
    max_score=0.01;

    % Objective function used to estimate the noise parameters
    residual_type='AR'; % or choose 'PL'

    % Pre initialze the array to store the results of the individual fits. 
    x_ols = zeros(length(ids), length(beta_init));  % Store individual parameters
    fval_ols = zeros(1, length(ids));               % Store the function values
    conv = [];                                      % Convergence 

    % model funtion handle that should be passed into the functions
    model_fn=@(time, params,options) RunLlamosi2016cvodes(time, params, options);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % OLS estimate:
    t1 = tic
    spmd, cpu0 = cputime(); end
   parfor i = 1: n_cells
        t = data.TIME(data.ID == ids(i));
        fun = @(para) OLS(t, [para(1:2),  para(3)], data.Y(data.ID == ids(i)),int_opts,model_fn); 

        % Optimization settings
        opt = struct();
        opt.algorithm = NLOPT_LN_NELDERMEAD;
        opt.min_objective = fun;
        opt.ftol_rel = f_tol;
        opt.xtol_rel = x_tol;
        [x_ols(i, :), fval_ols(i), exflg(i)] = nlopt_optimize(opt, beta_init);
    end
    t2 = toc(t1);

    %% Initialise with OLS estimate: beta_init. 
    beta_init = x_ols(:, 1: 3);

    % iteration counters
    itern = 0; 
    iternact=0; 


    % estimates in the first iteration
    betai_gls = beta_init;
    theta_gls = [theta_init];

    % Convergence score.
    score = 10^5; % Initialise the score to a large number      
    diffs = []; % to store error

    % To store parameters
    diff_noise = [];   
    diff_p1 = [];
    diff_p2 = [];
    diff_p3 = [];
    diff_fval = [];

    % Start GLS-POOL estimation
    t3 = tic;
    while (itern == 0 || ((itern <= max_iter_GLS-min_iter_GLS) && (score >= max_score)))  % Convergence criteria
	
	for_pl=struct();
	
	parfor i=1:n_cells
           t = data.TIME(data.ID == ids(i), : );
      	   f_t=model_fn(t,exp(betai_gls(i,:)),int_opts);
	   for_pl(i).f_t=f_t;
	end

    % Maximize noise parameters with
        if residual_type=='AR'
        fun = @(para) get_AR_gm(for_pl,  para, data, ids);
        else
        fun = @(para) get_PL_gm(for_pl,  para, data, ids);
        end

        opt_noise = struct();
        opt_noise.algorithm = NLOPT_LN_NELDERMEAD;
        opt_noise.min_objective = fun;
        opt_noise.ftol_rel = f_tol;
        opt_noise.xtol_rel = x_tol;
        [theta_hat, ~,~] = nlopt_optimize(opt_noise, theta_gls); 
        
        % initalizeing the array to store result
        betai_hat = zeros(length(ids), size(betai_gls, 2));

        % Construct weights with theta_hat and maximize parameters
        parfor i = 1: n_cells
            t = data.TIME(data.ID == ids(i), : );	
	
		% get weights
	   h=var_fun(theta_hat,for_pl(i).f_t); 	
	
		% Re-estimate individual parameters
	    fun = @(para) WOLS(t, [para(1:2), para(3)],  h, data.Y(data.ID == ids(i), : ),int_opts,model_fn); 
            opt = struct();
            opt.algorithm = NLOPT_LN_NELDERMEAD;
            opt.min_objective = fun;
            
            % Using low tolerances here can lead to bad estimates.
            opt.ftol_rel = f_tol_gls; 
            opt.xtol_rel = x_tol_gls;
            [betai_hat(i, :), fval(i), exflg(i)] = nlopt_optimize(opt, betai_gls(i, : ));
        end

        % Get the convergence measurement and store, based on relative convergence
        score1 = max(max(abs((betai_hat - betai_gls)./ betai_gls))); 
        score2 = max(abs(theta_gls - theta_hat)./theta_gls); 
        score = min(score1, score2);
        diffs = cat(1, diffs, cat(2, score2, score1));

        diff_noise = cat(1, diff_noise, theta_hat);
        diff_p1 = cat(2, diff_p1, betai_hat(:, 1));
        diff_p2 = cat(2, diff_p2, betai_hat(:, 2));
        diff_p3 = cat(2, diff_p3, betai_hat(:, 3));
        diff_fval = cat(2, diff_fval, fval);
        % disp(diffs)

        % Set for iterator
        iternact = iternact + 1;
        itern = max(0, iternact - 2);
        betai_gls = betai_hat(:, 1: 3);
        theta_gls = theta_hat;

    end


    %%
    % Calculate the sigma from the residuals
    for_pl = struct();
    for i = 1: n_cells
        p =  exp(betai_hat(i,:));
        t = data.TIME(data.ID == ids(i), : );
        for_pl(i).f_t=model_fn(t,p,int_opts);
        [for_pl(i).res, for_pl(i).h, for_pl(i).gm] = WLS_gm(for_pl(i).f_t,theta_hat, data.Y(data.ID == ids(i), : ));
    end

    % Calculate the estimate of sigma2 and the scaling parameter (eta, in book) (for AR)
    sigma = (cell2mat({for_pl.res}')./cell2mat({for_pl.h}')).^2;
    sigma2 = sum(sigma(: )) / (length(sigma(: )) - size(betai_gls, 2) * size(betai_gls, 1));
    e_ij = abs((cell2mat({for_pl.res}')./(sqrt(sigma2)*cell2mat({for_pl.h}'))));
    E_eij = mean(e_ij);
    scaling = E_eij * sqrt(sigma2);

    % Make directory to save results, and save all results in a structure and some variables in the MAT file. 
    mkdir(sprintf('%s',out_dir))
    GLS_params=struct();


    % Get the uncertainities of the estimate!
    C_inv=zeros(n_kinetic_params,n_kinetic_params,n_cells);     % Storing FIM.
    C_inv_hessian=C_inv;                                        % Storing Hessian

    for i=1:n_cells
    GLS_params(i).t=data.TIME(data.ID==ids(i),:);   % Time
    GLS_params(i).dat=data.Y(data.ID==ids(i),:);    % data
    GLS_params(i).params=betai_hat(i,:);            % estimates of parameters
    % Could have avoided this... by also storing sensitivities. 
    noise_params=sqrt(sigma2).*[theta_hat(1) exp(theta_hat(2))];
    [~,~,~,~, C_inv(:,:,i), C_inv_hessian(:,:,i)] = get_secondordgradient(t,exp(betai_hat(i,:)),GLS_params(i).dat,noise_params);
    GLS_params(i).C_inv=C_inv(:,:,i);               % FIM.
    GLS_params(i).C_inv_hessian=C_inv_hessian(:,:,i); % With second order information.               
    GLS_params(i).theta_hat=theta_hat;              % Noise estimates: Pooled 
    GLS_params(i).sigma2=sigma2;                    % Noise estimate:  Pooled, can be amended if the pooled assumption is not true 
    GLS_params(i).int_opts=int_opts;                % Integration options
    GLS_params(i).exflg=exflg(i);                   % Exit flags of the individual optimization of cells    
    GLS_params(i).fval=fval(i); 
    end

    t4 = toc(t3);
    spmd, cpu_end = cputime() - cpu0, end
    spmd, cptime = gplus(cpu_end), end
    cpu_end = cell2mat(cpu_end(: ));
    cptime = cell2mat(cptime(: ));

    mkdir(out_dir);
    % Save results                                  % (USER MUST MODIFY)

save(sprintf('%s/%s_Result_CVODES.mat', out_dir,residual_type),'diff_noise','diff_p1','diff_p2','diff_p3','diff_fval','diffs', 'GLS_params','cpu_end', 'theta_hat','cptime','betai_hat','C_inv','sigma2','scaling','int_opts','C_inv_hessian')  % end parallelization. 
    delete(gcp('nocreate'))
    diary off
end
