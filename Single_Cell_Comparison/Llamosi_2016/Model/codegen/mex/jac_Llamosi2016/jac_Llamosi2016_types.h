/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * jac_Llamosi2016_types.h
 *
 * Code generation for function 'jac_Llamosi2016'
 *
 */

#ifndef JAC_LLAMOSI2016_TYPES_H
#define JAC_LLAMOSI2016_TYPES_H

/* Include files */
#include "rtwtypes.h"
#endif

/* End of code generation (jac_Llamosi2016_types.h) */
