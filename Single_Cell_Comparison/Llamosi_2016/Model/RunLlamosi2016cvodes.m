function [f_t, dydp,H1] = RunLlamosi2016cvodes(varargin)

ME=[]; % Store error
try
 t=varargin{1};
 para=varargin{2};
 int_opts=varargin{3};
 
% Parameters passed 
k_m=para(1); %m production
g_m=para(2); %m degradation
g_p=para(3); %p degradation

% fixed constants
k_p=exp(-0.0545);
tau=exp(3.4);

addon=[0 2 3 10 14];% defines the time where the integration input changes

% times when input is added
t_in=[30  60.0000   90.0000  120.0000  150.0000  180.0000  210.0000  241.9064  290.4352  372.1597  455.3190 510.8225  546.0478];
t0=0;
x0=[1.0e-10 1.0e-10 0]';
times=t0;

% intial values
xs=[x0']';
ys=1e-10;
us=0;

% input time
inp=30;

options = amioption('sensi',1,'ism',2,'sensi_meth','forward',...
'atol',int_opts.abstol,'rtol',int_opts.reltol,'tstart',0,'maxsteps',10000,...
'x0',xs);

% intialise sensitivities vector 
sensitivities_1=0;
sensitivities_2=0;
sensitivities_3=0;

% Seond order sensitivities: intialise empty array
for i=1:length(t_in)
for j=1:length(addon)

    simulation_time=linspace(t0, t_in(i)+addon(j)+tau);
    simulated=simulate_Lamosi2exp(simulation_time,[k_m, g_m, g_p],[k_p,tau, inp+tau],[],options);
    t0=t_in(i)+addon(j)+tau;
    inp=t_in(i);
    x0=simulated.x(end,:)';
    times=cat(1,times,simulated.t(2:end));
    xs=cat(2,xs,simulated.x(2:end,:)');
    ys=cat(1,ys,simulated.y(2:end,1));
    us=cat(1,us,simulated.y(2:end,2));
    
    % first order sens: for observation
    sensitivities_1=cat(1,sensitivities_1, simulated.sy(2:end,1,1).*k_m);
    sensitivities_2=cat(1,sensitivities_2,simulated.sy(2:end,1,2).*g_m);
    sensitivities_3=cat(1,sensitivities_3,simulated.sy(2:end,1,3).*g_p);

    % initialize sensitivity also for next iteration
    init_sx=squeeze(simulated.sx(end,:,:));

    % new options set 
    options = amioption('sensi',1,'ism',2,'sensi_meth','forward',...
    'atol',int_opts.abstol,'rtol',int_opts.reltol,'tstart',t0,'maxsteps',10000,'x0',x0,'sx0',init_sx);
end
end
simulation_time=linspace(546.0478+14+tau, 650);
simulated=simulate_Lamosi2exp(simulation_time,([k_m, g_m, g_p]),[k_p,tau, inp+tau],[],options);

    times=cat(1,times,simulated.t(2:end));
    xs=cat(2,xs,simulated.x(2:end,:)');
    ys=cat(1,ys,simulated.y(2:end,1));
    us=cat(1,us,simulated.y(2:end,2));

    % Observation sensitivities
    sensitivities_1=cat(1,sensitivities_1,simulated.sy(2:end,1,1).*k_m);
    sensitivities_2=cat(1,sensitivities_2,simulated.sy(2:end,1,2).*g_m);
    sensitivities_3=cat(1,sensitivities_3,simulated.sy(2:end,1,3).*g_p);

    % Interpolate states/ observation for times we are interested in 
    f_t=interp1(times,ys(:,1),t);
    dydp=[sensitivities_1 sensitivities_2 sensitivities_3];
    dydp=interp1(times,dydp,t);
    us=interp1(times,us,t);

    H1=[];
    % noise model
    if nargin>3
      noise_params=varargin{4};
        
     sigma_y=noise_params(1)+f_t*noise_params(2);
     ydata=varargin{5};
      
     %Check NaNs in the data and exclude them.
     ids=1:length(ydata);
     nonans=~isnan(ydata);
     dydp=dydp(ids(nonans),[1,2,3]);  
     sigma_y=sigma_y(ids(nonans),:);


     % Hessian using the first order sensitivities: 
     H1= dydp'*get_stableinverse(diag(sigma_y.^2))*dydp;
     
    end
 
        

catch ME
disp(ME)
end

% incase there was an error
if isempty(ME)==false
    f_t=zeros(size(t))+Inf;
    f_t=f_t';
    dydp=[];
    H1=Inf;
end
end
