MATLAB="/net/bs-gridsw/sw-repo/bsse/MATLAB_R2016b"
Arch=glnxa64
ENTRYPOINT=mexFunction
MAPFILE=$ENTRYPOINT'.map'
PREFDIR="/home/dlekshmi/.matlab/R2016b"
OPTSFILE_NAME="./setEnv.sh"
. $OPTSFILE_NAME
COMPILER=$CC
. $OPTSFILE_NAME
echo "# Make settings for jac_Llamosi2016_o2" > jac_Llamosi2016_o2_mex.mki
echo "CC=$CC" >> jac_Llamosi2016_o2_mex.mki
echo "CFLAGS=$CFLAGS" >> jac_Llamosi2016_o2_mex.mki
echo "CLIBS=$CLIBS" >> jac_Llamosi2016_o2_mex.mki
echo "COPTIMFLAGS=$COPTIMFLAGS" >> jac_Llamosi2016_o2_mex.mki
echo "CDEBUGFLAGS=$CDEBUGFLAGS" >> jac_Llamosi2016_o2_mex.mki
echo "CXX=$CXX" >> jac_Llamosi2016_o2_mex.mki
echo "CXXFLAGS=$CXXFLAGS" >> jac_Llamosi2016_o2_mex.mki
echo "CXXLIBS=$CXXLIBS" >> jac_Llamosi2016_o2_mex.mki
echo "CXXOPTIMFLAGS=$CXXOPTIMFLAGS" >> jac_Llamosi2016_o2_mex.mki
echo "CXXDEBUGFLAGS=$CXXDEBUGFLAGS" >> jac_Llamosi2016_o2_mex.mki
echo "LDFLAGS=$LDFLAGS" >> jac_Llamosi2016_o2_mex.mki
echo "LDOPTIMFLAGS=$LDOPTIMFLAGS" >> jac_Llamosi2016_o2_mex.mki
echo "LDDEBUGFLAGS=$LDDEBUGFLAGS" >> jac_Llamosi2016_o2_mex.mki
echo "Arch=$Arch" >> jac_Llamosi2016_o2_mex.mki
echo "LD=$LD" >> jac_Llamosi2016_o2_mex.mki
echo OMPFLAGS= >> jac_Llamosi2016_o2_mex.mki
echo OMPLINKFLAGS= >> jac_Llamosi2016_o2_mex.mki
echo "EMC_COMPILER=gcc" >> jac_Llamosi2016_o2_mex.mki
echo "EMC_CONFIG=optim" >> jac_Llamosi2016_o2_mex.mki
"/net/bs-gridsw/sw-repo/bsse/MATLAB_R2016b/bin/glnxa64/gmake" -B -f jac_Llamosi2016_o2_mex.mk
