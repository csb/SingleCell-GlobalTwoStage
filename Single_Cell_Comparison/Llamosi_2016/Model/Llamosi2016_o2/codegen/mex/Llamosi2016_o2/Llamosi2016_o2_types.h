/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Llamosi2016_o2_types.h
 *
 * Code generation for function 'Llamosi2016_o2'
 *
 */

#ifndef LLAMOSI2016_O2_TYPES_H
#define LLAMOSI2016_O2_TYPES_H

/* Include files */
#include "rtwtypes.h"
#endif

/* End of code generation (Llamosi2016_o2_types.h) */
