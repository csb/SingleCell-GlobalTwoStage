/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Llamosi2016_o2_data.c
 *
 * Code generation for function 'Llamosi2016_o2_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Llamosi2016_o2.h"
#include "Llamosi2016_o2_data.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131435U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "Llamosi2016_o2",                    /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

emlrtRSInfo emlrtRSI = { 60,           /* lineNo */
  "Llamosi2016_o2",                    /* fcnName */
  "/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Llamosi_2016/Model/Llamosi2016_o2/Llamosi2016_o2.m"/* pathName */
};

emlrtRSInfo b_emlrtRSI = { 62,         /* lineNo */
  "jac_Llamosi2016_o2",                /* fcnName */
  "/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Llamosi_2016/Model/Llamosi2016_o2/jac_Llamosi2016_o2.m"/* pathName */
};

emlrtRSInfo c_emlrtRSI = { 88,         /* lineNo */
  "jac_Llamosi2016_o2",                /* fcnName */
  "/home/dlekshmi/Llamos_pr/All_cells/Iqm/Single_Cell_Comparison/Llamosi_2016/Model/Llamosi2016_o2/jac_Llamosi2016_o2.m"/* pathName */
};

emlrtRSInfo d_emlrtRSI = { 68,         /* lineNo */
  "eml_mtimes_helper",                 /* fcnName */
  "/net/bs-gridsw/sw-repo/bsse/MATLAB_R2016b/toolbox/eml/lib/matlab/ops/eml_mtimes_helper.m"/* pathName */
};

emlrtRSInfo e_emlrtRSI = { 87,         /* lineNo */
  "xgemm",                             /* fcnName */
  "/net/bs-gridsw/sw-repo/bsse/MATLAB_R2016b/toolbox/eml/eml/+coder/+internal/+blas/xgemm.m"/* pathName */
};

emlrtRSInfo f_emlrtRSI = { 89,         /* lineNo */
  "xgemm",                             /* fcnName */
  "/net/bs-gridsw/sw-repo/bsse/MATLAB_R2016b/toolbox/eml/eml/+coder/+internal/+blas/xgemm.m"/* pathName */
};

emlrtRSInfo g_emlrtRSI = { 90,         /* lineNo */
  "xgemm",                             /* fcnName */
  "/net/bs-gridsw/sw-repo/bsse/MATLAB_R2016b/toolbox/eml/eml/+coder/+internal/+blas/xgemm.m"/* pathName */
};

emlrtRSInfo h_emlrtRSI = { 92,         /* lineNo */
  "xgemm",                             /* fcnName */
  "/net/bs-gridsw/sw-repo/bsse/MATLAB_R2016b/toolbox/eml/eml/+coder/+internal/+blas/xgemm.m"/* pathName */
};

emlrtRSInfo i_emlrtRSI = { 85,         /* lineNo */
  "xgemm",                             /* fcnName */
  "/net/bs-gridsw/sw-repo/bsse/MATLAB_R2016b/toolbox/eml/eml/+coder/+internal/+blas/xgemm.m"/* pathName */
};

emlrtRSInfo j_emlrtRSI = { 10,         /* lineNo */
  "int",                               /* fcnName */
  "/net/bs-gridsw/sw-repo/bsse/MATLAB_R2016b/toolbox/eml/eml/+coder/+internal/+blas/int.m"/* pathName */
};

/* End of code generation (Llamosi2016_o2_data.c) */
