/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Llamosi2016_o2_terminate.c
 *
 * Code generation for function 'Llamosi2016_o2_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Llamosi2016_o2.h"
#include "Llamosi2016_o2_terminate.h"
#include "_coder_Llamosi2016_o2_mex.h"
#include "Llamosi2016_o2_data.h"

/* Function Definitions */
void Llamosi2016_o2_atexit(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

void Llamosi2016_o2_terminate(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/* End of code generation (Llamosi2016_o2_terminate.c) */
