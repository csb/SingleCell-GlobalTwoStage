%%%------------------------------------------------------------------------
%% The STS method done in Llamosi's paper. 
%  Inital values are taken from the 
%  Table S1 A. 
%    Tools needed: 
%         a) ODE SOLVER
%         b) Optimizer
%   Funtions, objective functions used: 
%         a) Obj_fun
%         b) Model integration, and evaluating the error model

%   Lines that have to modified:
%       % a) the paths to ODE integrator, and Optimizer. 
%         b) Output directory
%         c) parallellization options
%
%  Result is saved as a .mat file in Pubs_Result (Default) and contains: 
%        Time taken in each thread: 'cpu1', total: 'cptime'
%        elapsed time: 't_elapsed'
%        individual estiamtes: 'betai_sts';
%        function values and exit flags of optimizatio: 'fval', 'exflg':
%        Initial points: 'p0','logp0';
%        variance and mean of all individual parameters: 'Dhat_sts','betahat_sts'

%
%  Lekshmi March 2nd 2016
%  modified: January 2018
%%%------------------------------------------------------------------------
clear all

% Parallel ability
c = parcluster('local');
c.NumWorkers = 16;
parpool(c, c.NumWorkers);


% Add paths to optimizer, integrator, Load the data 
addpath(genpath('~/TOOLBOXES/nlopt-2.4.2/')) % Optimizer path
addpath(genpath('~/Repos/odeSD'))
addpath(genpath('../Model'))
data = readtable('../Data/Di_monolix_full.csv','Delimiter',',', 'TreatAsEmpty','NA');
ids=unique(data.ID);
%ids_nan=unique(data.ID(isnan(data.Y))); 

% output directory
out_dir='Pubs_Results';
mkdir(out_dir);


% Initial values for optimization
k_m=2.30;
g_m=-1.22;
k_p=-5.45*10^-2;
g_p=-5.52;
tau=3.4;
e_a=5.99;  % We use notation theta_0
e_b=-1.20; % We use notation theta_1

p0=exp([k_m g_m  g_p e_a e_b]); %tau, kp const
logp0=[log(p0(1:3)),p0(4),log(p0(5))];

%% 
t1=tic;
spmd, cpu0 = cputime(); end
x0=[0 0 0]; % m p u initial condition
betai_sts=zeros(length(ids),length(logp0)); % Initialize array to store estimates

% tolerances
int_opts=struct();
int_opts.abstol=1e-6;
int_opts.rtol=1e-4;

opt_tol=1e-4;
% Use parfor and do the fminsearch on each data
fval=zeros(length(ids),1);
exflg=zeros(length(ids),1);
t2=zeros(length(ids),1);
opt=struct();
parfor i = 1:length(ids)

    t=data.TIME(data.ID==ids(i),:);

    fun=@(para) Obj_fun(t,para,data.Y(data.ID==ids(i),:),int_opts);
    opt=struct()  ; 
    opt.algorithm = NLOPT_LN_NELDERMEAD;
    opt.min_objective = fun;
    opt.ftol_rel = opt_tol;
    opt.xtol_rel = opt_tol;

    [betai_sts(i,:),fval(i),exflg(i)]=nlopt_optimize(opt,logp0);

end
t_elapsed=toc(t1);
%%

% Aggregate the results
betahat_sts=mean(betai_sts(:,:));       % STS-Mean: beta_hat 
Dhat_sts=cov(betai_sts(:,:));           % STS-Covariance: Dhat

spmd, cpu1 = cputime() - cpu0, end
spmd, cptime=gplus(cpu1), end
cpu1=cpu1(:);
cptime=cptime(:);

%save(sprintf('%s/STS_325_%s_nlopt_fixed_ex.mat',outdir,date()),'cpu1','exflg','cptime','t_elapsed','betai_sts','fval','p0','logp0','Dhat_sts','betahat_sts')
save(sprintf('%s/STS_estimates.mat',out_dir),'cpu1','exflg','cptime','t_elapsed','betai_sts','fval','p0','logp0','Dhat_sts','betahat_sts')

delete(gcp('nocreate'))

