#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N plot_naive
#$ -o ./Plot.out
#$ -e ./Plot.err
#$ -cwd
matlab -nosplash  -r 'Plot_prediction; exit'
