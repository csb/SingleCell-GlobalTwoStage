function []=Plot_prections(trial)
%clear all 

rng('shuffle') %For reproducibility
addpath(genpath('~/PLOTS/'))
addpath(genpath('~/Repos/odeSD/'))
addpath(genpath('../Model'))

fname='STS_estimates.mat';
load(sprintf('Pubs_Results/%s',fname))

%% Plot NTS/ STS
params2=[];
Model_prediction=[];

Dhat_sts=Dhat_sts(1:3,1:3);
noise=betahat_sts(4:5);
betahat_sts=betahat_sts(1:3);
int_opts=struct();
int_opts.abstol=1e-6;
int_opts.rtol=1e-3;

% simulate
for i=1:10000
paras0=(mvnrnd(betahat_sts,Dhat_sts));
paras0=[exp(paras0(1:3))];
[f_t]=Run_Llamosi2016(0:600,paras0,int_opts);
h=(noise(1)+exp(noise(2))*f_t).*randn(size(f_t));
f_t=f_t+h;
%[f_t]=Run_Hogmodelode(0:600,[paras0,exp(tau)]);
Model_prediction=cat(1,Model_prediction,f_t);
end


mkdir(sprintf('Pubs_Results/plots_data'))
save(sprintf('Pubs_Results/plots_data/%s_%s.mat',fname,date()),'Model_prediction')

end

