#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N Naive_STS
#$ -pe openmpi624 16
#$ -o ./naive.out
#$ -e ./naive.err
#$ -cwd
matlab -nosplash  -r 'Naive_STS; exit'
