% This code aggregates the reuslts from sampling. 
% Lekshmi D
% Checked in : March 2018
% Run after sampling the cells

% Loading the result from GTS. 
load('../../../../Pubs_Results/AR_Result.mat')    
addpath('../../../../Auxilliary/')

n_cells=325;
n_params=3;
all_covs=zeros(n_params, n_params,n_cells);
all_means=zeros(n_cells,n_params);
all_acceptances=zeros(n_cells,1);
all_FIMS=all_covs;
all_times=zeros(1,n_cells);
ids=[];

for i=1:n_cells
    try
    load(sprintf('psampMH%d.mat',i))
    ids=cat(1,ids,i); 
    all_means(i,:)=mean(temp);
    [all_covs(:,:,i)]=cov(temp); % I need a mean and cov. 
    all_FIMS(:,:,i)=get_stableinverse(all_covs(:,:,i));
    all_times(i)=time_elapsed;
    all_acceptances(i)=length(unique(temp,'rows'))/size(temp,1);
    catch
        % all_FIMS(:,:,i)=C_inv(:,:,i);
    end
end


% Resubmit cells 
re_run=setdiff(1:325,ids);


%% Save results. 
save('../Samples_stats.mat','all_means','all_covs','ids','all_acceptances','all_FIMS','re_run')

betai_hat=all_means;
C_inv=all_FIMS;
save('../GTS_input_DRAM.mat','betai_hat','C_inv','sigma2','theta_hat')
% NOTE this should be input to SecondSatge after some minor modifications
% to script involving C_inv_hessian.

%%
% for i=1:325
%     load(sprintf('psampMH%d.mat',i))
%     scatter3(temp(5000:10000,1),temp(5000:10000,2),temp(5000:10000,3),[],obj_val(5000:10000))
%     hold on
% end