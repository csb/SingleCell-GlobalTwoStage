#!/bin/bash
#$ -V
#$ -q sc02.q
#$ -N Sample_LlamosiModel
##$ -pe openmpi624 16
##$ -o ./Sample.out
##$ -e ./Sample.err
#$ -cwd
matlab -nosplash  -r "sample_trajs($SGE_TASK_ID); exit"

