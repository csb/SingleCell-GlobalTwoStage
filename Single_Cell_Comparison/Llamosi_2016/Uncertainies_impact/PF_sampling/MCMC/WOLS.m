function res=WOLS(t,kinetic_params,weights,d,int_opts)
%%%------------------------------------------------------------------------
%
%   function WOLS.m
%
%   Standard WOLS (Weighted Ordinary Least Squares) method. 
%
%   Tools needed: 
        % a) ODE SOLVER

%  INPUT==> t: Time values in experiment,
%		  	kinetic_params: log kinetic parameters
% 		  noise_params: Noise parameters, theta in the model
%			d: data
%		int_opts: Options passed to integrator, structure.

%  OUTPUT==> res: The objective function weighted ordinary least squares. 

%   Lekshmi Dharmarajan, July 2017
%   CSB, ETH Zurich. 
%
%%%-----------------------------------------------------------------------
nonans=~isnan(d);
kinetic_exp=exp(kinetic_params);

if any(kinetic_exp==Inf) %If the parameter is too big 
res=-10^10;

else
[f_t]=Run_Llamosi2016wosens(t,kinetic_exp,int_opts); % No sensitivities integrated


% Only use the result if ODE integration did not fail. 
if (any(weights==Inf)||sum(log(weights)==-Inf)||~any(isreal(log(weights))))
res=-10^10;
else
res=(-0.5*nansum(((d(nonans)-f_t(nonans))./weights(nonans)).^2)); 
end



end
